<?php

namespace App;
use Exception;
use Omnipay\Omnipay;
use Illuminate\Database\Eloquent\Model;

class Esewa
{
    /**
     * @return \SecureGateway
     */
    public function gateway()
    {
        $gateway = Omnipay::create('Esewa_Secure');

        $gateway->setMerchantCode('testmerchant');
        $gateway->setTestMode(config('services.esewa.sandbox'));

        return $gateway;
    }
    /**
     * @param array $parameters
     * @return $response
     */
    public function purchase(array $parameters)
    {
        try {
         $response = $this->gateway()
            ->purchase($parameters)
            ->send();
        } 
        catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return $response;
    }

    /**
     * @param array $parameters
     * @return $response
     */
    public function verifyPayment(array $parameters)
    {
        $response = $this->gateway()
            ->verifyPayment($parameters)
            ->send();

        return $response;
    }

    /**
     * @param $amount
     */
    public function formatAmount($price)
    {
        return number_format($price, 2, '.', '');
    }

    /**
     * @param $order
     */
    public function getFailedUrl($cartContents)
    {
        return route('checkout.payment.esewa.failed');
    }

    /**
     * @param $order
     */
    public function getReturnUrl($cartContents)
    {
        return route('checkout.payment.esewa.completed');
    }
}