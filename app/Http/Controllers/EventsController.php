<?php

namespace App\Http\Controllers;

use App\ethnicity;
use App\events;
use App\EventsEthnics;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = events::latest()->get();
        $ethnicities = ethnicity::all();
        $eventsEthnics = EventsEthnics::all();
        return view ('dashboard.event.index', compact('events','ethnicities','eventsEthnics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $events = new events();
        $request->validate([
            'name' => 'required',
        ]);
        $events->name = $request->name;
        if(file_exists($request->file('image'))){
            $image = "events".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads/products/events');
            $request->file('image')->move($location, $image);
            $events->image = $image;
        }
        else{
            $events->image = 'Placeholder.jpg';
        }
        $events->save();
        $myProductId = $events->latest()->first()->id;
        $ethnicVariations= $request->ethnicity;
        if($ethnicVariations){
        foreach($ethnicVariations as $key=> $eth){

            EventsEthnics::create([
                'event_id'=>$myProductId,
                'ethnic_id'=>$ethnicVariations[$key]
            ]);

        }
    }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\events  $events
     * @return \Illuminate\Http\Response
     */
    public function show(events $events)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\events  $events
     * @return \Illuminate\Http\Response
     */
    public function edit(events $events,$id)
    {
        $ethnicities = ethnicity::latest()->get();
        $events = events::findOrfail($id);
        $eventsEthnics = EventsEthnics::where('event_id',$id)->select('ethnic_id')->get();
        return view ('dashboard.event.edit',compact('events','ethnicities','eventsEthnics'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\events  $events
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, events $events,$id)
    {
        $eventEth = EventsEthnics::where('event_id',$id)->delete();
        $events = events::findOrfail($id);
        $request->validate([
            'name' => 'required',
        ]);
        $events->name = $request->name;
        if(file_exists($request->file('image'))){
            $image = "events".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads/products/events');
            $request->file('image')->move($location, $image);
            $events->image = $image;
        }
        else{
            $events->image =  $events->image;
        }
        $events->save();
        $ethnicVariations= $request->ethnicity;
        if($ethnicVariations){
        foreach($ethnicVariations as $key=> $eth){

            EventsEthnics::create([
                'event_id'=>$id,
                'ethnic_id'=>$ethnicVariations[$key]
            ]);

        }
    }
        return redirect('/home/events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\events  $events
     * @return \Illuminate\Http\Response
     */
    public function destroy(events $events,$id)
    {
        $events = events::findOrFail($id)->delete();
        return redirect()->back();
    }
}
