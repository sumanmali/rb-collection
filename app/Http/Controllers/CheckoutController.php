<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Product;
use App\ProductVariation;
use Cart;

use Darryldecode\Cart\CartCondition;


class CheckoutController extends Controller
{
    public function redirectLogin(){
        
        dd('Herer');
    }

    public function checkout(){
        $user = Auth::user();
        $total = Cart::getTotal();
        $allProducts = Product::all();
        $cartContents = Cart::getContent();
        $product = ProductVariation::all(); 
        // dd($cartContents);
        // return $cartContents;
        return view('homepage.checkout', compact('total','cartContents','allProducts','user','product'));
    }
}
