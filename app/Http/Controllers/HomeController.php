<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use App\Product;
use App\SendMail;
use Cart;
use App\CustomOrder;
use App\productcategory;
use App\events;
use App\ethnicity;
use App\User;
use Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function userIndex(Request $request)
    {
        $id = Auth::user()->id;
        $currentuser = User::find($id);
        $allOrders = SendMail::where('user_id',$id)->get()->count();
        $cartCount = Cart::getContent()->count();
        return view ('userDashboard',compact('currentuser','allOrders','cartCount'));
    }

    public function userprofile(Request $request)
    {
        $id = Auth::user()->id;
        $currentuser = User::find($id);
        return view ('userProfile',compact('currentuser'));
    }
    public function passwordUpdate(Request $request,$id)
    {
         $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'password_confirmation' => ['same:new_password'],
            'image' => 'image|mimes:jpg,png,jpeg|',
        ]);
        $updateInfo = user::findorfail($id);
        if(file_exists($request->file('image'))){
            $image = "user".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('images/users');
            $request->file('image')->move($location, $image);
            $updateInfo->image = $image;
        }
        else{
            $updateInfo->image = $updateInfo->image;
        }
        $updateInfo->password = Hash::make($request->new_password);
        $updateInfo->save();
   
        return redirect()->back()->with('success','Password Changed Successfully');
    }
    public function userUpdate(Request $request,$id)
    {
        $currentuser = User::find($id);  
        return view('edit-user',compact('currentuser'));
    }
    public function orderHistory(Request $request)
    {
        $id = Auth::user()->id;
        $currentuser = User::find($id);
        $allOrders = SendMail::where('user_id',$id)->get();
        $prodName = DB::table('products')
        ->leftjoin('send_mails','products.id','=','send_mails.product_id')
        ->select("products.id","products.name")
        ->groupBy('id','name')
        ->get();
        return view ('order-history',compact('currentuser','allOrders','prodName'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totalProduct = Product::all()->count();
        $cat_count = productcategory::all()->count();
        $evt_count = events::all()->count();
        $eth_count = ethnicity::all()->count();
        $allOrders = DB::table('send_mails')
        ->leftjoin('users','send_mails.user_id','=','users.id')
        ->leftjoin('products','send_mails.product_id','=','products.id')
        ->paginate(5);
        $totalOrders = SendMail::count();
        $customOrder = CustomOrder::latest()->paginate(5);
        // return $customOrder;
        return view('dashboard',compact('totalProduct','cat_count','evt_count','eth_count','allOrders','totalOrders','customOrder'));
    }
}
