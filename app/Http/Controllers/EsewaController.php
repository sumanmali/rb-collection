<?php

namespace App\Http\Controllers;

use App\Esewa;
use Session;
use Illuminate\Http\Request;
use Cart;
use Darryldecode\Cart\CartCondition;
use App\Product;
use App\ProductVariation;

class EsewaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function checkout(Request $request)
    {
        $product = ProductVariation::findOrFail(mt_rand(1, 20));
        
        return view('esewa.checkout', compact('product'));
    }

    /**
     * @param $order_id
     * @param Request $request
     */
    public function payment(Request $request)
    {
        $gateway = with(new Esewa);

        try {
            $response = $gateway->purchase([
                'amount' => $gateway->formatAmount($request->rate),
                'totalAmount' => $gateway->formatAmount($request->rate),
                'productCode' => 'ABAC2098',
                'failedUrl' => $gateway->getFailedUrl($request->rate),
                'returnUrl' => $gateway->getReturnUrl($request->rate),
            ], $request);
           
        } catch (Exception $e) {
            $product->update(['payment_status' => ProductVariation::PAYMENT_PENDING]);

            return redirect()
                ->route('checkout.payment.esewa.failed', [$cartContents->id])
                ->with('message', sprintf("Your payment failed with error: %s", $e->getMessage()));
        }

        if ($response->isRedirect()) {
            $response->redirect();
        }

        return redirect()->back()->with([
            'message' => "We're unable to process your payment at the moment, please try again !",
        ]);
    }

    /**
     * @param $order_id
     * @param Request $request
     */
    public function completed(Request $request)
    {
        $product = ProductVariation::findOrFail();

        $gateway = with(new Esewa);

        $response = $gateway->verifyPayment([
            'amount' => $gateway->formatAmount($request->rate),
            'referenceNumber' => $request->get('refId'),
            'productCode' => $request->get('oid'),
        ], $request);

        if ($response->isSuccessful()) {
            $product->update([
                'transaction_id' => $request->get('refId'),
                'payment_status' => ProductVariation::PAYMENT_COMPLETED,
            ]);

            return redirect()->route('checkout.payment.esewa')->with([
                'message' => 'Thank you for your shopping, Your recent payment was successful.',
            ]);
        }

        return redirect()->route('checkout.payment.esewa')->with([
            'message' => 'Thank you for your shopping, However, the payment has been declined.',
        ]);
    }

    /**
     * @param $order_id
     * @param Request $request
     */
    public function failed(Request $request)
    {
        $product = ProductVariation::findOrFail();

        return view('esewa.checkout', compact('product'));
    }
}
