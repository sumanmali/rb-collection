<?php

namespace App\Http\Controllers;

use App\Product;
use App\productcategory;
use Illuminate\Http\Request;

class ProductcategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productcat = productcategory::all();
        return view ('dashboard.product-category.index', compact('productcat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.product-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $productcat = new productcategory();
        $request->validate([
            'name' => 'required',
        ]);
        $productcat->name = $request->name;
        if(file_exists($request->file('image'))){
            $image = "product-cat".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads/products');
            $request->file('image')->move($location, $image);
            $productcat->image = $image;
        }
        else{
            $productcat->image = 'Placeholder.jpg';
        }
        $productcat->save();
        return redirect('/home/product-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\productcategory  $productcategory
     * @return \Illuminate\Http\Response
     */
    public function show(productcategory $productcategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\productcategory  $productcategory
     * @return \Illuminate\Http\Response
     */
    public function edit(productcategory $productcategory,$id)
    {
        $productcat=productcategory::findOrfail($id); 
        return view ('dashboard.product-category.edit',compact('productcat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\productcategory  $productcategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, productcategory $productcategory,$id)
    {
        $productcat = productcategory::findOrfail($id);
        $request->validate([
            'name' => 'required',
        ]);
        $productcat->name = $request->name;
        if(file_exists($request->file('image'))){
            $image = "product-cat".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('uploads/products');
            $request->file('image')->move($location, $image);
            $productcat->image = $image;
        }
        else{
            $productcat->image = $productcat->image;
        }
        $productcat->save();
        return redirect('/home/product-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\productcategory  $productcategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productcat=productcategory::findOrFail($id)->delete();
        return redirect()->back();
    }
}
