<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductEvents extends Model
{
    protected $fillable=['product_id','event_id'];
}
