<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ethnicity extends Model
{
    protected $fillable = [
        'name','image',
    ];
}
