<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventsEthnics extends Model
{
    protected $fillable=[
    	'event_id','ethnic_id'
    ];
}
