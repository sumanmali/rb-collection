@extends('layouts.backend.user.app')

@section('content')

<!-- partial -->
<div class="main-panel">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-md-12 grid-margin">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <div class="d-sm-flex align-items-baseline report-summary-header">
                  <h5 class="font-weight-semibold">Order History</h5> <span class="ml-auto">Order History</span> <form action="javascript:history.go(0)"><button class="btn btn-icons border-0 p-2"><i class="icon-refresh"></i></button></form>
                </div>
              </div>
            </div>
            <div class="row report-inner-cards-wrapper">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <td>Order Id</td>
                    <td>Date Of Order</td>
                    <td>Product Name</td>
                    <td>Product Quantity</td>
                    <td>Product Price</td>
                  </tr>
                </thead> 
                <tbody>
                  @foreach($allOrders as $order)
                  <tr>
                    <td>{{$order->id}}</td>
                    <td>{{$order->created_at->format('d M Y')}}</td>
                    <td>
                      @foreach($prodName as $name)
                      @if($name->id == $order->product_id)
                      {{$name->name}}
                      @endif
                      @endforeach
                    </td>
                    <td>{{$order->product_quantity}}</td>
                    <td>{{$order->product_price}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="passwordChange" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div style="background: #ffff;" class="modal-body">
              <form class="form-horizontal" method="POST" action="/user/changePassword">
                @csrf
                <div class="form-group">
                                <label for="current_password" class="col-md-4 control-label">Current Password</label>

                                <div class="col-md-10">
                                    <input id="current_password" type="password" class="form-control" name="current_password" placeholder="Your Current Password" required>
                                </div>
                            </div>
                <div class="form-group">
                  <label for="new_password" class="col-md-4 control-label">New Password</label>

                  <div class="col-md-10">
                    <input id="new_password" type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" required autocomplete="new_password" placeholder="Password">

                                @error('new_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                  </div>
                </div>

                <div class="form-group">
                  <label for="password_confirmation" class="col-md-10 control-label">Confirm New Password</label>

                  <div class="col-md-10">
                    <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required autocomplete="password_confirmation" placeholder="Confirm Password">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                      Change Password
                    </button>
                  </div>
                </div>
              </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- content-wrapper ends -->
  @endsection