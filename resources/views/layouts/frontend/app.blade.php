
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Basic page needs ================================================== -->
  <meta charset="utf-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

  
  <link rel="shortcut icon" href="/images/site-logo.png" type="image/png" />
  

  <!-- Title and description ================================================== -->
  <title>
    RB Jewellery
  </title>
  
  <!-- Social meta ================================================== -->
  

  <meta property="og:type" content="website">
  <meta property="og:title" content="RB Jewellers">
  <meta property="og:url" content="">
  
  <meta property="og:image" content="">
  <meta property="og:image:secure_url" content="">
  
  <meta property="og:site_name" content="RB Jewellers">

  <meta name="twitter:card" content="summary">
  <!-- Helpers ================================================== -->
  <link rel="canonical" href="#">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="theme-color" content="#000000">

  <!-- CSS ================================================== -->
  <link href="{{asset('/css/timber.scss.css')}}" rel="stylesheet" type="text/css" media="all" />
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
  <link href="{{asset('/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />   
  
  <link href="{{asset('/css/settings.css')}}" rel="stylesheet" type="text/css" media="all" />
    
  
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato:300,300italic,400,600,400italic,600italic,700,700italic,800,800italic">
  
 <script src="{{asset('/js/load_feature-98ef862814fe2952ed0893b184775afe7f06464f1ff22ee18736b6431a6c6317.js')}}" type="text/javascript"></script>

 <script src="{{asset('/js/features-118a698fb45bb1e2a76ae81b3f81095d1e2c32ced33dd4e93e64378b5eb778d0.js')}}" type="text/javascript"></script>

  <meta id="shopify-digital-wallet" name="shopify-digital-wallet" content="/18119385/digital_wallets/dialog">
<script id="shopify-features" 
type="application/json">
{"accessToken":"5ae63d635341301eb227ed6539add8ca",
"betas":[],"domain":"huge-jewelry.myshopify.com",
"predictiveSearch":true,"shopId":18119385,
"smart_payment_buttons_url":"https:\/\/cdn.shopify.com\/shopifycloud\/payment-sheet\/assets\/latest\/spb.en.js",
"dynamic_checkout_cart_url":"https:\/\/cdn.shopify.com\/shopifycloud\/payment-sheet\/assets\/latest\/dynamic-checkout-cart.en.js",
"locale":"en"}
</script>
   
<link rel="stylesheet" media="screen" href="{{asset('/css/styles.css?3493')}}">

<script id="sections-script" data-sections="home-product-grid-type-3" defer="defer" src="{{asset('/js/scripts.js')}}"></script>

<script src="{{asset('/js/header.js')}}" type="text/javascript"></script> 

<script src="{{asset('/js/slider-init.js')}}" type="text/javascript"></script>

</head>

<body id="Rb Jewellers" class="template-index" >
  <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
    <div class="gf-menu-device-wrapper">
      <div class="close-menu">x</div>
      <div class="gf-menu-device-container"></div>
    </div>             
  </nav>
  <div class="wrapper-container"> 
    <div class="header-type-8">    
      <div id="SearchDrawer" class="search-bar drawer drawer--top search-bar-type-3">
        <div class="search-bar__table">
          <form action="/search" method="get" class="search-bar__table-cell search-bar__form" role="search">
            <div class="search-bar__table">
              <div class="search-bar__table-cell search-bar__icon-cell">
                <button type="submit" class="search-bar__icon-button search-bar__submit">
                  <span class="fa fa-search" aria-hidden="true"></span>
                </button>
              </div>
              <div class="search-bar__table-cell">
                <input type="search" id="SearchInput" name="q" value="" placeholder="Search..." aria-label="Search..." class="search-bar__input">
              </div>
            </div>
          </form>
          <div class="search-bar__table-cell text-right">
            <button type="button" class="search-bar__icon-button search-bar__close js-drawer-close">
              <span class="fa fa-times" aria-hidden="true"></span>
            </button>
          </div>
        </div>
      </div>
    @include('layouts.frontend.header')  
</div>   
@yield('content')    
</div>
@include('layouts.frontend.footer')

<script src="{{asset('/js/timber.js')}}" type="text/javascript"></script> 

<script src="{{asset('/js/theme.js')}}" type="text/javascript"></script>

<script src="{{asset('/js/option_selection-fe6b72c2bbdd3369ac0bfefe8648e3c889efca213baefd4cfb0dd9363563831f.js')}}" type="text/javascript"></script>

<script src="{{asset('/js/api.jquery-e94e010e92e659b566dbc436fdfe5242764380e00398907a14955ba301a4749f.js')}}" type="text/javascript"></script> 

<script src="{{asset('/js/footer.js')}}" type="text/javascript"></script>

<script src="{{asset('/js/shop.js')}}" type="text/javascript"></script> 

<script src="{{asset('/js/countdown.js')}}" type="text/javascript"></script>

<script src="{{asset('/js/jquery.elevatezoom.js')}}" type="text/javascript"></script>

<script src="{{asset('/js/jquery.fancybox.js')}}" type="text/javascript"></script>

<div class="clearfix" id="quickview-template" style="display:none">
  <div class="overlay"></div>
  <div class="content clearfix">
    <div class="product-img images">
      <div class="quickview-featured-image product-photo-container"></div>
      <div class="more-view-wrapper">
        <ul class="product-photo-thumbs quickview-more-views-owlslider owl-carousel owl-theme">
        </ul>
        <div class="quick-view-carousel"></div>
      </div>
    </div>
    <div class="product-shop summary">
      <div class="product-item product-detail-section">
        <h2 class="product-title"><a>&nbsp;</a></h2>

        <div class="prices product_price">
          <label>Effective Price:</label>
          <div class="price h6" id="QProductPrice"></div>
          <div class="compare-price" id="QComparePrice"></div>
        </div>
        
        <div class="product-infor">
          <p class="product-inventory"><label>Availability:</label><span></span></p>    
        </div>
        
        <div class="details clearfix">
          <form action="/cart/add" method="post" class="variants">
            <select name='id' style="display:none"></select>
            <div class="qty-section quantity-box">
              <label>Quantity:</label>
              <div class="dec button qtyminus">-</div>
              <input type="number" name="quantity" id="Qty" value="1" class="quantity">
              <div class="inc button qtyplus">+</div>
            </div>
     
            <div class="total-price">
              <label>Subtotal</label><span class="h5"></span>
            </div>

            <div class="actions">
              <button type="button" class="add-to-cart-btn btn">
                Add to Cart
              </button>
            </div>
          </form>
        </div>     
      </div>
    </div>  
    <a href="javascript:void(0)" class="close-window"></a> 
  </div>
</div>
<!-- End of quick-view-template -->
    <div class="loading-modal modal">Loading</div>

    <div class="ajax-error-modal modal">
      <div class="modal-inner">
        <div class="ajax-error-title">Error</div>
        <div class="ajax-error-message"></div>
      </div>
    </div>

    <div class="ajax-success-modal modal">
      <div class="overlay"></div>
      <div class="content"> 

        <p class="added-to-cart info">Added to cart</p>
        <p class="added-to-wishlist info">Added to wishlist</p>
        <div class="ajax-left">        
          <img class="ajax-product-image" alt="modal window" src="/" />
        </div>
        <div class="ajax-right"> 
          <h3 class="ajax-product-title">Product name</h3>
          <span class="ajax_price"></span>
          <div class="success-message added-to-cart"><a href="/cart" class="btn"><i class="fa fa-shopping-cart"></i>View Cart</a> </div>  
          <div class="success-message added-to-wishlist"> <a href="/pages/wishlist" class="btn"><i class="fa fa-heart"></i>View Wishlist</a></div>                
        </div>
        <a href="javascript:void(0)" class="close-modal"><i class="fa fa-times-circle"></i></a>
      </div>    
    </div>

    <script src="{{asset('/js/currencies.js')}}" type="text/javascript"></script>
    <script src="{{asset('/js/jquery.currencies.min.js?3493')}}" type="text/javascript"></script>


<script type="text/javascript">// <![CDATA[
jQuery(document).ready(function() {    //
  var $modalParent        = jQuery('div.newsletterwrapper'),
  modalWindow         = jQuery('#email-modal'),
  emailModal          = jQuery('#email-modal'),
  modalPageURL        = window.location.pathname; 

  modalWindow = modalWindow.html();
  modalWindow = '<div id="email-modal">' + modalWindow + '</div>';
  $modalParent.css({'position':'relative'});
  jQuery('.wrapper #email-modal').remove();
  $modalParent.append(modalWindow);
  
  if (jQuery.cookie('emailSubcribeModal') != 'closed') {
    openEmailModalWindow();
  };

  jQuery('#email-modal .btn.close').click(function(e) {
    e.preventDefault();
    closeEmailModalWindow();
  });
  jQuery('body').keydown(function(e) {
    if( e.which == 27) {
      closeEmailModalWindow();
      jQuery('body').unbind('keydown');
    }
  });
  jQuery('#mc_embed_signup form').submit(function() {
    if (jQuery('#mc_embed_signup .email').val() != '') {
      closeEmailModalWindow();
    }
  });

  function closeEmailModalWindow () {
    jQuery('#email-modal .modal-window').fadeOut(600, function() {
      jQuery('#email-modal .modal-overlay').fadeOut(600, function() {
        jQuery('#email-modal').hide();
        jQuery.cookie('emailSubcribeModal', 'closed', {expires:1, path:'/'});
      });
    })
  }
  function openEmailModalWindow () {
    jQuery('#email-modal').fadeIn(600, function() {
     jQuery('#email-modal .modal-window').fadeIn(600);
   });
  }

});
</script>
<div class="newsletterwrapper">
  <div id="email-modal" style="display: none;">
    <div class="modal-overlay"></div>
    <div class="modal-window" style="display: none;">
      <div class="window-window">
        <a class="btn close" title="Close Window"></a>
        <div class="window-content">
          <div class="newsletter-title">
           <h2 class="title">Newsletter Signup</h2>
         </div>
         <p class="message">Subscribe and get notified at first on the latest update and offers!</p>
         <div id="mailchimp-email-subscibe">
          <div id="mc_embed_signup">
            <form method="post" action="/contact#contact_form" id="contact_form" accept-charset="UTF-8" class="contact-form"><input type="hidden" name="form_type" value="customer" /><input type="hidden" name="utf8" value="✓" />
              <input type="email" value="" placeholder="Email address" name="contact[email]" class="mail" aria-label="Email address" >
              <input type="hidden" name="contact[tags]" value="newsletter">
              <button type="submit" class="btn" name="subscribe" value=""  id="subscribe" >Send</button>          
            </form>  
          </div>   
          <span>Note:we do not spam</span>
        </div>   
      </div>
    </div>
  </div>
</div>
</div>
<script src="{{asset('/js/wow.js?3493')}}" type="text/javascript"></script> 

<script src="{{asset('/js/classie.js?3493')}}"></script>

</body>
</html>


