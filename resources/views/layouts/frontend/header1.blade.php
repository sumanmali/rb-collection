
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
    <div class="gf-menu-device-wrapper">
      <div class="close-menu">x</div>
      <div class="gf-menu-device-container">
        <div class="menu-tool">  
          <div style="display: none !important;" class="container make_responsive_container">
            <ul class="site-nav gf-menu" style="display: none !important;">

              <li class=" ">
                <a href="/" class="current">
                  <span>         
                    Home          
                  </span>  

                </a>  

              </li>

              <li class=" dropdown  mega-menu"><p class="toogleClick">+</p>
                <a href="/collections" class="">
                  <span>         
                    Collection          
                  </span>  

                </a>  

                <div class="site-nav-dropdown">     
                 <div class="container   style_2"> 
                  <div class="col-1 parent-mega-menu">        

                    <?php
                    use App\Product;
                    use App\productcategory;
                    
                    $pCat = productcategory::latest()->get();
                    $count=1;
                    $countP=1;
                    ?>

                    @foreach($pCat as $pcat)
                    <div class="inner col-xs-12 col-sm-4">
                      <!-- Menu level 2 -->
                      <a  href="/products/{{$pcat->id}}" class="">
                        {{$pcat->name}} 

                      </a>
                      <?php 
                      $singleProducts = Product::where('cat_id',$pcat->id)->latest()->get();
                      ?>

                      <ul class="dropdown">

                        <!-- Menu level 3 -->
                        @foreach($singleProducts as $sprod)
                        <li>
                          <a href="/product-detail/{{$sprod->id}}" >
                           {{$sprod->name}}
                         </a>
                       </li>
                       <?php $countP ++; ?>
                       <?php if($countP > 5)
                       break
                       ?>
                       @endforeach            
                     </ul>
                   </div>
                   <?php $count ++; ?>
                   <?php if($count > 6)
                   break
                   ?>


                   @endforeach
                 </div>
                 <div class="col-2">
                  <div class="col-left col-sm-6">
                    <a href="" title="">
                      <img src="//cdn.shopify.com/s/files/1/1811/9385/files/dropdown_2_menu_image_1_2000x.jpg?v=1537514876" alt="">
                    </a>
                    <a href="" title="">
                      <img src="//cdn.shopify.com/s/files/1/1811/9385/files/dropdown_2_menu_image_2_2000x.jpg?v=1537514886" alt="">
                    </a>
                  </div>
                  <div class="col-right col-sm-6">
                    <a href="" title="">
                      <img src="//cdn.shopify.com/s/files/1/1811/9385/files/dropdown_2_menu_image_3_2000x.jpg?v=1537514897" alt="">
                    </a>
                  </div>
                </div>
              </li>
              <li>                    
                <a href="/about" class="">               
                  <span>               
                    About us                
                  </span>
                </a>
                <ul class="site-nav-dropdown" style="display: none;">
                </ul>
              </li>
              <li>                    
                <a href="/blogs" class="">               
                  <span>               
                    Blog                
                  </span>
                </a>
                <ul class="site-nav-dropdown" style="display: none;">
                </ul>
              </li>
              <li>                    
                <a href="/contact" class="">               
                  <span>               
                    Contact us                
                  </span>
                </a>
                <ul class="site-nav-dropdown" style="display: none;">
                </ul>
              </li>
            </ul>  
          </div>
        </div>
      </div>
    </div>             
  </nav>

  <div class="wrapper-container"> 
    <div class="header-type-8">    
      <div id="SearchDrawer" class="search-bar drawer drawer--top search-bar-type-3">
        <div class="search-bar__table">
          <form action="/search" method="get" class="search-bar__table-cell search-bar__form" role="search">
            <div class="search-bar__table">
              <div class="search-bar__table-cell search-bar__icon-cell">
                <button type="submit" class="search-bar__icon-button search-bar__submit">
                  <span class="fa fa-search" aria-hidden="true"></span>
                </button>
              </div>
              <div class="search-bar__table-cell">
                <input type="search" id="SearchInput" name="q" value="" placeholder="Search..." aria-label="Search..." class="search-bar__input">
              </div>
            </div>
          </form>
          <div class="search-bar__table-cell text-right">
            <button type="button" class="search-bar__icon-button search-bar__close js-drawer-close">
              <span class="fa fa-times" aria-hidden="true"></span>
            </button>
          </div>
        </div>
      </div>
      <header class="site-header">
        <div class="header-sticky">
          <div id="header-landing" class="sticky-animate">
            <div class="wrapper">
              <div class="grid--full site-header__menubar"> 
                <div class="h1 grid__item wide--one-sixth post-large--one-sixth large--one-sixth site-header__logo" itemscope itemtype="http://schema.org/Organization">
                  <a href="/">
                    <img class="normal-logo" src="/images/site-logo.png" alt="Huge Jewelry" itemprop="logo">
                  </a>
                </div>
                <div class="grid__item wide--five-sixths post-large--five-sixths large--five-sixths menubar-section">
                  <div id="shopify-section-navigation" class="shopify-section"><div class="desktop-megamenu">
                    <div class="header-sticky">
                      <div id="header-landing" class="sticky-animate">
                        <div class="nav-bar-mobile">
                          <nav class="nav-bar" role="navigation">
                            <div class="site-nav-dropdown_inner">
                              <div class="menu-tool">  
                                <div class="container">
                                  <ul class="site-nav">
                                    <li class=" ">
                                      <a  href="/" class="{{ Request::is('/') ? 'current' : '' }}">
                                        <span>         
                                          Home
                                        </span>  

                                      </a>
                                    </li>
                                    <li class=" ">
                                      <a  href="/about" class="{{ Request::is('about') ? 'current' : '' }}">
                                        <span>         
                                          About us          
                                        </span>  

                                      </a>
                                    </li>
                                    <li class=" dropdown  mega-menu">
                                      <a  href="/collections" class="{{ Request::is('collections') ? 'current' : '' }}">
                                        <span>         
                                          Collection          
                                        </span>  

                                      </a>  
                                      <div class="site-nav-dropdown">     
                                       <div class="container   style_2"> 
                                        <div class="col-1 parent-mega-menu">

                                          <?php
                                          $count=1;
                                          $countP=1;
                                          ?>

                                          @foreach($pCat as $pcat)

                                          <?php $proceed = 0;
                                          $products = Product::all();
                                          ?>
                                          @foreach($products as $product)
                                            @if($pcat->id == $product->cat_id)
                                              <?php $proceed = $proceed + 1; ?>
                                            @endif
                                          @endforeach
                                        @if($proceed) 

                                          <div class="inner col-xs-12 col-sm-4">
                                            <!-- Menu level 2 -->
                                            <a  href="/products/{{$pcat->id}}" class="">
                                              {{$pcat->name}} 

                                            </a>
                                            <?php 
                                            $singleProducts = Product::where('cat_id',$pcat->id)->latest()->get();
                                            ?>

                                            <ul class="dropdown">

                                              <!-- Menu level 3 -->
                                              @foreach($singleProducts as $sprod)
                                              <li>
                                                <a href="/product-detail/{{$sprod->id}}" >
                                                 {{$sprod->name}}
                                               </a>
                                             </li>
                                             <?php $countP ++; ?>
                                             <?php if($countP > 5)
                                             break
                                             ?>
                                             @endforeach            
                                           </ul>
                                         </div>
                                         <?php $proceed = 0; ?>
                                         @endif
                                         <?php $count ++; ?>
                                         <?php if($count > 6)
                                         break
                                         ?>


                                         @endforeach


                                       </div>
                                       <div class="col-2">
                                        <div class="col-left col-sm-6">
                                          <a href="" title="">
                                            <img class="small-image" src="/images/image5ad.jpg" alt="" />

                                          </a>
                                          <a href="" title="">
                                            <img class="small-image" src="/images/image2ad.jpg" alt=""/>

                                          </a>
                                        </div>
                                        <div style="text-align: left;" class="col-left col-sm-6">
                                          <a href="" title="">
                                            <img class="small-image" src="/images/image1ad.jpg" alt=""/>

                                          </a>
                                          <a href="" title="">
                                            <img class="small-image" src="/images/image4ad.jpg" alt="" />

                                          </a>
                                        </div>

                                      </div>


                                    </div>
                                  </div>                     




                                </li>
                                <li class="dropdown ethnics-dropdown">
                                  <a  href="/allethnics" class="{{ Request::is('allethnics') ? 'current' : '' }}">
                                    <span>
                                      Ethnics
                                    </span>

                                  </a>
                                  <div class="site-nav-dropdown">
                                    <div style="padding: 5px 0;" class="container">
                                      <div class="col-sm-12 ">

                                        <?php
                                        use App\ethnicity;


                                        $pCat = ethnicity::get();
                                        $count=1;
                                        $countP=1;
                                        ?>

                                        @foreach($pCat as $pcat)
                                        <div class="inner col-xs-12 col-sm-12">
                                          <!-- Menu level 2 -->
                                          <div class="ethnics-submenu"> <a  href="/ethnics/{{$pcat->id}}" class="">
                                            {{$pcat->name}}

                                          </a></div>


                                        </div>
                                        <?php $count ++; ?>
                                        <?php if($count > 6)
                                        break
                                        ?>


                                        @endforeach


                                      </div>


                                    </div>
                                  </div>




                                </li>
                                <li class=" ">
                                  <a  href="/blogs" class="{{ Request::is('blogs') ? 'current' : '' }}">
                                    <span>         
                                      Blog          
                                    </span>  

                                  </a>
                                </li> 
                                <li class=" ">
                                  <a  href="/contact" class="{{ Request::is('contact') ? 'current' : '' }}">
                                    <span>         
                                      Contact us          
                                    </span>  

                                  </a>
                                </li>

   <!--  <li class="  dropdown">
      <a class="menu__moblie"  href="/pages/about" class="">
        <span>         
          Pages          
        </span>  
        
      </a>  

      
         
        

      
      
      <ul class="site-nav-dropdown">
  
  <li >                    
    <a href="/pages/about" class="">               
      <span>               
        About us                
      </span>
      
    </a>
    <ul class="site-nav-dropdown">
      
    </ul>
  </li>
  
  <li >                    
    <a href="/pages/contact" class="">               
      <span>               
        Contact us                
      </span>
      
    </a>
    <ul class="site-nav-dropdown">
      
    </ul>
  </li>
  
  <li >                    
    <a href="/blogs/news" class="">               
      <span>               
        Blog                
      </span>
      
    </a>
    <ul class="site-nav-dropdown">
      
    </ul>
  </li>
  
</ul>
      
      

      

</li> -->

</ul>  
</div>
</div>
</div>
</nav>
</div>
</div>
</div>
</div>

</div>              
</div> 
<div id="shopify-section-header" class="shopify-section"><div class="menu-icon">           

  <div class="header-bar__module cart header_cart">               
    <!-- Mini Cart Start -->
    <div class="baskettop">
      <div class="wrapper-top-cart">
        <a href="/cart" id="ToggleDown" class="icon-cart-arrow">     
          <i class="fa fa-shopping-basket" aria-hidden="true"></i>
          <div class="detail">
            <div id="cartCount"> 
              @php
              

              $cartCount = Cart::getContent();
              echo $cartCount->count();
              @endphp
            </div>
          </div>
        </a> 
        <div id="slidedown-cart" style="display:none"> 
          <!--  <h3>Shopping cart</h3>-->
          
          @if(count($cartCount))
          <div class="has-items" style="display: block;">
        <ul class="mini-products-list">
@foreach($cartCount as $cartContent)
          <li class="item" id="cart-item-37510477518">
            <a href="/products/amor-solitaire-ring-mount?variant=37510477518" title="Amor Solitaire Ring Mount - 2.4 / Platinum / Silver" class="product-image">
              <img src="/uploads/products/{{ $cartContent->attributes[1] }}" alt="{{ $cartContent->name }}"></a>
              <div class="product-details">
                <a href="/cart/remove/{{$cartContent->id}}" title="Remove This Item" class="btn-remove1">
                  <i class="fa fa-times"></i>
                </a>
                  <p class="product-name">
                    <a href="/product-detail/{{ $cartContent->id}}">{{ $cartContent->name }}</a>
                  </p>
                  <div class="cart-collateral">{{ $cartContent->quantity }} x 
                    <span class="price"><span class="money">Rs.{{ $cartContent->price }}</span>
                  </span>
                </div>
              </div>
            </li>
      @endforeach
          </ul>
        <div class="summary">                
          <p class="total">
            <span class="label1">Cart total:</span>
            <span class="price"><span class="money">Rs.{{ Cart::getSubTotal() }}</span></span> 
          </p>
        </div>
        <div class="actions">
          <button class="btn" onclick="window.location='/checkout'"><i class="fa fa-check"></i>Check Out</button>
          <button class="btn text-cart" onclick="window.location='/cart'"><i class="fa fa-shopping-basket"></i>View Cart</button>
        </div>
      </div>
      @else
      <div class="no-items">
            <p>Your cart is currently empty!</p>
            <p class="text-continue"><a  href="javascript:void(0)">Continue shopping</a></p>
          </div>
          @endif
        </div>
      </div>
    </div> <!-- End Top Header -->                   
  </div> 
  

  
  <div class="header-search">
    <div class="header-search">
      <a href="/search" class="site-header__link site-header__search js-drawer-open-top">
        <span class="fa fa-search" aria-hidden="true"></span>
      </a>
    </div>
  </div>
  <ul class="menu_bar_right">
    <li>
      <div class="slidedown_section">
        <a  id="Togglemodal" class="icon-cart-arrow" title="account"><i class="fa fa-user"></i></a>
        <div id="slidedown-modal">
          <div class="header-panel-top">
            <ul>
              <li>
                <?php 
                $user = Auth::user();
                 ?>
                <div class="customer_account">                          
                  <ul>
                    @if($user)
                    <li><p>Welcome, {{$user->name}}</p></li>
                    <li><a href="/home">Dashboard</a></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    Sign Out
                    </a></li>
                    @else
                    <li>
                      <a href="/login" title="Log in">Log in</a>
                    </li>
                    <li>
                      <a href="/register" title="Create account">Create account</a>
                    </li>          
                    @endif
                 </ul>
               </div>    
             </li>
             
           </ul>
         </div>
       </div>
     </div>
   </li>
 </ul>

 <div class="header-mobile">
  <div class="menu-block visible-phone"><!-- start Navigation Mobile  -->
    <div id="showLeftPush">
      <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
  </div><!-- end Navigation Mobile  --> 
</div>

</div>



</div>
</div>
</div>
</div>
</div>
</header>
</div>