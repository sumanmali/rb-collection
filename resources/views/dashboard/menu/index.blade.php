@extends('layouts.backend.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		
		<div class="col-sm-12 col-sm-12">
			<h3>Menu</h3>
			<a class="btn btn-primary" href="{{url('home/menu/create')}}">Create Menu</a>
			<table class="table table-hover table-menu">
				<thead>
					<th>Menu Id</th>
					<th>Menu Name</th>
					<th>Action</th>
				</thead>
				@foreach($menu as $menuitem)
				<tbody>
					<td>{{$menuitem->id}}</td>
					<td>{{$menuitem->menuName}}</td>
					<td>
						<a class="btn btn-info btn-sm" href="menu/edit/{{$menuitem->id}}">Edit</a><br>
						<a class="btn btn-danger btn-sm" href="menu/delete/{{$menuitem->id}}" onclick="deleteAction(event)">Delete</a>
					</td>
				</tbody>
				@endforeach
			</table>
		</div>
	</div>


@endsection