@extends('layouts.backend.app')

@section('content')
<div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> All Sliders </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Sliders</li>
                </ol>
              </nav>
            </div>
            <a style="margin-bottom: 10px;" href="/home/slider/create" class="btn btn-primary">Add New Slider</a>
              <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <!-- DATA TABLE-->
		<div class="table-responsive m-b-40">
			<table class="table table-borderless table-data3">
				<div class="container">
			<thead>
				<tr><th>Id</th>
					<th>Name</th>
					<th>Image</th>
					<th>Title</th>
					<th>Description</th>
					<th>Action</th>
				</tr>
				</thead>
			<tbody>
				@foreach($sliders as $slider)
				<tr>
				<td>{{ $loop->iteration }}</td>
				<td>{{$slider->name}}</td>
				<td>
					<img class="slider-image" src="/uploads/{{$slider->image}}" alt="{{$slider->name}}">
				</td>
				<td>{{$slider->title}}</td>
				<td><?php echo ($slider->description )?></td>
				<td><a href="{{route('slider.edit', $slider->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
				<form method="post" action="{{route('slider.delete',$slider->id)}}">
					@csrf
					{{ method_field('DELETE') }}
					<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
				</form>
				</td>
			</tr>
				@endforeach
			</tbody>
		</div>
			</table>
		</div>
		<!-- END DATA TABLE-->
	</div>
</div>
</div>
</div>

@endsection