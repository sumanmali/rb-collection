  @extends('layouts.backend.app')

  @section('content')
  <div class="main-panel">
    <div class="content-wrapper">
      <div class="page-header">
        <h3 class="page-title"> All Product Videos </h3>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Product Videos</li>
          </ol>
        </nav>
      </div>
      <!-- <a style="margin-bottom: 10px;" href="/home/material/create" class="btn btn-primary">Add New Material</a> -->
      <button style="margin: 10px;padding: 15px 53px" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Video</button>
      <form style="margin: 10px" method="post" action="/home/productVideo/deleteall">
       @csrf
       {{ method_field('DELETE') }}
       <button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete All Videos</button>
     </form>
     <!-- Modal -->
     <div id="myModal" class="modal fade" role="dialog">
      <div style="max-width: 900px !important;" class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Create New Video</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
           <div class="col-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <!-- <h4 class="card-title"></h4> -->
                <p class="card-description"> Please fill the below forms to add a new Video. [ * : required ] </p>
                @if($errors->any())
                @foreach($errors->all() as $error)
                <ul>
                  <li>{{$error}}</li>
                </ul>
                @endforeach
                @endif
                <form action="/home/productVideo/create" method="POST"  class="forms-sample" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group">
                    <label for="video_name">video Title</label>
                    <input type="text" class="form-control" name="video_name" id="name" placeholder="Video Name">
                  </div>
                  <div class="form-group">
                    <label for="category">Choose Products</label><br>
                    <div class="has-many-cat row">
                      @foreach($productcat as $vcat)
                      <div class="form-check select-tab-view col-sm-5">
                        <label class="form-check-label select-tab-view">
                          <input type="checkbox" class="form-check-input checkbox ethnicCheckbox" name="product_id[]" value="{{$vcat->id}}"> {{$vcat->name}}
                        </label>
                      </div>
                      @endforeach
                    </div>
                  </div>
                  <div class="row form-group">
                    <div class="col col-md-12">
                      <label for="video" class=" form-control-label">Video input*</label>
                    </div>
                    <div class="col-12 col-md-12">
                      <input type="file" name="video" class="form-control-file file_multi_video" accept="video/mp4,video/avi,video/mpeg,video/MKV">

                      <video style="margin-top: 20px;" width="100%" controls>
                        <source src="#" id="video_here">
                          Your browser does not support HTML5 video.
                        </video>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <!-- <button type="reset" class="btn btn-danger mr-2"><i class="fa fa-ban"></i> Reset</button> -->
                    <button type="reset" class="btn btn-light">Reset</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
    <div class="col-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <!-- DATA TABLE-->
          <div class="table-responsive m-b-40">
           <table class="table table-borderless table-data3">
            <div class="container">
             <thead>
              <tr><th>Id</th>
               <th>Video Name</th>
               <th>video</th>
               <th>Products</th>
               <th>Action</th>
             </tr>
           </thead>
           <tbody>
            @foreach($productVideo as $pvideo)
            <tr>
              <td>{{ $loop->iteration }}</td>
              <td>{{$pvideo->video_name}}</td>
              <td>
                <video style="" width="70%" controls>
                  <source src="/uploads/video/{{$pvideo->video}}">
                    Your browser does not support HTML5 video.
                  </video>
                </td>
                <td>
                  <ul>
                    @foreach($productcat as $videoprod)
                    @if($videoprod->id == $pvideo->product_id)
                    <li>
                      {{$videoprod->name}}
                    </li>
                    @endif
                    @endforeach
                  </ul>
                </td>
                <td><a href="{{route('productVideo.edit', $pvideo->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>|
                  <form method="post" action="{{route('productVideo.delete',$pvideo->id)}}">
                   @csrf
                   {{ method_field('DELETE') }}
                   <button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
                 </form>
               </td>
             </tr>
             @endforeach
           </tbody>
         </div>
       </table>
     </div>
     <!-- END DATA TABLE-->
   </div>
 </div>
</div>
</div>
<script type='text/javascript'>
 $(document).ready(function(){

   // Changing state of CheckAll checkbox 
   $(".checkbox").change(function(){
    var checkAll = $('#checkall').is(':checked');
    var checkBox = $('.ethnicCheckbox').is(':checked');
    if(checkAll){
     if(checkBox){
       $("#checkall").prop("checked", false);
     }
   }
   
   if($(".checkbox").length == $(".checkbox:checked").length) {
    $("#checkall").prop("checked", true);
  } else {
    $("#checkall").removeAttr("checked");
  }
});

   // Check or Uncheck All checkboxes
   $("#checkall").change(function(){
     var checked = $(this).is(':checked');
     if(checked){
       $(".checkbox").each(function(){
         $(this).prop("checked",true);
       });
     }else{
       $(".checkbox").each(function(){
         $(this).prop("checked",false);
       });
     }
   });


 });
</script>

@endsection