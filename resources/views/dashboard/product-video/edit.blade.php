@extends('layouts.backend.app')
@inject('checkSelected','App\Services\Ethnics')
@section('content')
<!-- partial -->
<div class="main-panel">
  <div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title"> Edit Events </h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/home/events">Events</a></li>
          <li class="breadcrumb-item active" aria-current="page">Edit Events</li>
        </ol>
      </nav>
    </div>
    <div class="col-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Edit Events Title</h4>
          <p class="card-description"> Please fill the below forms to edit the Events. [ * : required ] </p>
          @if($errors->any())
          @foreach($errors->all() as $error)
          <ul>
            <li>{{$error}}</li>
          </ul>
          @endforeach
          @endif
          <form action="/home/event/edit/{{$events->id}}" method="POST"  class="forms-sample" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="name">Name*</label>
              <input type="text" class="form-control" name="name" id="name" value="{{$events->name}}">
            </div>
            <div class="form-group">
              <label for="category">Product Ethnicity*</label><br>

              @foreach($ethnicities as $allethnicity)

              
              <div class="form-check select-tab-view">
                <label class="form-check-label select-tab-view">
 
                  <input type="checkbox" class="form-check-input checkbox ethnicCheckbox"
                   {{$checkSelected->findCheckedEthnics($events->id,$allethnicity->id)}} 
                   name="ethnicity[]" value="{{$allethnicity->id}}"> {{$allethnicity->name}}
                  
                </label>
              </div>
          
              @endforeach

             
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-12">
                  <label>Image upload*</label>
                  <div class="input-group">
                    <input type="file" name="image" class="form-control file-upload-info" onchange="readURL(this);" accept="image/png, image/jpg, image/jpeg" placeholder="Upload Image">
                    <img style="max-width: 50%;" id="load-img" class="file-upload-info" src="/uploads/products/events/{{$events->image}}" alt="{{$events->name}}" />
                    <span class="input-group-append">
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary mr-2">Submit</button>
            <!-- <button type="reset" class="btn btn-danger mr-2"><i class="fa fa-ban"></i> Reset</button> -->
            <button type="reset" class="btn btn-light">Reset</button>
          </form>
        </div>
      </div>
    </div>
  </div>
  @endsection