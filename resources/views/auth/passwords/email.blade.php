@extends('layouts.app')

@section('content')
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <div class="brand-logo"><a href="/">
                  <img style="width: 22%;" src="/images/small-site.png">
                  </a>
                </div>
                <h4>Hello! let's Reset Your Password</h4>
                <h6 class="font-weight-light">Enter your email to continue.</h6>
                <form class="pt-3" method="POST" action="{{ route('password.email') }}">
                        @csrf
                  <div class="form-group">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-Mail Address">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                  </div>
                  <div class="mt-3">
                    <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                  </div>
                  <div class="text-center mt-4 font-weight-light"> Don't have an account?  <a class="text-primary" href="{{ route('register') }}">{{ __('Register') }}</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
@endsection
