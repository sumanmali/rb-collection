@extends('layouts.frontend.app')

@section('content')
<nav class="breadcrumb" aria-label="breadcrumbs">
  
  <h1>404 Not Found</h1>
  <a href="/" title="Back to the frontpage">Home</a>

  <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
  <span>404 Not Found</span>
  
</nav>    
      <div class="dt-sc-hr-invisible-large"></div>
      <div class="container-bg">
        
        <div class="grid__item">         
          
<h1>404 Page Not Found</h1>
<p>The page you requested does not exist. Click <a href="/collections">here</a> to continue shopping.</p>
		          
        </div>       
        
      </div>
      
      
      <div class="dt-sc-hr-invisible-large"></div>
      
    </main>
  </div>
@endsection
