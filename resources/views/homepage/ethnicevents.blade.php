@extends('layouts.frontend.app')

@section('content')
<main class="main-content"> 




 <nav class="breadcrumb" aria-label="breadcrumbs">


  @if($ethnicName)

  <h1>{{$ethnicName->name}}</h1>
  <a href="/" title="Back to the frontpage">Home</a>
  <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>

  <span>{{$ethnicName->name}}</span>
  <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>

  <span>Events</span>
  @endif

</nav>




  <div class="dt-sc-hr-invisible-large"></div>
  <div class="container-bg">

    @if(count($allEvents))
    <div class="grid__item">         
      <div class="grid-uniform list-collection-products">  

        @foreach($allEvents as $ethnic)



        <div class="grid__item grid__item wide--one-third post-large--one-third large--one-third medium--one-half small--grid__item text-center pickgradient-products">


          <a href="/ethnic/{{$ethnic->id}}" title="{{$ethnic->name}}" class="pickgradient grid-link">

            <img src="/uploads/products/events/{{$ethnic->image}}" alt="{{$ethnic->name}}" />

            <div class="dt-sc-event-overlay">

              <p class="collection-count">
                <?php $count = 0;?>
               @foreach($alleth as $ethnics)
                @if($ethnic->id == $ethnics->event_id)
                <?php $count = $count + 1; ?>
                @endif
                @endforeach
                {{ $count }}
                <?php $count = 0; ?>
                <span>Items</span>
              </p>


            </div>

          </a>


          <a href="/ethnic/{{$ethnic->id}}" title="Browse our Rare collections collection" class="grid-link">

            <span class="grid-link__title">{{$ethnic->name}}</span></a>

          </div>


          @endforeach

        </div>              
      </div>
      @else
      <div class="text-center">
      <h3>No Events</h3>
      <p>Selected Ethnicity is empty!</p>
      <a href="/allethnics"><i class="fa fa-angle-left" aria-hidden="true"></i> All Ethnicity</a><br>
      <a href="/allevents"><i class="fa fa-angle-left" aria-hidden="true"></i> All Events</a>
    </div>
      @endif   

    </div>


    <div class="dt-sc-hr-invisible-large"></div>

  </main>
  @endsection