@extends('layouts.frontend.app')

@section('content')
<main class="main-content"> 




  <nav class="breadcrumb" aria-label="breadcrumbs">



    <h1>Contact</h1>
    <a href="/" title="Back to the frontpage">Home</a>

    <span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
    <span>Contact</span>


  </nav>
   <div class="grid__item">         
    <div class="contact-address">
      <div class="wrapper">
        <div class="dt-sc-hr-invisible-large"></div>
        <div class="grid__item">
          <div class="container">
            <ul>
              <li class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half">
                <div class="icon-wrapper">
                  <div class="icon">
                    <i class="fa fa-phone"></i> 
                  </div>
                  <h4>Talk to us</h4>
                  <p>  <b>Toll-Free: </b> 0803 - 080 - 3081 <br><b>Fax: </b> 0803 - 080 - 3082</p><mark></mark> 
                </div>
              </li>
              <li class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half">
               <div class="icon-wrapper">
                <div class="icon">
                  <i class="fa fa-envelope"></i>
                </div>
                <h4>Contact Us</h4>
                <p> 
                  <a title="" href="">buddhathemes@somemail.com</a><br><a title="" href="">support@somemail.com</a>
                </p> 
              </div>
            </li>
            <li class="address grid__item wide--one-third post-large--one-third large--one-third medium--one-half"> 
             <div class="icon-wrapper">
              <div class="icon">
               <i class="fa fa-location-arrow"></i>
             </div> 
             <h4>Location</h4>
             <p> No: 58 A, East Madison Street,<br /> Baltimore, MD, USA
              4508<br /></p><mark></mark> 
            </div>
          </li>
       </ul>
       </div>
    </div>
    <div class="dt-sc-hr-invisible-large"></div>
    <div id="map"><iframe style="border:0;overflow:hidden;" src="https://maps.google.co.in/?ie=UTF8&t=m&ll=-37.823006,144.977388&spn=0.02034,0.042915&z=14&output=embed" width="960" height="500"></iframe> </div>
    <div class="dt-sc-hr-invisible-large"></div>
    <div class="grid__item">
      <div class="container">
        <div class="contact-form-section">
          <form method="post" action="/contact#contact_form" id="contact_form" accept-charset="UTF-8" class="contact-form"><input type="hidden" name="form_type" value="contact" /><input type="hidden" name="utf8" value="✓" />
            <label for="ContactFormName" class="label--hidden">Name</label>
            <input type="text" id="ContactFormName" name="contact[name]" placeholder="Name" autocapitalize="words" value="">
            <label for="ContactFormEmail" class="label--hidden">Email</label>
            <input type="email" id="ContactFormEmail" name="contact[email]" placeholder="Email" autocorrect="off" autocapitalize="off" value="">
            <label for="ContactFormSub" class="label--hidden">translation missing: en.contact.form.phone</label>
            <input type="text" id="ContactFormSub" name="contact[subject]" placeholder="Subject" autocapitalize="words" value="">
            <label for="ContactFormMessage" class="label--hidden">Message</label>
            <textarea rows="7" id="ContactFormMessage" name="contact[body]" placeholder="Message"></textarea>
            <button type="submit" class="btn">Send</button>
          </form>
        </div>
       <div class="contact-line"></div>
      </div>
    </div>
  </div>
  <div class="grid__item">
    <div class="container">
      <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item"> 
       <div class="grid-uniform social-icon-wrapper">
         <ul class="social-icon">
            <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
           <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
           <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
          </ul>
        </div>
     </div>
      <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item"> 
       <div class="contact-number">
         <p>contact us <span>+(11)123456789</span></p>
       </div>





     </div>
     <div class="dt-sc-hr-invisible-large"></div>
   </div>
 </div>



</div>
</div>






</div>       


</main>
@endsection