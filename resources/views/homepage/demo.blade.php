
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js"> <!--<![endif]-->
<head>

  <!-- Basic page needs ================================================== -->
  <meta charset="utf-8">
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

  
  <link rel="shortcut icon" href="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/favicon.png?3493" type="image/png" />
  

  <!-- Title and description ================================================== -->
  <title>
    Huge Jewelry
  </title>
  
  <!-- Social meta ================================================== -->
  

  <meta property="og:type" content="website">
  <meta property="og:title" content="Huge Jewelry">
  <meta property="og:url" content="https://huge-jewelry.myshopify.com/">
  
  <meta property="og:image" content="http://cdn.shopify.com/s/files/1/1811/9385/t/7/assets/logo.png?3493">
  <meta property="og:image:secure_url" content="https://cdn.shopify.com/s/files/1/1811/9385/t/7/assets/logo.png?3493">
  


<meta property="og:site_name" content="Huge Jewelry">



<meta name="twitter:card" content="summary">





  <!-- Helpers ================================================== -->
  <link rel="canonical" href="https://huge-jewelry.myshopify.com/">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="theme-color" content="#000000">

  <!-- CSS ================================================== -->
  <link href="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/timber.scss.css?3493" rel="stylesheet" type="text/css" media="all" />
  <link href="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/font-awesome.min.css?3493" rel="stylesheet" type="text/css" media="all" />
  <link href="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/style.css?3493" rel="stylesheet" type="text/css" media="all" />   
  
  <link href="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/settings.css?3493" rel="stylesheet" type="text/css" media="all" />
  
  
  
  
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lato:300,300italic,400,600,400italic,600italic,700,700italic,800,800italic">
  

  <!-- Header hook for plugins ================================================== -->
  <script>window.performance && window.performance.mark && window.performance.mark('shopify.content_for_header.start');</script><meta id="shopify-digital-wallet" name="shopify-digital-wallet" content="/18119385/digital_wallets/dialog">
<script id="shopify-features" type="application/json">{"accessToken":"5ae63d635341301eb227ed6539add8ca","betas":[],"domain":"huge-jewelry.myshopify.com","predictiveSearch":true,"shopId":18119385,"smart_payment_buttons_url":"https:\/\/cdn.shopify.com\/shopifycloud\/payment-sheet\/assets\/latest\/spb.en.js","dynamic_checkout_cart_url":"https:\/\/cdn.shopify.com\/shopifycloud\/payment-sheet\/assets\/latest\/dynamic-checkout-cart.en.js"}</script>
<script>var Shopify = Shopify || {};
Shopify.shop = "huge-jewelry.myshopify.com";
Shopify.currency = {"active":"USD","rate":"1.0"};
Shopify.theme = {"name":"jewelry","id":40281145393,"theme_store_id":null,"role":"main"};
Shopify.theme.handle = "null";
Shopify.theme.style = {"id":null,"handle":null};</script>
<script type="module">!function(o){(o.Shopify=o.Shopify||{}).modules=!0}(window);</script>
<script>!function(o){function n(){var o=[];function n(){o.push(Array.prototype.slice.apply(arguments))}return n.q=o,n}var t=o.Shopify=o.Shopify||{};t.loadJS=n(),t.detectLoadJS=n()}(window);</script>
<script>(function() {
  function asyncLoad() {
    var urls = ["\/\/productreviews.shopifycdn.com\/assets\/v4\/spr.js?shop=huge-jewelry.myshopify.com"];
    for (var i = 0; i < urls.length; i++) {
      var s = document.createElement('script');
      s.type = 'text/javascript';
      s.async = true;
      s.src = urls[i];
      var x = document.getElementsByTagName('script')[0];
      x.parentNode.insertBefore(s, x);
    }
  };
  if(window.attachEvent) {
    window.attachEvent('onload', asyncLoad);
  } else {
    window.addEventListener('load', asyncLoad, false);
  }
})();</script>
<script id="__st">var __st={"a":18119385,"offset":-18000,"reqid":"5def1f25-7134-4a22-bbfc-4a7050d64376","pageurl":"huge-jewelry.myshopify.com\/","u":"8917f21f780c","p":"home"};</script>
<script>window.ShopifyPaypalV4VisibilityTracking = true;</script>
<script>window.ShopifyAnalytics = window.ShopifyAnalytics || {};
window.ShopifyAnalytics.meta = window.ShopifyAnalytics.meta || {};
window.ShopifyAnalytics.meta.currency = 'USD';
var meta = {"page":{"pageType":"home"}};
for (var attr in meta) {
  window.ShopifyAnalytics.meta[attr] = meta[attr];
}</script>
<script>window.ShopifyAnalytics.merchantGoogleAnalytics = function() {
  
};
</script>
<script class="analytics">(function () {
  var customDocumentWrite = function(content) {
    var jquery = null;

    if (window.jQuery) {
      jquery = window.jQuery;
    } else if (window.Checkout && window.Checkout.$) {
      jquery = window.Checkout.$;
    }

    if (jquery) {
      jquery('body').append(content);
    }
  };

  var isDuplicatedThankYouPageView = function() {
    return document.cookie.indexOf('loggedConversion=' + window.location.pathname) !== -1;
  }

  var setCookieIfThankYouPage = function() {
    if (window.location.pathname.indexOf('/checkouts') !== -1 &&
        window.location.pathname.indexOf('/thank_you') !== -1) {

      var twoMonthsFromNow = new Date(Date.now());
      twoMonthsFromNow.setMonth(twoMonthsFromNow.getMonth() + 2);

      document.cookie = 'loggedConversion=' + window.location.pathname + '; expires=' + twoMonthsFromNow;
    }
  }

  var trekkie = window.ShopifyAnalytics.lib = window.trekkie = window.trekkie || [];
  if (trekkie.integrations) {
    return;
  }
  trekkie.methods = [
    'identify',
    'page',
    'ready',
    'track',
    'trackForm',
    'trackLink'
  ];
  trekkie.factory = function(method) {
    return function() {
      var args = Array.prototype.slice.call(arguments);
      args.unshift(method);
      trekkie.push(args);
      return trekkie;
    };
  };
  for (var i = 0; i < trekkie.methods.length; i++) {
    var key = trekkie.methods[i];
    trekkie[key] = trekkie.factory(key);
  }
  trekkie.load = function(config) {
    trekkie.config = config;
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.onerror = function(e) {
      (new Image()).src = '//v.shopify.com/internal_errors/track?error=trekkie_load';
    };
    script.async = true;
    script.src = 'https://cdn.shopify.com/s/javascripts/tricorder/trekkie.storefront.min.js?v=2019.11.04.1';
    var first = document.getElementsByTagName('script')[0];
    first.parentNode.insertBefore(script, first);
  };
  trekkie.load(
    {"Trekkie":{"appName":"storefront","development":false,"defaultAttributes":{"shopId":18119385,"isMerchantRequest":null,"themeId":40281145393,"themeCityHash":"4764226070704452619","contentLanguage":"en","currency":"USD"}},"Performance":{"navigationTimingApiMeasurementsEnabled":true,"navigationTimingApiMeasurementsSampleRate":1},"Session Attribution":{}}
  );

  var loaded = false;
  trekkie.ready(function() {
    if (loaded) return;
    loaded = true;

    window.ShopifyAnalytics.lib = window.trekkie;
    

    var originalDocumentWrite = document.write;
    document.write = customDocumentWrite;
    try { window.ShopifyAnalytics.merchantGoogleAnalytics.call(this); } catch(error) {};
    document.write = originalDocumentWrite;
      (function () {
        if (window.BOOMR && (window.BOOMR.version || window.BOOMR.snippetExecuted)) {
          return;
        }
        window.BOOMR = window.BOOMR || {};
        window.BOOMR.snippetStart = new Date().getTime();
        window.BOOMR.snippetExecuted = true;
        window.BOOMR.snippetVersion = 12;
        window.BOOMR.shopId = 18119385;
        window.BOOMR.themeId = 40281145393;
        window.BOOMR.url =
          "https://cdn.shopify.com/shopifycloud/boomerang/boomerang-latest.min.js";
        var where = document.currentScript || document.getElementsByTagName("script")[0];
        if (!where || !where.parentNode){
          return;
        }
        var promoted = false;
        var LOADER_TIMEOUT = 3000;
        function promote() {
          if (promoted) {
            return;
          }
          var script = document.createElement("script");
          script.id = "boomr-scr-as";
          script.src = window.BOOMR.url;
          script.async = true;
          where.parentNode.appendChild(script);
          promoted = true;
        }
        function iframeLoader(wasFallback) {
          promoted = true;
          var dom, bootstrap, iframe, iframeStyle;
          var doc = document;
          var win = window;
          window.BOOMR.snippetMethod = wasFallback ? "if" : "i";
          bootstrap = function(parent, scriptId) {
            var script = doc.createElement("script");
            script.id = scriptId || "boomr-if-as";
            script.src = window.BOOMR.url;
            BOOMR_lstart = new Date().getTime();
            parent = parent || doc.body;
            parent.appendChild(script);
          };
          if (!window.addEventListener && window.attachEvent && navigator.userAgent.match(/MSIE [67]./)) {
            window.BOOMR.snippetMethod = "s";
            bootstrap(where.parentNode, "boomr-async");
            return;
          }
          iframe = document.createElement("IFRAME");
          iframe.src = "about:blank";
          iframe.title = "";
          iframe.role = "presentation";
          iframe.loading = "eager";
          iframeStyle = (iframe.frameElement || iframe).style;
          iframeStyle.width = 0;
          iframeStyle.height = 0;
          iframeStyle.border = 0;
          iframeStyle.display = "none";
          where.parentNode.appendChild(iframe);
          try {
            win = iframe.contentWindow;
            doc = win.document.open();
          } catch (e) {
            dom = document.domain;
            iframe.src = "javascript:var d=document.open();d.domain='" + dom + "';void(0);";
            win = iframe.contentWindow;
            doc = win.document.open();
          }
          if (dom) {
            doc._boomrl = function() {
              this.domain = dom;
              bootstrap();
            };
            doc.write("<body onload='document._boomrl();'>");
          } else {
            win._boomrl = function() {
              bootstrap();
            };
            if (win.addEventListener) {
              win.addEventListener("load", win._boomrl, false);
            } else if (win.attachEvent) {
              win.attachEvent("onload", win._boomrl);
            }
          }
          doc.close();
        }
        var link = document.createElement("link");
        if (link.relList &&
          typeof link.relList.supports === "function" &&
          link.relList.supports("preload") &&
          ("as" in link)) {
          window.BOOMR.snippetMethod = "p";
          link.href = window.BOOMR.url;
          link.rel = "preload";
          link.as = "script";
          link.addEventListener("load", promote);
          link.addEventListener("error", function() {
            iframeLoader(true);
          });
          setTimeout(function() {
            if (!promoted) {
              iframeLoader(true);
            }
          }, LOADER_TIMEOUT);
          BOOMR_lstart = new Date().getTime();
          where.parentNode.appendChild(link);
        } else {
          iframeLoader(false);
        }
        function boomerangSaveLoadTime(e) {
          window.BOOMR_onload = (e && e.timeStamp) || new Date().getTime();
        }
        if (window.addEventListener) {
          window.addEventListener("load", boomerangSaveLoadTime, false);
        } else if (window.attachEvent) {
          window.attachEvent("onload", boomerangSaveLoadTime);
        }
        if (document.addEventListener) {
          document.addEventListener("onBoomerangLoaded", function(e) {
            e.detail.BOOMR.init({});
            e.detail.BOOMR.t_end = new Date().getTime();
          });
        } else if (document.attachEvent) {
          document.attachEvent("onpropertychange", function(e) {
            if (!e) e=event;
            if (e.propertyName === "onBoomerangLoaded") {
              e.detail.BOOMR.init({});
              e.detail.BOOMR.t_end = new Date().getTime();
            }
          });
        }
      })();
    

    if (!isDuplicatedThankYouPageView()) {
      setCookieIfThankYouPage();
      
        window.ShopifyAnalytics.lib.page(
          null,
          {"pageType":"home"}
        );
      
      
    }
  });

  
      var eventsListenerScript = document.createElement('script');
      eventsListenerScript.async = true;
      eventsListenerScript.src = "//cdn.shopify.com/s/assets/shop_events_listener-17b815ecd2d75d5d3ec1b7a2a59daadee017bd9097e9b4629937b0a78cf0ecaa.js";
      document.getElementsByTagName('head')[0].appendChild(eventsListenerScript);
    
})();</script>
<script integrity="sha256-/LWbHGRT9fhJCeTFZxJJr7GGGJRbAOrw4xIjESlEc8I=" crossorigin="anonymous" data-source-attribution="shopify.loadjs" defer="defer" src="//cdn.shopify.com/s/assets/storefront/load_js-fcb59b1c6453f5f84909e4c5671249afb18618945b00eaf0e3122311294473c2.js"></script>
<script integrity="sha256-2P0MRbAT3p4Oh8olbuAvRl44EikliFx94nnWg4+R+mo=" data-source-attribution="shopify.dynamic-checkout" defer="defer" src="//cdn.shopify.com/s/assets/storefront/features-d8fd0c45b013de9e0e87ca256ee02f465e38122925885c7de279d6838f91fa6a.js" crossorigin="anonymous"></script>
<link rel="stylesheet" media="screen" href="//cdn.shopify.com/s/files/1/1811/9385/t/7/compiled_assets/styles.css?3493">
<script id="sections-script" data-sections="home-product-grid-type-3" defer="defer" src="//cdn.shopify.com/s/files/1/1811/9385/t/7/compiled_assets/scripts.js?3493"></script>

<script>window.performance && window.performance.mark && window.performance.mark('shopify.content_for_header.end');</script>
  

<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js" type="text/javascript"></script>
<script src="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/respond.min.js?3493" type="text/javascript"></script>
<link href="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/respond-proxy.html" id="respond-proxy" rel="respond-proxy" />
<link href="//huge-jewelry.myshopify.com/search?q=e9f7c25edc57908987f1cb9196e3fda1" id="respond-redirect" rel="respond-redirect" />
<script src="//huge-jewelry.myshopify.com/search?q=e9f7c25edc57908987f1cb9196e3fda1" type="text/javascript"></script>
<![endif]-->



  <script src="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/header.js?3493" type="text/javascript"></script>     
  
  <script>
    window.use_sticky = true;
    window.ajax_cart = true;
    window.money_format = "<span class=money>${{amount}} USD</span>";
    window.shop_currency = "USD";
    window.show_multiple_currencies = true;
    window.enable_sidebar_multiple_choice = true;
    window.loading_url = "//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/loading.gif?3493";     
    window.dropdowncart_type = "hover";
    window.file_url = "//cdn.shopify.com/s/files/1/1811/9385/files/?3493";
    window.asset_url = "";
    window.items="Items";
    window.many_in_stock="Many In Stock";
    window.out_of_stock=" Out of stock";
    window.in_stock=" In Stock";
    window.unavailable="Unavailable";
    window.use_color_swatch = true;
  </script>
</head>

<body id="huge-jewelry" class="template-index" >
  <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
    <div class="gf-menu-device-wrapper">
      <div class="close-menu">x</div>
      <div class="gf-menu-device-container"></div>
    </div>             
  </nav>
  <div class="wrapper-container"> 
    <div class="header-type-8">    
      <div id="SearchDrawer" class="search-bar drawer drawer--top search-bar-type-3">
  <div class="search-bar__table">
    <form action="/search" method="get" class="search-bar__table-cell search-bar__form" role="search">
      
       
      <div class="search-bar__table">
        <div class="search-bar__table-cell search-bar__icon-cell">
          <button type="submit" class="search-bar__icon-button search-bar__submit">
            <span class="fa fa-search" aria-hidden="true"></span>
          </button>
        </div>
        <div class="search-bar__table-cell">
          <input type="search" id="SearchInput" name="q" value="" placeholder="Search..." aria-label="Search..." class="search-bar__input">
        </div>
      </div>
    </form>
    <div class="search-bar__table-cell text-right">
      <button type="button" class="search-bar__icon-button search-bar__close js-drawer-close">
        <span class="fa fa-times" aria-hidden="true"></span>
      </button>
    </div>
  </div>
</div>
      <header class="site-header">
        <div class="header-sticky">
          <div id="header-landing" class="sticky-animate">
            <div class="wrapper">
              <div class="grid--full site-header__menubar"> 
                
<h1 class="grid__item wide--one-sixth post-large--one-sixth large--one-sixth site-header__logo" itemscope itemtype="http://schema.org/Organization">
  
    
    <a href="/">
      <img class="normal-logo" src="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/logo.png?3493" alt="Huge Jewelry" itemprop="logo">
    </a>
    
    
    </h1>
     
                <div class="grid__item wide--five-sixths post-large--five-sixths large--five-sixths menubar-section">
                  <div id="shopify-section-navigation" class="shopify-section"><div class="desktop-megamenu">
  <div class="header-sticky">
    <div id="header-landing" class="sticky-animate">
      <div class="nav-bar-mobile">
        <nav class="nav-bar" role="navigation">
          <div class="site-nav-dropdown_inner">
            <div class="menu-tool">  
  <div class="container">
  <ul class="site-nav">
    
    
    

      
    
    
    <li class=" ">
      <a  href="/" class="current">
        <span>         
          Home          
        </span>  
        
      </a>  

      
         
        

      
      
      

      

    </li>
    
    
    

      
    
    
    <li class=" dropdown  mega-menu">
      <a  href="/collections" class="">
        <span>         
          Collection          
        </span>  
        
      </a>  

      
         
        

                
      <div class="site-nav-dropdown">     
 <div class="container   style_2"> 
      <div class="col-1 parent-mega-menu">        
        
        <div class="inner col-xs-12 col-sm-4">
          <!-- Menu level 2 -->
          <a  href="/collections" class=" ">
            Bangle Sets 
            
          </a>
          
          <ul class="dropdown">
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-83" >
                Gold Bangles
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-87" >
                Enameled Gold
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-69" >
                Dailywear Women
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/promise-solitaire-ring-mount" >
                 Circuit Bangles
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/promise-solitaire-ring-mount" >
                Floral Gold 
              </a>
            </li>
            
          </ul>
          
        </div>
        
        <div class="inner col-xs-12 col-sm-4">
          <!-- Menu level 2 -->
          <a  href="/collections/bracelets" class=" ">
            Bracelets 
            
          </a>
          
          <ul class="dropdown">
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-83" >
                Silver Bracelet
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-83" >
                Flower Silver
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/lush-ring-mount" >
                Rhodium Bangle
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/lush-ring-mount" >
                Cuff Wrist Band
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-69" >
                Fancy Pearl 
              </a>
            </li>
            
          </ul>
          
        </div>
        
        <div class="inner col-xs-12 col-sm-4">
          <!-- Menu level 2 -->
          <a  href="/collections" class=" ">
            Pendants 
            
          </a>
          
          <ul class="dropdown">
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/lush-ring-mount" >
                Gold Pendants
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/hera-solitaire-ring" >
                Lion Gold 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-83" >
                Rudracha Gold
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-78" >
                Hearting Gold
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/hera-solitaire-ring" >
                Glove Pendant
              </a>
            </li>
            
          </ul>
          
        </div>
        
        <div class="inner col-xs-12 col-sm-4">
          <!-- Menu level 2 -->
          <a  href="/collections" class=" ">
            Earrings 
            
          </a>
          
          <ul class="dropdown">
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-83" >
                Studded Gold
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/lush-ring-mount" >
                Apple Earrings
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/promise-solitaire-ring-mount" >
                Square Shape 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/promise-solitaire-ring-mount" >
                Casting Stud
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-87" >
                Balls Jimmiki
              </a>
            </li>
            
          </ul>
          
        </div>
        
        <div class="inner col-xs-12 col-sm-4">
          <!-- Menu level 2 -->
          <a  href="/collections" class=" ">
            Watches 
            
          </a>
          
          <ul class="dropdown">
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-69" >
                Hizone Analog
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-83" >
                Giordano Analog
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-83" >
                Analog Watch
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/barry-gold-bangle-for-her" >
                Smart Watches
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/barry-gold-bangle-for-her" >
                Patek Watches
              </a>
            </li>
            
          </ul>
          
        </div>
        
        <div class="inner col-xs-12 col-sm-4">
          <!-- Menu level 2 -->
          <a  href="/collections" class=" ">
            straight-chain  
            
          </a>
          
          <ul class="dropdown">
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/barry-gold-bangle-for-her" >
                Necklace Set
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/promise-solitaire-ring-mount" >
                 Square Ruby
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/classic-prong-set-ring-mount" >
                Balls Gold
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-70" >
                Delight Ruby
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/grooved-texture-gold-bangle-set" >
                Royal Bloom 
              </a>
            </li>
            
          </ul>
          
        </div>
        
     </div>
      
      
      <div class="col-2">
        
        <div class="col-left col-sm-6">
          <a href="" title="">
            <img src="//cdn.shopify.com/s/files/1/1811/9385/files/dropdown_2_menu_image_1_2000x.jpg?v=1537514876" alt="" />
          </a>
          <a href="" title="">
            <img src="//cdn.shopify.com/s/files/1/1811/9385/files/dropdown_2_menu_image_2_2000x.jpg?v=1537514886" alt="" />
          </a>
        </div>
        
        
        <div class="col-right col-sm-6">
          <a href="" title="">
            <img src="//cdn.shopify.com/s/files/1/1811/9385/files/dropdown_2_menu_image_3_2000x.jpg?v=1537514897" alt="" />
          </a>
        </div>
            

      </div>
      
      
    </div>
</div>                     
      

      

    </li>
    
    
    

      
    
    
    <li class=" dropdown  mega-menu">
      <a  href="/collections/jewel" class="">
        <span>         
          Jewel          
        </span>  
        
      </a>  

      
         
        

                
      <div class="site-nav-dropdown">     
 <div class="container   style_5"> 
      <div class="col-1 parent-mega-menu">        
        
        <div class="inner col-xs-12 col-sm-4">
          <!-- Menu level 2 -->
          <a  href="/collections/all" class=" ">
            Bangle Sets 
            
          </a>
          
          <ul class="dropdown">
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/grooved-texture-gold-bangle-set" >
                Wave Gold
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/barry-gold-bangle-for-her" >
                Enameled Gold 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-70" >
                Dailywear Women 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-68" >
                Trendy Parallel 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/barry-gold-bangle-for-her" >
                 Floral Gold 
              </a>
            </li>
            
          </ul>
          
        </div>
        
        <div class="inner col-xs-12 col-sm-4">
          <!-- Menu level 2 -->
          <a  href="/collections/all" class=" ">
            Bracelets 
            
          </a>
          
          <ul class="dropdown">
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/barry-gold-bangle-for-her" >
                Sterling Silver 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/classic-prong-set-ring-mount" >
                 Flower Silver 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-70" >
                Silver Rhodium 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/promise-solitaire-ring-mount" >
                Men's Cuff 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/promise-solitaire-ring-mount" >
                Pearl Link 
              </a>
            </li>
            
          </ul>
          
        </div>
        
        <div class="inner col-xs-12 col-sm-4">
          <!-- Menu level 2 -->
          <a  href="/collections/all" class=" ">
            Pendants 
            
          </a>
          
          <ul class="dropdown">
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/amor-solitaire-ring-mount" >
                Heart Gold 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/promise-solitaire-ring-mount" >
                Lion Gold 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/classic-prong-set-ring-mount" >
                Rudracha Gold 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/promise-solitaire-ring-mount" >
                Hearting Gold 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-70" >
                Glove Pendant
              </a>
            </li>
            
          </ul>
          
        </div>
        
        <div class="inner col-xs-12 col-sm-4">
          <!-- Menu level 2 -->
          <a  href="/collections/all" class=" ">
            Earrings 
            
          </a>
          
          <ul class="dropdown">
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/lush-ring-mount" >
                Studded Gold 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/classic-prong-set-ring-mount" >
                Gold Apple 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-70" >
                Square Shape 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-70" >
                Hanging Flower 
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/grooved-texture-gold-bangle-set" >
                Gold Balls 
              </a>
            </li>
            
          </ul>
          
        </div>
        
     </div>
            
            
      <div class="col-wide">
        
        <div class="bottom_left">          
          <a href="" title="">
            <img src="//cdn.shopify.com/s/files/1/1811/9385/files/bottom_3_menu_image_3_2000x.jpg?v=1537514932" alt="" />
          </a>
        </div>
        
        
        <div class="bottom_right">          
          <a href="" title="">
            <img src="//cdn.shopify.com/s/files/1/1811/9385/files/bottom_right_3_menu_image_3_2000x.jpg?v=1537514939" alt="" />
          </a>
        </div>
        
      </div>      
      
      
    </div>
</div>                     
      

      

    </li>
    
    
    

      
    
    
    <li class=" dropdown  mega-menu">
      <a  href="/collections/rare-collections" class="">
        <span>         
          Rings          
        </span>  
        
      </a>  

      
         
        

                
      <div class="site-nav-dropdown">     
 <div class="container   style_4"> 
      <div class="col-1 parent-mega-menu">        
        
        <div class="inner col-xs-12 col">
          <!-- Menu level 2 -->
          <a  href="/collections/crystal-female-ring" class=" ">
            Crystal Female Ring 
            
            
            
            
            
             
          
            <img src="//cdn.shopify.com/s/files/1/1811/9385/collections/Ring1_medium.jpg?v=1496239419" />         
            
          
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
          </a>
          
        </div>
        
        <div class="inner col-xs-12 col">
          <!-- Menu level 2 -->
          <a  href="/collections/rhodium-plated-ring" class=" ">
            Rhodium Plated Ring 
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
             
          
            <img src="//cdn.shopify.com/s/files/1/1811/9385/collections/Ring_2_medium.jpg?v=1496239497" />          
            
          
            
            
            
            
          </a>
          
        </div>
        
        <div class="inner col-xs-12 col">
          <!-- Menu level 2 -->
          <a  href="/collections/crystal-female-ring" class=" ">
            Crystal Ring Jewelry 
            
            
            
            
            
            
            
             
          
            <img src="//cdn.shopify.com/s/files/1/1811/9385/collections/Ring_3_medium.jpg?v=1496239530" />          
            
          
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
          </a>
          
        </div>
        
        <div class="inner col-xs-12 col">
          <!-- Menu level 2 -->
          <a  href="/collections/sterling-silver-jewelry" class=" ">
             Sterling Silver Jewelry  
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
             
          
            <img src="//cdn.shopify.com/s/files/1/1811/9385/collections/Ring_4_medium.jpg?v=1496239594" />          
            
          
            
            
          </a>
          
        </div>
        
     </div>
      
    </div>
</div>                     
      

      

    </li>
    
    
    

      
    
    
    <li class=" dropdown  mega-menu">
      <a  href="/collections/new-arrivals" class="">
        <span>         
          Necklaces          
        </span>  
        
      </a>  

      
         
        

                
      <div class="site-nav-dropdown">     
 <div class="container   style_1"> 
      <div class="col-1 parent-mega-menu">        
        
        <div class="inner col-xs-12 col-sm-4">
          <!-- Menu level 2 -->
          <a  href="/collections" class=" ">
            Silver 
            
          </a>
          
          <ul class="dropdown">
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-69" >
                Welaribe Sta
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/grooved-texture-gold-bangle-set" >
                Chokers & Necklaces
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-69" >
                Princess necklace
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/classic-prong-set-ring-mount" >
                Matinee necklace
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-70" >
                Opera necklace
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-70" >
                Rope necklace
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/amor-solitaire-ring-mount" >
                Lariat necklace
              </a>
            </li>
            
          </ul>
          
        </div>
        
        <div class="inner col-xs-12 col-sm-4">
          <!-- Menu level 2 -->
          <a  href="/collections" class=" ">
            Gold  
            
          </a>
          
          <ul class="dropdown">
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/barry-gold-bangle-for-her" >
                Cross necklace
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-70" >
                Pearl necklace
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/lush-ring-mount" >
                pendant necklace
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/promise-solitaire-ring-mount" >
                Rivière Necklace
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/promise-solitaire-ring-mount" >
                Prayer bead necklace
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/classic-prong-set-ring-mount" >
                Antique Jewellery
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-70" >
                Handmade jewellery
              </a>
            </li>
            
          </ul>
          
        </div>
        
        <div class="inner col-xs-12 col-sm-4">
          <!-- Menu level 2 -->
          <a  href="/collections" class=" ">
            Diamond 
            
          </a>
          
          <ul class="dropdown">
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/grooved-texture-gold-bangle-set" >
                Gemstone Jewellery
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/classic-prong-set-ring-mount" >
                Tribal Jewellery
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-70" >
                Custom Jewellery
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-70" >
                Filigree Jewellery
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-69" >
                Swamy Jewellery
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/grooved-texture-gold-bangle-set" >
                Paambadam jewellery
              </a>
            </li>
            
            <!-- Menu level 3 -->
            <li>
              <a href="/products/product-name-70" >
                Estate Jewellery
              </a>
            </li>
            
          </ul>
          
        </div>
        
     </div>
      
      
      <div class="col-2">
        
        <p class="first">
          <a href="" title="">
            <img src="//cdn.shopify.com/s/files/1/1811/9385/files/dropdown_5_menu_image_3_2000x.jpg?v=1537515016" alt="" />
          </a>
        </p>
        
        
      </div>
      
      
    </div>
</div>                     
      

      

    </li>
    
    
    

      
    
    
    <li class="  dropdown">
      <a class="menu__moblie"  href="/pages/about" class="">
        <span>         
          Pages          
        </span>  
        
      </a>  

      
         
        

      
      
      <ul class="site-nav-dropdown">
  
  <li >                    
    <a href="/pages/about" class="">               
      <span>               
        About us                
      </span>
      
    </a>
    <ul class="site-nav-dropdown">
      
    </ul>
  </li>
  
  <li >                    
    <a href="/pages/contact" class="">               
      <span>               
        Contact us                
      </span>
      
    </a>
    <ul class="site-nav-dropdown">
      
    </ul>
  </li>
  
  <li >                    
    <a href="/blogs/news" class="">               
      <span>               
        Blog                
      </span>
      
    </a>
    <ul class="site-nav-dropdown">
      
    </ul>
  </li>
  
</ul>
      
      

      

    </li>
    
  </ul>  
  </div>
</div>
          </div>
        </nav>
      </div>
    </div>
  </div>
</div>

</div>              
                </div> 
                <div id="shopify-section-header" class="shopify-section"><div class="menu-icon">           
  
  <div class="header-bar__module cart header_cart">               
    <!-- Mini Cart Start -->
<div class="baskettop">
  <div class="wrapper-top-cart">
    <a href="javascript:void(0)" id="ToggleDown" class="icon-cart-arrow">     
      <i class="fa fa-shopping-basket" aria-hidden="true"></i>
      <div class="detail">
        <div id="cartCount"> 
          0
        </div>
      </div>
    </a> 
    <div id="slidedown-cart" style="display:none"> 
      <!--  <h3>Shopping cart</h3>-->
      <div class="no-items">
        <p>Your cart is currently empty!</p>
        <p class="text-continue"><a  href="javascript:void(0)">Continue shopping</a></p>
      </div>
      <div class="has-items">
        <ul class="mini-products-list">  
          
        </ul>
        <div class="summary">                
          <p class="total">
            <span class="label">Cart total:</span>
            <span class="price"><span class=money>$0.00</span></span> 
          </p>
        </div>
        <div class="actions">
          <button class="btn" onclick="window.location='/checkout'"><i class="icon-check"></i>Check Out</button>
          <button class="btn text-cart" onclick="window.location='/cart'"><i class="icon-basket"></i>View Cart</button>
        </div>
      </div>
    </div>
  </div>
</div> <!-- End Top Header -->                   
  </div> 
   

                  
  <div class="header-search">
    <div class="header-search">
      <a href="/search" class="site-header__link site-header__search js-drawer-open-top">
        <span class="fa fa-search" aria-hidden="true"></span>
      </a>
    </div>
  </div>
    
   
  <div class="header_currency">
    <ul class="tbl-list">
      <li class="currency dropdown-parent uppercase currency-block">
        
<div class="selector-arrow">
<select class="currencies_src" id="currencies">
  
  
  <option data-currency="USD"  selected  value="USD">USD</option> 
  
  
  
  <option data-currency="EUR"  value="EUR">EUR</option> 
  
  
  
  <option data-currency="GBP"  value="GBP">GBP</option> 
  
  
  
  <option data-currency="AUD"  value="AUD">AUD</option> 
  
  
  
  <option data-currency="INR"  value="INR">INR</option> 
  
   
  
  
  
  
  
  
  
  
  
</select>
</div>





      </li>
    </ul>
  </div>  

  
  <ul class="menu_bar_right">
    <li>
      <div class="slidedown_section">
        <a  id="Togglemodal" class="icon-cart-arrow" title="account"><i class="fa fa-user"></i></a>
        <div id="slidedown-modal">
          <div class="header-panel-top">
            <ul>
                      
              <li>
                <div class="customer_account">                          
                  <ul>
                    
                    
                    <li>
                      <a href="/account/login" title="Log in">Log in</a>
                    </li>
                    <li>
                      <a href="/account/register" title="Create account">Create account</a>
                    </li>          
                      
                     
                       
                    <li>
                       <a class="wishlist" href="/pages/wishlist" title="Wishlist">Wishlist</a>
 
                    </li>
                     
                  </ul>
                </div>    
              </li>
              
            </ul>
          </div>
        </div>
      </div>
    </li>
  </ul>

  <div class="header-mobile">
    <div class="menu-block visible-phone"><!-- start Navigation Mobile  -->
      <div id="showLeftPush">
        <i class="fa fa-bars" aria-hidden="true"></i>
      </div>
    </div><!-- end Navigation Mobile  --> 
  </div>

</div>
<style> 

  /* Top block */
  .header-type-8 .top_bar { background: ; }
  .header-type-8 .top_bar li, .header-type-8 .top_bar span { color:;}     
  .header-type-8 .top_bar a { color:;}    
  .header-type-8 .top_bar a:hover { color:;}    
  .header-type-8 .header-bar__module.cart .baskettop a.icon-cart-arrow #cartCount { background: #d2aa5c;color:#ffffff;}
  .header-type-8 .header-bar__module.cart .baskettop a.icon-cart-arrow:hover #cartCount { background: #000000;color:#ffffff;}

  /* Logo block */
  .header-type-8 .site-header__logo a { color:#ffffff;}
  .header-type-8 .site-header__logo a:hover { color:#d2aa5c;}    

  /* Search block */     
  .header-type-8 .search-bar input[type="search"] {color:#000000;} 
  .header-type-8 .header-search span  {color:#ffffff;} 
  .header-type-8 .header-search span:hover {color:#d2aa5c;} 
  .header-type-8 .search-bar__form, .header-type-8 #SearchDrawer  {  }
  .header-type-8 .search-bar__form button,.header-type-8 .search-bar__icon-button { color:#000000;}
  .header-type-8 .search-bar__form button:hover,.header-type-8 .search-bar__icon-button:hover { color:#d2aa5c;}

  .header-type-8 .search-bar input[type="search"]::-webkit-input-placeholder  { /* Chrome/Opera/Safari */
    color:#000000;
  }
  .header-type-8 .search-bar input[type="search"]::-moz-placeholder { /* Firefox 19+ */
    color:#000000;
  }
  .header-type-8 .search-bar input[type="search"]:-ms-input-placeholder { /* IE 10+ */
    color:#000000;
  }
  .header-type-8 .search-bar input[type="search"]:-moz-placeholder { /* Firefox 18- */
    color:#000000;
  }


  /* Menu  block */
  
     .header-type-8 .is-sticky .site-header__menubar,.mobile-nav-section  { background:#000000; } 
  .header-type-8 .menu-tool ul li {color: ;}
  .header-type-8 .menu-tool ul li a,.mobile-nav-section .mobile-nav-trigger , .header-mobile #showLeftPush {color:#ffffff;}  
  .header-type-8 .menu-tool ul li a:hover,.header-type-8 .menu-tool .site-nav > li > a.current:hover {color:#d2aa5c;} 
  .header-type-8 .menu-tool .site-nav >  li > a.current {color:#d2aa5c;} 
  .header-type-8 .site-nav-dropdown,#MobileNav,.mobile-nav__sublist { background: #fff;}
  .header-type-8 .site-nav-dropdown .inner > a {color: #d2aa5c;}    
  .header-type-8 .site-nav-dropdown .inner > a:hover {color: #d2aa5c;}    
  .header-type-8 .site-nav-dropdown .inner .dropdown a,.header-type-8 .menu-tool .site-nav .site-nav-dropdown li a,.header-type-8 .site-nav .widget-featured-product .product-title,.header-type-8 .site-nav .widget-featured-product .widget-title h3,#MobileNav a,.mobile-nav__sublist a,.site-nav .widget-featured-nav .owl-prev a,.site-nav .widget-featured-nav .owl-next a  {color: #3e454c;}
  .header-type-8 .site-nav-dropdown .inner .dropdown a:hover,.header-type-8 .menu-tool .site-nav .site-nav-dropdown li a:hover,.header-type-8 .site-nav-dropdown .inner .dropdown a.current,.header-type-8 .menu-tool .site-nav .site-nav-dropdown li a.current,.header-type-8 .site-nav .widget-featured-product .product-title:hover,#MobileNav a.current,.mobile-nav__sublist a.current,.site-nav .widget-featured-nav .owl-prev a:hover,.site-nav .widget-featured-nav .owl-next a:hover {color: #d2aa5c;}    

 /* .header-type-8 .site-nav > li > a::before {background: #ffffff; }*/
  
  /* Dropdown block */
  .header-type-8 .menubar-section #Togglemodal i {color: #ffffff;}
  .header-type-8 .menubar-section #Togglemodal i:hover {color: #d2aa5c;}
  .header-type-8 #slidedown-modal {background: #fff;}
  .header-type-8 #slidedown-modal ul li a {color:#3e454c;} 
  .header-type-8 #slidedown-modal ul li a:hover {color:#d2aa5c;} 


  /* Cart Summary block */

  .header-type-8 #slidedown-cart .actions, .header-type-8  #slidedown-cart  {background: #ffffff;}
  .header-type-8 .header-bar__module a {color:#d2aa5c;} 
  .header-type-8 .header-bar__module.cart .baskettop a.icon-cart-arrow i { color: #ffffff;}
  .header-type-8 .header-bar__module.cart .baskettop a.icon-cart-arrow:hover i {color: #d2aa5c;}
  .header-type-8 .header-bar__module a:hover {color:#d2aa5c;} 

  .header-type-8 #slidedown-cart li {  }

  .header-type-8 .menu_bar_right i {color:#ffffff;}
  .header-type-8 .menu_bar_right i:hover {color:#d2aa5c;}



  /* Currency block */



  .header-type-8 .header_currency ul select , .header_currency ul li.currency .selector-arrow::after{color:#ffffff;}   
  .header-type-8 .header_currency ul select:hover ,.header_currency ul li.currency:hover .selector-arrow::after{color:#d2aa5c;}  
  .header-type-8 .header_currency ul li.currency:hover:after {border-top-color:#d2aa5c;}
  .header-type-8 .header_currency ul li.currency:after {border-top-color:#ffffff;}
  .header-type-8 .header_currency option {background:#fff;color:#3e454c;}


  /* Header borders */





  .header-type-8 .site-header {background:#000000;}

  .header-mobile {color:#3e454c;}


  @media only screen and (min-width:320px) and (max-width:967px) {


    .template-index .header-type-8 .site-header {background:#000000;}
  }




  @media only screen and (min-width:967px) and (max-width:5000px) {


      .template-index .header-type-8:hover .site-header {  background:rgba(0, 0, 0, 0.7) }
    
    .template-index .header-type-8 .site-header {  background:transparent}


  }


  @media(max-width:767px){ 

    .header-type-8 .site-header__menubar { background:; } 
  }
  /* General styles for all menus */


  .gf-menu-device-wrapper .close-menu {
    font-size: 17px;
    padding: 12px 20px;
    text-align: right;
    display: block;
    border-bottom: 1px solid #e5e5e5;
  }

  .cbp-spmenu {

    position: fixed;
  }

  .cbp-spmenu h3 {

    font-size: 1.9em;
    padding: 20px;
    margin: 0;
    font-weight: 300;
    background: #0d77b6;
  }

  .cbp-spmenu a {
    display: block;

    font-size: 1.1em;
    font-weight: 300;
  }




  /* Orientation-dependent styles for the content of the menu */

  .cbp-spmenu-vertical {
    width: 240px;
    height: 100%;
    top: 0;
    z-index: 1000;
  }

  .cbp-spmenu-vertical a {

    padding: 1em;
  }

  .cbp-spmenu-horizontal {
    width: 100%;
    height: 150px;
    left: 0;
    z-index: 1000;
    overflow: hidden;
  }

  .cbp-spmenu-horizontal h3 {
    height: 100%;
    width: 20%;
    float: left;
  }

  .cbp-spmenu-horizontal a {
    float: left;
    width: 20%;
    padding: 0.8em;

  }

  /* Vertical menu that slides from the left or right */

  .cbp-spmenu-left {
    left: -240px;
  }

  .cbp-spmenu-right {
    right: -240px;
  }

  .cbp-spmenu-left.cbp-spmenu-open {
    left: 0px;
  }

  .cbp-spmenu-right.cbp-spmenu-open {
    right: 0px;
  }

  /* Horizontal menu that slides from the top or bottom */

  .cbp-spmenu-top {
    top: -150px;
  }

  .cbp-spmenu-bottom {
    bottom: -150px;
  }

  .cbp-spmenu-top.cbp-spmenu-open {
    top: 0px;
  }

  .cbp-spmenu-bottom.cbp-spmenu-open {
    bottom: 0px;
  }

  /* Push classes applied to the body */

  .cbp-spmenu-push {
    overflow-x: hidden;
    position: relative;
    left: 0;
  }

  .cbp-spmenu-push-toright {
    left: 240px;
  }

  .cbp-spmenu-push-toleft {
    left: -240px;
  }

  /* Transitions */

  .cbp-spmenu,
  .cbp-spmenu-push {
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    transition: all 0.3s ease;
  }

  /* Example media queries */

  @media screen and (max-width: 55.1875em){

    .cbp-spmenu-horizontal {
      font-size: 75%;
      height: 110px;
    }

    .cbp-spmenu-top {
      top: -110px;
    }

    .cbp-spmenu-bottom {
      bottom: -110px;
    }

  }

  @media screen and (max-height: 26.375em){

    .cbp-spmenu-vertical {
      font-size: 90%;
      width: 190px;
    }

    .cbp-spmenu-left,
    .cbp-spmenu-push-toleft {
      left: -190px;
    }

    .cbp-spmenu-right {
      right: -190px;
    }

    .cbp-spmenu-push-toright {
      left: 190px;
    }
  }







  /* width: 750px  */
  @media (min-width: 968) and (max-width: 991px) {
    .banner .container { margin-left: -375px; }
    .banner .inner { width: 320px; }

    .header-bottom.on .header-panel-top { right: 65px; }
    .header-bottom.on .site-nav { padding-right: 90px; }

    .nav-bar .header-logo-fix { margin-left: 10px; margin-right: 10px; }
    .site-nav > li > a { margin-left: 12px; margin-right: 12px; }
    .header-bottom.on .site-nav > li > a { margin-left: 7px; margin-right: 7px; font-size: 10px; }

  }

  @media (min-width: 968px) {
    .cbp-spmenu-push-toright {left:0!important;}
    #cbp-spmenu-s1 {display: none !important;}


    .header-bottom.on .nav-bar .header-logo-fix { display: table; height: 48px; position: relative; z-index: 2; }
    .header-bottom.on .nav-bar .header-logo-fix a { display: table-cell; vertical-align: middle; }
    .header-bottom.on .site-nav { padding-right: 120px; }  
    .have-fixed .nav-bar {position: fixed;left: 0;right: 0;top: 0;z-index: 99;padding: 0;}
    .have-fixed .nav-search {position: fixed;top: 0;right: 65px;z-index: 100;width:52px;}


  }

  /* width: 100%  */
  @media (max-width: 967px) {
    body.cbp-spmenu-push-toright {
      cursor: pointer;
    }

    .cbp-spmenu .site-nav-dropdown.style_4 .inner img{margin-top:10px;}


    .visible-phone { display: block; }
    .hidden-phone { display: none; }

    /* header */





    /* Fix Menu Mobile */
    .nav-bar { display: none; }
    .gf-menu-device-container .site-nav { display: block!important; overflow: hidden;width:100%; }
    .gf-menu-device-container .site-nav li { width:100%; }
    .gf-menu-device-container .site-nav.gf-menu.clicked { visibility: visible; height: auto; }  
    /* End Fix Menu Mobile */

    .cbp-spmenu-left.cbp-spmenu-open { left: 0; overflow-y: auto; }
    .cbp-spmenu-push-toright {  overflow-y: hidden;position: fixed; width: 100%;}

    #megamenu-responsive-root { display: none !important; }
    .menu-block { width: 100%; float: left; padding: 0; }

    /* Icon Menu */
    .site-nav > li:hover > a > span:first-child, 
    .site-nav > li:hover > a.current > span:first-child, 
    .site-nav > li > a.current > span:first-child { border: 0; }
    .site-nav a { white-space: normal; }
    .cbp-spmenu {  }
    .cbp-spmenu .site-nav > li > a { font-size: 13px; font-weight: 700;  padding: 12px 20px; margin: 0; }

    .cbp-spmenu .site-nav > li.dropdown.open > a {position: relative;}
    .cbp-spmenu .site-nav > li.dropdown.open > a:before {top: 15px; }

    .menu-block .site-nav { border-bottom: none; }

    .site-nav li { position: relative; }
    .site-nav li.dropdown { position: relative; }
    .site-nav > li { display: block; clear: both; position: relative;}
    .site-nav > li > a { padding: 12px 0; }
    .site-nav > li.dropdown > p.toogleClick { height:0; width:0; display:block; margin-left: 7px; top: 2px; right: 5px; margin:0; padding: 0; z-index: 2; padding: 20px;}
    .site-nav > li.dropdown p.toogleClick { position: absolute; right: 0; text-indent: -999em; cursor: pointer; }
    .site-nav > li.dropdown > p.toogleClick.mobile-toggle-open:before { border-top-color:transparent; top: 14px;}

    .site-nav li.dropdown a > .icon-dropdown { display: none; }

    .site-nav-dropdown .container { padding-left: 0; padding-right: 0; }
    .site-nav-dropdown .row{margin:0px!important}


    .site-nav > li.dropdown ul p.toogleClick.mobile-toggle-open:before { top: 10px; }
    .site-nav-dropdown .col-1 .inner p.toogleClick:before,
    .site-nav > li.dropdown ul p.toogleClick:before { display: block; content:""; position: absolute; right: 0; top: -15px!important; width: 20px; height: 40px; }
    .site-nav-dropdown p.toogleClick { background: url(//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/icon-megamenu.png?3493) no-repeat; padding: 0; width: 8px; height: 8px; right: 0; top: 18px; z-index: 2; }
    .site-nav-dropdown .col-1 .inner p.toogleClick { display: block!important; }
    .site-nav > li.dropdown ul p.toogleClick.mobile-toggle-open,
    .site-nav-dropdown .col-1 .inner p.toogleClick.mobile-toggle-open { background-position: center bottom; height: 4px; }

    .site-nav > li > ul > .dropdown.open > ul {display: block;}
    .site-nav > li > ul > li > ul > .dropdown.open > ul {display: block;}
    .site-nav > li > ul > li > ul > ul > li > .dropdown.open > ul {display: block;}

    .site-nav > li > .site-nav-dropdown {}
    .site-nav > li > .site-nav-dropdown > li > a { padding: 11px 0 13px; text-transform: uppercase; font-size: 11px; font-weight: 700;  }
    .site-nav > li > .site-nav-dropdown > li:first-child > a { border-top: 0; }
    .site-nav > li > .site-nav-dropdown > li:hover > a:before { background: none; }
    .site-nav > li li .site-nav-dropdown { padding: 0 15px; margin-bottom: 20px; }
    .site-nav-dropdown li:hover a { background: none; }
    .site-nav-dropdown li:hover a, 
    .site-nav-dropdown a:active { padding-left: 0; }

    .site-nav-dropdown li li a { padding: 7px 0; }
    .site-nav-dropdown li li:hover a { padding: 7px 20px; }
    .site-nav-dropdown li:hover > a:before { left: 0; }

    .site-nav-dropdown .col-1,
    .site-nav-dropdown .col-2,
    .site-nav-dropdown .col-3 { width: 100%; padding: 0; }
    .site-nav-dropdown .col-3 { padding-bottom: 28px; }
    .site-nav-dropdown .col-1 .inner { width: 100%; padding: 0; }
    .cbp-spmenu .site-nav-dropdown .col-1 .inner:first-child > a { border-top: 0; }
    .site-nav-dropdown .col-1 ul.dropdown li a { padding: 7px 15px; font-size: 12px; font-weight: 400; text-transform: none; border: 0; }
    .site-nav-dropdown .col-1 ul.dropdown li:hover > a:before { left: 20px; }
    .site-nav .widget-featured-product { text-align: left;border-width: 1px 0; margin-bottom: 10px; padding-top: 23px; padding-bottom: 25px; }
    .site-nav .products-grid .grid-item { text-align: left; }
    .site-nav .products-grid .grid-item .product-grid-image { float: left; margin: 0 15px 0 0; }
    .site-nav .widget-featured-product .widget-title h3 { font-size: 11px; }
    .widget-featured-product .grid-item .product-grid-image img { width: 80px; }
    .widget-featured-product .products-grid .grid-item { position: relative; }
    .widget-featured-product .details { overflow: hidden; }
    .site-nav .product-label { display: none; }
    .site-nav .product-label strong { float: left; }

    .cbp-spmenu .site-nav-dropdown .col-1 .inner { width: 100%!important; position: relative; padding: 0; float: left; }
    .cbp-spmenu .site-nav-dropdown.style_4 .inner{width:100%;}

    /*Update 2.0.1*/
    .site-nav-dropdown .col-2 .col-left { width: 100%; clear: both; padding: 0; }
    .site-nav-dropdown .col-2 .col-right { width: 100%; clear: both; padding: 0 0 10px; }
    .site-nav-dropdown .style_2 .col-2 .col-left a { padding-right: 0; }
    .site-nav-dropdown .style_3 .inner > img { display: none; }
    .site-nav-dropdown .style_4 .col-2 { padding-right: 0; }

  }


  .header-mobile { position: relative; }
  .header-mobile #showLeftPush.active .fa-times{display:block;line-height:46px;}
  .header-mobile #showLeftPush {  display: inline-block;font-size: 16px; text-align: center;  cursor: pointer; }
  .header-mobile #showLeftPush.active,
  .header-mobile #showLeftPush:hover {  }
  .header-mobile .customer-area { float: left; width: 50%; position: static; }
  .header-mobile .customer-area > a { float: left; width: 100%; height:46px;}

  .header-mobile .customer-links { margin: 0; }
  .header-mobile .dropdown-menu { font-size:12px; margin: 0; width: 200%; padding: 10px 15px; 
    -webkit-border-radius: 0; -moz-border-radius: 0; border-radius: 0; 
    -webkit-box-shadow: none; -moz-box-shadow: none; box-shadow: none; }
  .header-mobile .dropdown-menu ul { overflow: hidden; margin: 0 0 10px; padding-left: 0; list-style: none; }
  .header-mobile .customer-area .fa-user{display: block;text-align: center;line-height: 46px;font-size: 20px;}





</style> 



</div>
              </div>
            </div>
          </div>
        </div>
      </header>
    </div>   
             
    <div id="PageContainer"></div>   
    <div class="quick-view"></div>        
    <main class="main-content"> 
      
       
      
 
        
      
      
        <div class="grid__item">         
          <div id="shopify-section-slider-revolution" class="shopify-section index-section">


<div id="rev_slider_72_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="jewel" data-source="gallery" style="margin:0px auto;background:#bababa;padding:0px;margin-top:0px;margin-bottom:0px;">
<!-- START REVOLUTION SLIDER 5.4.3.1 auto mode -->
  <div id="rev_slider_72_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.3.1">
<ul>  <!-- SLIDE  -->
    
  <li data-index="rs-289" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="//cdn.shopify.com/s/files/1/1811/9385/files/jewel1_712212f2-13a9-4677-a2f1-28cbab658c3c_100x.jpg?v=1557139733"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
    <!-- MAIN IMAGE -->
          
    <img src="//cdn.shopify.com/s/files/1/1811/9385/files/jewel1_712212f2-13a9-4677-a2f1-28cbab658c3c.jpg?v=1557139733"  alt="ORNAMENTS" title="ORNAMENTS"  width="1920" height="750" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
    <!-- LAYERS -->

      
    <!-- LAYER NR. 1 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-289-layer-1" 
       data-x="['center','center','center','center']" data-hoffset="['-560','-560','-200','-60']" 
       data-y="['top','top','top','top']" data-voffset="['129','129','80','80']" 
            data-width="none"
      data-height="none"
      data-whitespace="nowrap"
 
      data-type="image" 
      data-responsive_offset="on" 

      data-frames='[{"delay":2000,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
      data-textAlign="['inherit','inherit','inherit','inherit']"
      data-paddingtop="[0,0,0,0]"
      data-paddingright="[0,0,0,0]"
      data-paddingbottom="[0,0,0,0]"
      data-paddingleft="[0,0,0,0]"

      style="z-index: 5;"><img src="//cdn.shopify.com/s/files/1/1811/9385/files/jewel2_339f32bd-f59b-42da-8d60-10159e6b1aaa.png?v=1557139758"  alt="ORNAMENTS" title="ORNAMENTS"  data-ww="['231px','231px','231px','231px']" data-hh="['56px','56px','56px','56px']" width="231" height="56" data-no-retina> </div>
            
            
    

    <!-- LAYER NR. 2 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-289-layer-2" 
       data-x="['center','center','center','center']" data-hoffset="['-560','-560','-200','-60']" 
       data-y="['top','top','top','top']" data-voffset="['209','209','160','160']" 
            data-width="none"
      data-height="none"
      data-whitespace="nowrap"
 
      data-type="image" 
      data-responsive_offset="on" 

      data-frames='[{"delay":500,"speed":2000,"frame":"0","from":"x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
      data-textAlign="['inherit','inherit','inherit','inherit']"
      data-paddingtop="[0,0,0,0]"
      data-paddingright="[0,0,0,0]"
      data-paddingbottom="[0,0,0,0]"
      data-paddingleft="[0,0,0,0]"

      style="z-index: 6;"><img src="//cdn.shopify.com/s/files/1/1811/9385/files/jewel3_b74f8644-3d72-4945-9b03-9bd95552435d.jpg?v=1557139769"  alt="ORNAMENTS" data-ww="['184px','184px','146px','146px']" data-hh="['227px','227px','180px','180px']" width="184" height="227" data-no-retina> </div>
 
    
    <!-- LAYER NR. 3 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-289-layer-3" 
       data-x="['center','center','center','center']" data-hoffset="['-552','-552','-200','-60']" 
       data-y="['top','top','top','top']" data-voffset="['458','458','373','373']" 
            data-fontsize="['38','38','28','28']"
      data-letterspacing="['30','30','20','20']"
      data-width="none"
      data-height="none"
      data-whitespace="nowrap"
 
      data-type="text" 
      data-responsive_offset="on" 

      data-frames='[{"delay":2000,"speed":2000,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
      data-textAlign="['center','center','center','center']"
      data-paddingtop="[0,0,0,0]"
      data-paddingright="[0,0,0,0]"
      data-paddingbottom="[0,0,0,0]"
      data-paddingleft="[0,0,0,0]"

      style="z-index: 7; white-space: nowrap; font-size: 38px; line-height: 40px; font-weight: 300; color: #020202; letter-spacing: 30px;text-transform:uppercase;">ORNAMENTS </div>
            
  </li>
       

  
  <!-- SLIDE  -->
  <li data-index="rs-290" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="//cdn.shopify.com/s/files/1/1811/9385/files/jewel4_fe0e6035-36ab-4597-ba7b-d58662f729ce_100x.jpg?v=1557139778"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
    <!-- MAIN IMAGE -->
           
    <img src="//cdn.shopify.com/s/files/1/1811/9385/files/jewel4_fe0e6035-36ab-4597-ba7b-d58662f729ce.jpg?v=1557139778"  alt="ORNAMENTS" title="ORNAMENTS"  width="1920" height="750" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
        
    <!-- LAYERS -->

    <!-- LAYER NR. 4 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-290-layer-1" 
       data-x="['center','center','center','center']" data-hoffset="['320','320','144','0']" 
       data-y="['top','top','top','top']" data-voffset="['291','291','74','74']" 
            data-width="none"
      data-height="none"
      data-whitespace="nowrap"
 
      data-type="text" 
      data-responsive_offset="on" 

      data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
      data-textAlign="['center','center','center','center']"
      data-paddingtop="[0,0,0,0]"
      data-paddingright="[0,0,0,0]"
      data-paddingbottom="[0,0,0,0]"
      data-paddingleft="[0,0,0,0]"

      style="z-index: 5; white-space: nowrap; font-size: 50px; line-height: 50px; font-weight: 400; color: #dfad41; letter-spacing: 0px;font-style:italic;">Style In</div>

     
    <!-- LAYER NR. 5 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-290-layer-2" 
       data-x="['center','center','center','center']" data-hoffset="['320','320','144','0']" 
       data-y="['top','top','top','top']" data-voffset="['225','225','18','18']" 
            data-width="none"
      data-height="none"
      data-whitespace="nowrap"
 
      data-type="image" 
      data-responsive_offset="on" 

      data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
      data-textAlign="['inherit','inherit','inherit','inherit']"
      data-paddingtop="[0,0,0,0]"
      data-paddingright="[0,0,0,0]"
      data-paddingbottom="[0,0,0,0]"
      data-paddingleft="[0,0,0,0]"

      style="z-index: 6;"><img src="//cdn.shopify.com/s/files/1/1811/9385/files/jewel5_5f731ec3-5ad5-4dcc-b6fb-120d2fdaf448.png?v=1557139786"  alt="Style In" data-ww="['89px','89px','89px','89px']" data-hh="['47px','47px','47px','47px']" width="89" height="47" data-no-retina> </div>

      
    <!-- LAYER NR. 6 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-290-layer-3" 
       data-x="['center','center','center','center']" data-hoffset="['319','319','143','0']" 
       data-y="['top','top','top','top']" data-voffset="['365','365','133','133']" 
            data-width="none"
      data-height="none"
      data-whitespace="nowrap"
 
      data-type="image" 
      data-responsive_offset="on" 

      data-frames='[{"delay":2000,"speed":1500,"frame":"0","from":"sX:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
      data-textAlign="['inherit','inherit','inherit','inherit']"
      data-paddingtop="[0,0,0,0]"
      data-paddingright="[0,0,0,0]"
      data-paddingbottom="[0,0,0,0]"
      data-paddingleft="[0,0,0,0]"

      style="z-index: 7;"><img src="//cdn.shopify.com/s/files/1/1811/9385/files/jewel6_6abc121f-916b-41e0-a192-fd78dc5054f4.png?v=1557139793"  alt="Style In" data-ww="['310px','310px','310px','310px']" data-hh="['9px','9px','9px','9px']" width="310" height="9" data-no-retina> </div>

     
    <!-- LAYER NR. 7 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-290-layer-4" 
       data-x="['center','center','center','center']" data-hoffset="['320','320','156','0']" 
       data-y="['top','top','top','top']" data-voffset="['401','401','152','152']" 
            data-width="none"
      data-height="none"
      data-whitespace="nowrap"
 
      data-type="image" 
      data-responsive_offset="on" 

      data-frames='[{"delay":3000,"speed":1500,"frame":"0","from":"x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[-100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
      data-textAlign="['inherit','inherit','inherit','inherit']"
      data-paddingtop="[0,0,0,0]"
      data-paddingright="[0,0,0,0]"
      data-paddingbottom="[0,0,0,0]"
      data-paddingleft="[0,0,0,0]"

      style="z-index: 8;"><img src="//cdn.shopify.com/s/files/1/1811/9385/files/jewel7_e0e17077-4ccc-4086-a4d4-b24b1cadae69.png?v=1557139802"  alt="Style In"  data-ww="['488px','488px','323px','323px']" data-hh="['121px','121px','80px','80px']" width="488" height="121" data-no-retina> </div>
            
  </li>
  <!-- SLIDE  -->
     
  <li data-index="rs-291" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="//cdn.shopify.com/s/files/1/1811/9385/files/jewel8_8738411a-6ef0-450e-9e5f-cad20dcab8ee_100x.jpg?v=1557139812"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
    <!-- MAIN IMAGE -->
          
    <img src="//cdn.shopify.com/s/files/1/1811/9385/files/jewel8_8738411a-6ef0-450e-9e5f-cad20dcab8ee.jpg?v=1557139812" alt="A New Design Breed" title="A New Design Breed"   width="1920" height="750" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
    <!-- LAYERS -->

    
    <!-- LAYER NR. 8 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-291-layer-3" 
       data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
       data-y="['middle','middle','middle','middle']" data-voffset="['0','0','-140','-140']" 
            data-width="none"
      data-height="none"
      data-whitespace="nowrap"
 
      data-type="image" 
      data-responsive_offset="on" 

      data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
      data-textAlign="['inherit','inherit','inherit','inherit']"
      data-paddingtop="[0,0,0,0]"
      data-paddingright="[0,0,0,0]"
      data-paddingbottom="[0,0,0,0]"
      data-paddingleft="[0,0,0,0]"

      style="z-index: 5;"><img src="//cdn.shopify.com/s/files/1/1811/9385/files/jewel10_467e507d-9d9e-4895-9e85-91439d9b401a.png?v=1557139824" alt="A New Design Breed" data-ww="['470px','470px','259','259']" data-hh="['145px','145px','80px','80px']" width="470" height="145" data-no-retina> </div>

      
    <!-- LAYER NR. 9 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-291-layer-2" 
       data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
       data-y="['middle','middle','middle','middle']" data-voffset="['-130','-130','-190','-190']" 
            data-width="none"
      data-height="none"
      data-whitespace="nowrap"
 
      data-type="image" 
      data-responsive_offset="on" 

      data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
      data-textAlign="['inherit','inherit','inherit','inherit']"
      data-paddingtop="[0,0,0,0]"
      data-paddingright="[0,0,0,0]"
      data-paddingbottom="[0,0,0,0]"
      data-paddingleft="[0,0,0,0]"

      style="z-index: 6;"><img src="//cdn.shopify.com/s/files/1/1811/9385/files/jewel9_696b35e4-5059-4af5-9bf6-bcd914e82896.png?v=1557139833" alt="A New Design Breed" alt="" data-ww="['71px','71px','38','38']" data-hh="['66px','66px','35px','35px']" width="71" height="66" data-no-retina> </div>

    <!-- LAYER NR. 10 -->
    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" 
       id="slide-291-layer-5" 
       data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
       data-y="['middle','middle','middle','middle']" data-voffset="['130','130','150','150']" 
            data-width="500"
      data-height="55"
      data-whitespace="nowrap"
 
      data-type="shape" 
      data-responsive_offset="on" 

      data-frames='[{"delay":2500,"speed":1500,"frame":"0","from":"x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[-100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
      data-textAlign="['inherit','inherit','inherit','inherit']"
      data-paddingtop="[0,0,0,0]"
      data-paddingright="[0,0,0,0]"
      data-paddingbottom="[0,0,0,0]"
      data-paddingleft="[0,0,0,0]"

      style="z-index: 7;background-color:rgba(196, 145, 41, 0.9);"> </div>

    <!-- LAYER NR. 11 -->
    <div class="tp-caption   tp-resizeme" 
       id="slide-291-layer-4" 
       data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
       data-y="['middle','middle','middle','middle']" data-voffset="['130','130','150','150']" 
            data-width="none"
      data-height="none"
      data-whitespace="nowrap"
 
      data-type="text" 
      data-responsive_offset="on" 

      data-frames='[{"delay":2500,"speed":1500,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
      data-textAlign="['center','center','center','center']"
      data-paddingtop="[0,0,0,0]"
      data-paddingright="[0,0,0,0]"
      data-paddingbottom="[0,0,0,0]"
      data-paddingleft="[0,0,0,0]"

      style="z-index: 8; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: ; letter-spacing: 10px;text-transform:uppercase;">A New Design Breed </div>
             
  </li>
     
</ul>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
</div><!-- END REVOLUTION SLIDER -->

<script type="text/javascript">
var revapi72,
  tpj=jQuery;
      
tpj(document).ready(function() {
  if(tpj("#rev_slider_72_1").revolution == undefined){
    revslider_showDoubleJqueryError("#rev_slider_72_1");
  }else{
    revapi72 = tpj("#rev_slider_72_1").show().revolution({
      sliderType:"standard",
      // jsFileLocation:"//localhost/finance/wp-content/plugins/revslider/public/assets/js/",
      sliderLayout:"auto",
      dottedOverlay:"none",
      delay:9000,
      navigation: {
        keyboardNavigation:"off",
        keyboard_direction: "horizontal",
        mouseScrollNavigation:"off",
              mouseScrollReverse:"default",
        onHoverStop:"off",
        arrows: {
          style:"gyges",
          enable:true,
          hide_onmobile:true,
          hide_under:480,
          hide_onleave:true,
          hide_delay:200,
          hide_delay_mobile:1200,
          tmp:'',
          left: {
            h_align:"left",
            v_align:"center",
            h_offset:20,
            v_offset:0
          },
          right: {
            h_align:"right",
            v_align:"center",
            h_offset:20,
            v_offset:0
          }
        }
        ,
        bullets: {
          enable:true,
          hide_onmobile:false,
          style:"hebe",
          hide_onleave:false,
          direction:"horizontal",
          h_align:"center",
          v_align:"bottom",
          h_offset:0,
          v_offset:20,
          space:5,
          tmp:'<span class="tp-bullet-image"></span>'
        }
      },
      responsiveLevels:[1240,1240,778,480],
      visibilityLevels:[1240,1240,778,480],
      gridwidth:[1750,1024,778,480],
      gridheight:[750,768,500,500],
      lazyType:"none",
      shadow:0,
      spinner:"spinner2",
      stopLoop:"off",
      stopAfterLoops:-1,
      stopAtSlide:-1,
      shuffle:"off",
      autoHeight:"off",
      disableProgressBar:"on",
      hideThumbsOnMobile:"off",
      hideSliderAtLimit:0,
      hideCaptionAtLimit:0,
      hideAllCaptionAtLilmit:0,
      debugMode:false,
      fallbacks: {
        simplifyAll:"off",
        nextSlideOnWindowFocus:"off",
        disableFocusListener:false,
      }
    });
  }
  
}); /*ready*/
</script>


  

</div>
<!-- BEGIN content_for_index -->
<div id="shopify-section-1492003551011" class="shopify-section index-section">
  <div data-section-id="1492003551011" data-section-type="grid-banner-type-9" class="grid-banner-type-9">  
      <div class="grid-uniform">        
            
          
          
      <div class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item section-wrapper section-wrapper-1492003551011-0" style="background: #000000;"> 
        <div class="section-inner">        
            
            <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item left">
          
              <a href="/collections/all" class="collection-img" >
              <img src="//cdn.shopify.com/s/files/1/1811/9385/files/home-1_377782d4-8524-481b-a0cb-92d1d3e7d865.jpg?v=1496145380" alt="Just Launched"/>
              </a>
           
          </div>
           
          
         <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
          <div class="collection-info"> 
           
                                    
            <h2 style="color:#ffffff">
              
              <a href="/collections/all" style="color:#ffffff">
                
              Just Launched
              </a>      
              </h2>        
                               
            <a href="/collections/all" class="btn">View Details <i class="zmdi zmdi-long-arrow-right"></i></a>
          
           </div>
          </div>
          
               
          
        </div>
      </div>         
          
          
      <div class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item section-wrapper section-wrapper-1492003551011-1" style="background: #000000;"> 
        <div class="section-inner">        
            
            <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item left">
          
              <a href="/collections/all" class="collection-img" >
              <img src="//cdn.shopify.com/s/files/1/1811/9385/files/home-3.jpg?v=1496145718" alt="Best Summer Sale"/>
              </a>
           
          </div>
           
          
         <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
          <div class="collection-info"> 
           
                                    
            <h2 style="color:#ffffff">
              
              <a href="/collections/all" style="color:#ffffff">
                
              Best Summer Sale
              </a>      
              </h2>        
                               
            <a href="/collections/all" class="btn">View Details <i class="zmdi zmdi-long-arrow-right"></i></a>
          
           </div>
          </div>
          
               
          
        </div>
      </div>         
      
          
   
  </div> 
    

</div>


    
<style type="text/css">
  .grid-banner-type-9 .section-wrapper-1492003551011-0:hover  { background:#d2aa5c !important;}  
  .grid-banner-type-9 .section-wrapper-1492003551011-0 .btn { background: #d2aa5c;color: #fff;}
  .grid-banner-type-9 .section-wrapper-1492003551011-0 .btn:hover {background: #fff;color: #000;}
</style>
    
<style type="text/css">
  .grid-banner-type-9 .section-wrapper-1492003551011-1:hover  { background:#d2aa5c !important;}  
  .grid-banner-type-9 .section-wrapper-1492003551011-1 .btn { background: #d2aa5c;color: #fff;}
  .grid-banner-type-9 .section-wrapper-1492003551011-1 .btn:hover {background: #fff;color: #000;}
</style>




</div>
<div id="shopify-section-1492003917601" class="shopify-section index-section"><div data-section-id="1492003917601" data-section-type="grid-banner-type-9" class="grid-banner-type-9">  
      <div class="grid-uniform">        
            
          
          
      <div class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item section-wrapper section-wrapper-1492003917601-0" style="background: #000000;"> 
        <div class="section-inner">        
           
          
         <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
          <div class="collection-info"> 
           
                                    
            <h2 style="color:#ffffff">
              
              <a href="/collections/all" style="color:#ffffff">
                
              Necklaces
              </a>      
              </h2>        
                               
            <a href="/collections/all" class="btn">View Details <i class="zmdi zmdi-long-arrow-right"></i></a>
          
           </div>
          </div>
          
              
            <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item right">
          
          <a href="/collections/all" class="collection-img" >
              <img src="//cdn.shopify.com/s/files/1/1811/9385/files/home-2_ff355ce1-e9a1-4b43-9ceb-ab1665620f99.jpg?v=1496145737" alt="Necklaces" />
              </a>
           
          </div>
             
          
        </div>
      </div>         
          
          
      <div class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item section-wrapper section-wrapper-1492003917601-1" style="background: #000000;"> 
        <div class="section-inner">        
           
          
         <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item">
          <div class="collection-info"> 
           
                                    
            <h2 style="color:#ffffff">
              
              <a href="/collections/all" style="color:#ffffff">
                
              Diamond Chain
              </a>      
              </h2>        
                               
            <a href="/collections/all" class="btn">View Details <i class="zmdi zmdi-long-arrow-right"></i></a>
          
           </div>
          </div>
          
              
            <div class="grid__item wide--one-half post-large--one-half large--one-half medium--one-half small-grid__item right">
          
          <a href="/collections/all" class="collection-img" >
              <img src="//cdn.shopify.com/s/files/1/1811/9385/files/home-4.jpg?v=1496145749" alt="Diamond Chain" />
              </a>
           
          </div>
             
          
        </div>
      </div>         
      
          
   
  </div> 
    

</div>


    
<style type="text/css">
  .grid-banner-type-9 .section-wrapper-1492003917601-0:hover  { background:#d2aa5c !important;}  
  .grid-banner-type-9 .section-wrapper-1492003917601-0 .btn { background: #d2aa5c;color: #fff;}
  .grid-banner-type-9 .section-wrapper-1492003917601-0 .btn:hover {background: #ffffff;color: #000;}
</style>
    
<style type="text/css">
  .grid-banner-type-9 .section-wrapper-1492003917601-1:hover  { background:#d2aa5c !important;}  
  .grid-banner-type-9 .section-wrapper-1492003917601-1 .btn { background: #d2aa5c;color: #fff;}
  .grid-banner-type-9 .section-wrapper-1492003917601-1 .btn:hover {background: #fff;color: #000;}
</style>




</div>
<div id="shopify-section-1492059459559" class="shopify-section index-section"><div data-section-id="1492059459559"  data-section-type="home-product-grid-type-6" class="home-product-grid-type-6"><div class="dt-sc-hr-invisible-large"></div> 
  <div class="full_width_tab load-wrapper">       
    
    <div class="section-header section-header--small">
      <div class="border-title">
                    
        
        <h2 class="section-header__title" style="color:#000;">    
          New Arrivals
        </h2>
          
         <div class="short-desc"> <p style="color:#000;">Just in now</p></div>  
        
      </div>
    </div>    
    <div class="dt-sc-hr-invisible-small"></div>
          
    

    <div class="grid-uniform">
      

      <div class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item">              
        <ul class="type4__items">
          
          










<li class="grid__item item-row  wide--one-third post-large--one-third large--one-third medium--one-third small-grid__item" id="product-10848592526"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/new-arrivals/products/barry-gold-bangle-for-her" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/10_large.jpg?v=1496149646" class="featured-image" alt="Barry Gold Bangle for her">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10848592526">                                    
        <input type="hidden" name="id" value="42024508430" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-barry-gold-bangle-for-her loading"><a class="add-in-wishlist-js btn" href="barry-gold-bangle-for-her"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-barry-gold-bangle-for-her loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="barry-gold-bangle-for-her"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-barry-gold-bangle-for-her loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/new-arrivals/products/barry-gold-bangle-for-her" class="grid-link__title">Barry Gold Bangle for her</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>CaratLane</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$765.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10848592526"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
          
          
          










<li class="grid__item item-row  wide--one-third post-large--one-third large--one-third medium--one-third small-grid__item" id="product-10109324238"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/new-arrivals/products/product-name" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/11_large.jpg?v=1496149366" class="featured-image" alt="New Trendy copper bangle">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109324238">                                    
        <input type="hidden" name="id" value="37510519438" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name loading"><a class="add-in-wishlist-js btn" href="product-name"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/new-arrivals/products/product-name" class="grid-link__title">New Trendy copper bangle</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>Tradino</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$50.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109324238"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
          
          
          










<li class="grid__item item-row  wide--one-third post-large--one-third large--one-third medium--one-third small-grid__item" id="product-10109323854"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/new-arrivals/products/grooved-texture-gold-bangle-set" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/9_large.jpg?v=1496148941" class="featured-image" alt="Grooved Texture Gold Bangle Set">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109323854">                                    
        <input type="hidden" name="id" value="37510518926" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-grooved-texture-gold-bangle-set loading"><a class="add-in-wishlist-js btn" href="grooved-texture-gold-bangle-set"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-grooved-texture-gold-bangle-set loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="grooved-texture-gold-bangle-set"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-grooved-texture-gold-bangle-set loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/new-arrivals/products/grooved-texture-gold-bangle-set" class="grid-link__title">Grooved Texture Gold Bangle Set</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>CaratLane</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$765.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109323854"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
          
          
          










<li class="grid__item item-row  wide--one-third post-large--one-third large--one-third medium--one-third small-grid__item" id="product-10109317582"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/new-arrivals/products/classic-prong-set-ring-mount" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/4_large.jpg?v=1496145230" class="featured-image" alt="Classic Prong Set Ring Mount">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109317582">                                    
        <input type="hidden" name="id" value="37510483662" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-classic-prong-set-ring-mount loading"><a class="add-in-wishlist-js btn" href="classic-prong-set-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-classic-prong-set-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="classic-prong-set-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-classic-prong-set-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/new-arrivals/products/classic-prong-set-ring-mount" class="grid-link__title">Classic Prong Set Ring Mount</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>CaratLane</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$199.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109317582"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
          
          
          










<li class="grid__item item-row  wide--one-third post-large--one-third large--one-third medium--one-third small-grid__item" id="product-10109316878"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/new-arrivals/products/amor-solitaire-ring-mount" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      <div class="reveal"> 
        <span class="product-additional">      
          <img src="//cdn.shopify.com/s/files/1/1811/9385/products/3_large.jpg?v=1540785990" class="featured-image" alt="Amor Solitaire Ring Mount">
        </span>
        <img  src="//cdn.shopify.com/s/files/1/1811/9385/products/4_884ddec6-3cdf-462a-b72a-c330074faca3_large.jpg?v=1540785990" class="hidden-feature_img" alt="Amor Solitaire Ring Mount" />
      </div> 
      
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109316878">                                    
        <input type="hidden" name="id" value="37510477518" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-amor-solitaire-ring-mount loading"><a class="add-in-wishlist-js btn" href="amor-solitaire-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-amor-solitaire-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="amor-solitaire-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-amor-solitaire-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/new-arrivals/products/amor-solitaire-ring-mount" class="grid-link__title">Amor Solitaire Ring Mount</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>Crocs</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$700.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109316878"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
          
          
          










<li class="grid__item item-row  wide--one-third post-large--one-third large--one-third medium--one-third small-grid__item" id="product-10109315918"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/new-arrivals/products/lush-ring-mount" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/2_ac2c90d2-fae2-47b5-9c9c-7b73500c454a_large.jpg?v=1496144771" class="featured-image" alt="Lush Ring Mount">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109315918">                                    
        <input type="hidden" name="id" value="37510472974" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-lush-ring-mount loading"><a class="add-in-wishlist-js btn" href="lush-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-lush-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="lush-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-lush-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/new-arrivals/products/lush-ring-mount" class="grid-link__title">Lush Ring Mount</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>Glamr</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$620.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109315918"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
          
          
        </ul>      
      </div>
      
      <div class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item right">
        <ul class="type4__singele_items">
          
          











<li class="deal grid grid__item item-row  " id="product-10109315278"  >
  <div class="products   wow fadeIn  ">
    <div class="product-container">  
      
      
          
  <div class="deal-clock-single lof-clock-timer-detail-single"></div>
  <script>
  //<![CDATA[
  jQuery(document).ready(function($){
    $(".lof-clock-timer-detail-single").lofCountDown({      
      TargetDate:"",
      DisplayFormat:"<ul class='list-inline'><li class='day'>%%D%%<span>Days</span></li><li class='hours'>%%H%%<span>Hours</span></li><li class='mins'>%%M%%<span>Minutes</span></li><li class='seconds'>%%S%%<span>Seconds</span></li></ul>",
      FinishMessage: "Expired"
      });
  });
  //]]>
</script>
      
      <a href="/collections/new-arrivals/products/promise-solitaire-ring-mount" class="grid-link">    
        
        
        
           
        
        
        
        
        <div class="reveal"> 
          <span class="product-additional">      
              
            <img  src="//cdn.shopify.com/s/files/1/1811/9385/files/home-8_731c34f9-2ee1-4dac-9d40-60d26cf3e50a_1024x.jpg?v=1496232909" alt="New Arrivals" class="featured-image"  />
            
          </span>    
            
          <img  src="//cdn.shopify.com/s/files/1/1811/9385/files/product-hover-ad_716cc867-d9aa-4f48-9970-e4764c3bdb84_1024x.jpg?v=1496232882" alt="New Arrivals"  class="hidden-feature_img"/>
                 
        </div> 
        
       
       
        
      </a>
      <div class="ImageWrapper">
         <div class="product-button">  
                 
          <a href="javascript:void(0)" id="promise-solitaire-ring-mount" class="quick-view-text">                      
            <i class="zmdi zmdi-eye"></i>
          </a>       
          
                  
          <a href="/products/promise-solitaire-ring-mount">                      
            <i class="zmdi zmdi-link"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-promise-solitaire-ring-mount loading"><a class="add-in-wishlist-js btn" href="promise-solitaire-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-promise-solitaire-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="promise-solitaire-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-promise-solitaire-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      <div class="product_left">
      <a href="/collections/new-arrivals/products/promise-solitaire-ring-mount" class="grid-link__title">Let Promise Solitaire Ring Mount</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$590.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109315278"></span>
      </div>
      
      
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

   </div>
      
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109315278">                                    
        <input type="hidden" name="id" value="37510470862" />  
         <a class="add-cart-btn">
        <i class="icon-basket"></i>
          </a>
      </form>  
      
      
      </div> 
  </div>
</li>
          
          
        </ul>
      </div>
      
    </div>
  </div>
   <style>
     .home-product-grid-type-6 .border-title:after {  content:"";background-image:url(//cdn.shopify.com/s/files/1/1811/9385/files/divider1.png?v=1496672279);display:inline-block;background-repeat:no-repeat;width:136px;height:15px;} 

  </style>
  <div class="dt-sc-hr-invisible-large"></div> 
</div>  

 
  




</div><div id="shopify-section-1492056756357" class="shopify-section index-section"><div data-section-id="1492056756357" data-section-type="grid-banner-type-7" class="grid-banner-type-7">  
  <div class="grid-uniform">        
          
        
    
    
    <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half small-grid__item"> 
      <div class="img-hover-effect bg-effect-1492056756357-0">      
 <div class="ovrly09">  
        
        <img src="//cdn.shopify.com/s/files/1/1811/9385/files/home-5_8d09cc6a-4b17-42d1-8b01-8ac0742de5ea_700x.jpg?v=1496149968" alt="Vivaldi me" />        
        <div class="overlay"></div>
         
      <div class="block-content">              
                                
        <h6 style="color:#ffffff">Vivaldi me</h6>        
           
                                 
        <h4 style="color:#ffffff">Diamond’s Haven</h4>        
         
                                 
        <p style="color:#ffffff;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>        
         
        <a style="color:#ffffff" href="/collections/all" onmouseover='this.style.color="#ffffff"' onmouseout='this.style.color="#ffffff"' class="">Shop Now </a>
      </div> 
        </div>
      </div>
    </div>
   
    
         
    
    
    <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half small-grid__item"> 
      <div class="img-hover-effect bg-effect-1492056756357-1">      
 <div class="ovrly09">  
        
        <img src="//cdn.shopify.com/s/files/1/1811/9385/files/home-6_6726457a-d591-41b8-b03e-f8b96d7e178a_700x.jpg?v=1496149812" alt="Tress" />        
        <div class="overlay"></div>
         
      <div class="block-content">              
                                
        <h6 style="color:#ffffff">Tress</h6>        
           
                                 
        <h4 style="color:#ffffff">Exclusive n Elegant</h4>        
         
         
        <a style="color:#ffffff" href="/collections/all" onmouseover='this.style.color="#ffffff"' onmouseout='this.style.color="#ffffff"' class="">Shop Now </a>
      </div> 
        </div>
      </div>
    </div>
   
    
         
    
    
    <div class="grid__item wide--one-third post-large--one-third large--one-third medium--one-half small-grid__item"> 
      <div class="img-hover-effect bg-effect-1492056884112">      
 <div class="ovrly09">  
        
        <img src="//cdn.shopify.com/s/files/1/1811/9385/files/home-7_8f65301e-6216-4443-8514-28e6cff80c9f_700x.jpg?v=1496149553" alt="Tress" />        
        <div class="overlay"></div>
         
      <div class="block-content">              
                                
        <h6 style="color:#ffffff">Tress</h6>        
           
                                 
        <h4 style="color:#ffffff">Soul Searcher</h4>        
         
                                 
        <p style="color:#ffffff;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</p>        
         
        <a style="color:#ffffff" href="/collections/all" onmouseover='this.style.color="#fff"' onmouseout='this.style.color="#ffffff"' class="">Shop Now </a>
      </div> 
        </div>
      </div>
    </div>
   
    
     
    
  </div>
     
</div>
<div class="dt-sc-hr-invisible-large"></div> 


      
    
    <style>      
      
      .grid-banner-type-7 .bg-effect-1492056756357-0 .overlay { background:rgba(0, 0, 0, 0.7); }
      
  </style>
    
    <style>      
      
      .grid-banner-type-7 .bg-effect-1492056756357-1 .overlay { background:rgba(0, 0, 0, 0.7); }
      
  </style>
    
    <style>      
      
      .grid-banner-type-7 .bg-effect-1492056884112 .overlay { background:rgba(0, 0, 0, 0.7); }
      
  </style>
    
    



</div><div id="shopify-section-1492061167634" class="shopify-section index-section"><div data-section-id="1492061167634"  data-section-type="home-product-grid-type-13" class="home-product-grid-type-13">
 <div class="container"><div class="product-grid-type-13">  
    
    <div class="border-title">
      
          
          <h2 class="section-header__title" style="color:#000;">    
            Rare Collections
          </h2>
                
      <div class="short-desc"> <p style="color:#000;">Carefully crafted</p></div>  
       
      </div>
    <div class="dt-sc-hr-invisible-small"></div>
     
<ul class="grid-uniform product-grid-type__13">
        
        










<li class="grid__item item-row  wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item" id="product-10848622862"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/rare-collections/products/caren-swirl-stud-earrings" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/15_large.jpg?v=1496150696" class="featured-image" alt="Qaren Swirl Stud Earrings">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="caren-swirl-stud-earrings" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10848622862">                                    
        <input type="hidden" name="id" value="42025126606" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/caren-swirl-stud-earrings">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-caren-swirl-stud-earrings loading"><a class="add-in-wishlist-js btn" href="caren-swirl-stud-earrings"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-caren-swirl-stud-earrings loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="caren-swirl-stud-earrings"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-caren-swirl-stud-earrings loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/rare-collections/products/caren-swirl-stud-earrings" class="grid-link__title">Qaren Swirl Stud Earrings</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$765.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10848622862"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
        
        










<li class="grid__item item-row  wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item" id="product-10109336206"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/rare-collections/products/product-name-87" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/14_large.jpg?v=1496150074" class="featured-image" alt="Xvino Nakshi Beads Necklace - Chokapora">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name-87" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109336206">                                    
        <input type="hidden" name="id" value="37510593806" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name-87">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-87 loading"><a class="add-in-wishlist-js btn" href="product-name-87"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-87 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-87"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-87 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/rare-collections/products/product-name-87" class="grid-link__title">Xvino Nakshi Beads Necklace - Chokapora</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$765.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109336206"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
        
        










<li class="grid__item item-row  wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item on-sale" id="product-10109330318"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/rare-collections/products/product-name-83" class="grid-link">    
        
        
        <div class="featured-tag">
        <span class="badge badge--sale">
          
          <span class="gift-tag badge__text">Sale</span>
        </span>
        </div>
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      <div class="reveal"> 
        <span class="product-additional">      
          <img src="//cdn.shopify.com/s/files/1/1811/9385/products/12_large.jpg?v=1496150442" class="featured-image" alt="Welaribe Star Moon Necklace">
        </span>
        <img  src="//cdn.shopify.com/s/files/1/1811/9385/products/14_large_ec6cc15a-2d75-4748-bc37-ba2dbe299601_large.jpg?v=1537862649" class="hidden-feature_img" alt="Welaribe Star Moon Necklace" />
      </div> 
      
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name-83" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109330318">                                    
        <input type="hidden" name="id" value="37510561294" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name-83">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-83 loading"><a class="add-in-wishlist-js btn" href="product-name-83"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-83 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-83"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-83 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/rare-collections/products/product-name-83" class="grid-link__title">Welaribe Star Moon Necklace</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$400.00</span>
          </div>
        
        
        <del class="grid-link__sale_price"><span class=money>$450.00</span></del>
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109330318"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  
    
    
    
    
      
        
      
      <li>
       <label style="background-color: gold; background-image: url(//cdn.shopify.com/s/files/1/1811/9385/files/gold.png?3493);"></label>
        
        <div class="hidden">
          <img src="//cdn.shopify.com/s/files/1/1811/9385/products/12_grande.jpg?v=1496150442" alt="gold"/>
        </div>
        
      </li>
        
        
      
    
      
        
      
      <li>
       <label style="background-color: green; background-image: url(//cdn.shopify.com/s/files/1/1811/9385/files/green.png?3493);"></label>
        
        <div class="hidden">
          <img src="//cdn.shopify.com/s/files/1/1811/9385/products/13_large_a68f8af4-e9a1-4056-aacf-423dea679c4e_grande.jpg?v=1537862648" alt="green"/>
        </div>
        
      </li>
        
        
      
    
      
        
      
      <li>
       <label style="border: 1px solid #cbcbcb; background-color: white; background-image: url(//cdn.shopify.com/s/files/1/1811/9385/files/white.png?3493);"></label>
        
        <div class="hidden">
          <img src="//cdn.shopify.com/s/files/1/1811/9385/products/14_large_ec6cc15a-2d75-4748-bc37-ba2dbe299601_grande.jpg?v=1537862649" alt="white"/>
        </div>
        
      </li>
        
        
      
    
  

</ul>

        
    
     
  </div>
  </div>
</li>
          
        
        










<li class="grid__item item-row  wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item" id="product-10109326798"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/rare-collections/products/product-name-78" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/13_large.jpg?v=1496149831" class="featured-image" alt="Zombi Lotus and Coin Gold Necklace">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name-78" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109326798">                                    
        <input type="hidden" name="id" value="37510546510" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name-78">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-78 loading"><a class="add-in-wishlist-js btn" href="product-name-78"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-78 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-78"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-78 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/rare-collections/products/product-name-78" class="grid-link__title">Zombi Lotus and Coin Gold Necklace</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$90.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109326798"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
        
        










<li class="grid__item item-row  wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item" id="product-10109324238"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/rare-collections/products/product-name" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/11_large.jpg?v=1496149366" class="featured-image" alt="New Trendy copper bangle">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109324238">                                    
        <input type="hidden" name="id" value="37510519438" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name loading"><a class="add-in-wishlist-js btn" href="product-name"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/rare-collections/products/product-name" class="grid-link__title">New Trendy copper bangle</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$50.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109324238"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                
        <li class="grid__item wide--three-quarters post-large--three-quarters large--three-quarters medium--one-half small-grid__item">  
          <div class="collection_content_section">
          <div class="grid__item wide--one-third post-large--one-third large--one-third medium--grid__item small-grid__item">
          
        <img src="//cdn.shopify.com/s/files/1/1811/9385/files/page-collection.jpg?v=1496233004" alt="Platinum Necklace" />
               
          </div>
          <div class="collection-content grid__item wide--two-thirds post-large--two-thirds large--two-thirds medium--hide small--hide">
                                
        <h2 style="color:#000">Platinum Necklace</h2>        
           
                                 
        <h4 style="color:#000">Necklace &amp; Pendant Set</h4>        
         
                                 
        <p style="color:#000;">Huge Jewelry
Pellentesque posuere orci lobortis scelerisque blandit. Donec id tellus lacinia an, tincidunt risus ac, consequat velit. Quisquemos sodales suscipit tortor ditaemcos condimentum lacus meleifend medimentum lacus meleifedimentum lacus meleifend menean nd menean nean viverra auctor blanditos.</p>        
         
        <a href="/collections/all" style="" class="btn">View all collection</a>
          </div>
          </div>
            </li>
         <div class="collection-content grid__item wide--hide post-large--hide large--hide medium--grid__item small-grid__item">
                                
        <h2 style="color:#000">Platinum Necklace</h2>        
           
                                 
        <h4 style="color:#000">Necklace &amp; Pendant Set</h4>        
         
                                 
        <p style="color:#000;">Huge Jewelry
Pellentesque posuere orci lobortis scelerisque blandit. Donec id tellus lacinia an, tincidunt risus ac, consequat velit. Quisquemos sodales suscipit tortor ditaemcos condimentum lacus meleifend medimentum lacus meleifedimentum lacus meleifend menean nd menean nean viverra auctor blanditos.</p>        
         
        <a href="/collections/all" style="" class="btn">View all collection</a>
          </div>
      </ul>
     <ul class="grid-uniform product-grid-type__13">
      
        










<li class="grid__item item-row  wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item on-sale" id="product-10109321806"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/rare-collections/products/product-name-70" class="grid-link">    
        
        
        <div class="featured-tag">
        <span class="badge badge--sale">
          
          <span class="gift-tag badge__text">Sale</span>
        </span>
        </div>
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/8_large.jpg?v=1496148739" class="featured-image" alt="Crumpled hard Ring">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name-70" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109321806">                                    
        <input type="hidden" name="id" value="37510511886" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name-70">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-70 loading"><a class="add-in-wishlist-js btn" href="product-name-70"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-70 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-70"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-70 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/rare-collections/products/product-name-70" class="grid-link__title">Crumpled hard Ring</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$60.00</span>
          </div>
        
        
        <del class="grid-link__sale_price"><span class=money>$75.00</span></del>
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109321806"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
        
        










<li class="grid__item item-row  wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item" id="product-10109320334"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/rare-collections/products/product-name-69" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/7_large.jpg?v=1496148582" class="featured-image" alt="Light Stone mounted Ring">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name-69" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109320334">                                    
        <input type="hidden" name="id" value="37510503694" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name-69">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-69 loading"><a class="add-in-wishlist-js btn" href="product-name-69"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-69 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-69"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-69 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/rare-collections/products/product-name-69" class="grid-link__title">Light Stone mounted Ring</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$299.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109320334"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
        
        










<li class="grid__item item-row  wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item on-sale" id="product-10109319118"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/rare-collections/products/product-name-68" class="grid-link">    
        
        
        <div class="featured-tag">
        <span class="badge badge--sale">
          
          <span class="gift-tag badge__text">Sale</span>
        </span>
        </div>
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/6_large.jpg?v=1496146889" class="featured-image" alt="Plaited Shank Ring Mount">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name-68" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109319118">                                    
        <input type="hidden" name="id" value="37510498254" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name-68">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-68 loading"><a class="add-in-wishlist-js btn" href="product-name-68"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-68 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-68"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-68 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/rare-collections/products/product-name-68" class="grid-link__title">Plaited Shank Ring Mount</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$99.00</span>
          </div>
        
        
        <del class="grid-link__sale_price"><span class=money>$109.00</span></del>
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109319118"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
         
       <li class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small--grid__item">
         <div class="collection-right">
          
        <img src="//cdn.shopify.com/s/files/1/1811/9385/files/page-collection2.jpg?v=1496233022" alt="Platinum Necklace" />
            
         </div>
       </li>
         
    </ul>
    
    </div>
   <style>
     .home-product-grid-type-13 .collection-content .btn { color:#fff;border:none;background:#000; }
     .home-product-grid-type-13 .collection-content .btn:hover { color:#ffffff;background:#d2aa5c; }
     .home-product-grid-type-13 .border-title:after {  content:"";background-image:url(//cdn.shopify.com/s/files/1/1811/9385/files/divider2.png?v=1496673430);display:inline-block;background-repeat:no-repeat;width:136px;height:15px;} 
   </style>
    <div class="dt-sc-hr-invisible-large"></div> 
  </div> 
  </div>
 










</div><div id="shopify-section-1492061373549" class="shopify-section index-section"><div data-section-id="1492061373549"  data-section-type="home-product-grid-type-3" class="home-product-grid-type-3"><div class="grid--uniform">
    <div class="grid__item wide--one-quarter post-large--two-quarters large--two-quarters medium-down--grid__item small--grid__item"> 
      <div id="type-3__carousel_banner">
        <div class="img-hover-effect">
        <div class="ovrly23">
          
          
          <img src="//cdn.shopify.com/s/files/1/1811/9385/files/page-collection3_500x.jpg?v=1496233077" alt="Handpicked products" />
          <div class="overlay" style="background:rgba(0, 0, 0, 0.5);"></div>
                     
          
          <div class="featured-content"> 
                       
             
            <p style="color:#ffffff;">Pandora Rings</p>
            
            
            <h2 style="color:#ffffff;">Handpicked products</h2>
            
            
            <h6 style="color:#ffffff;">Fine silver and gold bands with intricate details and gemstone accents</h6>    
              
            
            <a class="btn" href="" style="color:#fff;border:none;background-color:#000000;">Have a look</a>    
                     
          </div>   
        </div>    
        </div>
      </div>
    </div>
   <div class="grid__item wide--three-quarters post-large--two-quarters large--two-quarters medium-down--grid__item small--grid__item"> 
     <div class="home-product-grid-type-3-carousel">  
    <div class="full_width_tab type-3__items-wrapper owl-carousel owl-theme">                            
       <ul class="type3__items Handpicked products">                       
                            
           
         
        










<li class="grid__item item-row   " id="product-10109315918"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/lush-ring-mount" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/2_ac2c90d2-fae2-47b5-9c9c-7b73500c454a_large.jpg?v=1496144771" class="featured-image" alt="Lush Ring Mount">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109315918">                                    
        <input type="hidden" name="id" value="37510472974" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-lush-ring-mount loading"><a class="add-in-wishlist-js btn" href="lush-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-lush-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="lush-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-lush-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/lush-ring-mount" class="grid-link__title">Lush Ring Mount</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>Glamr</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$620.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109315918"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
            
         
        










<li class="grid__item item-row   " id="product-10109317582"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/classic-prong-set-ring-mount" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/4_large.jpg?v=1496145230" class="featured-image" alt="Classic Prong Set Ring Mount">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109317582">                                    
        <input type="hidden" name="id" value="37510483662" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-classic-prong-set-ring-mount loading"><a class="add-in-wishlist-js btn" href="classic-prong-set-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-classic-prong-set-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="classic-prong-set-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-classic-prong-set-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/classic-prong-set-ring-mount" class="grid-link__title">Classic Prong Set Ring Mount</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>CaratLane</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$199.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109317582"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
         
         </ul>
      <ul class="type3__items">                       
        
         
        
            
         
        










<li class="grid__item item-row   " id="product-10109316878"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/amor-solitaire-ring-mount" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      <div class="reveal"> 
        <span class="product-additional">      
          <img src="//cdn.shopify.com/s/files/1/1811/9385/products/3_large.jpg?v=1540785990" class="featured-image" alt="Amor Solitaire Ring Mount">
        </span>
        <img  src="//cdn.shopify.com/s/files/1/1811/9385/products/4_884ddec6-3cdf-462a-b72a-c330074faca3_large.jpg?v=1540785990" class="hidden-feature_img" alt="Amor Solitaire Ring Mount" />
      </div> 
      
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109316878">                                    
        <input type="hidden" name="id" value="37510477518" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-amor-solitaire-ring-mount loading"><a class="add-in-wishlist-js btn" href="amor-solitaire-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-amor-solitaire-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="amor-solitaire-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-amor-solitaire-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/amor-solitaire-ring-mount" class="grid-link__title">Amor Solitaire Ring Mount</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>Crocs</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$700.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109316878"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
         
         
        
            
         
        










<li class="grid__item item-row   " id="product-10848592526"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/barry-gold-bangle-for-her" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/10_large.jpg?v=1496149646" class="featured-image" alt="Barry Gold Bangle for her">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10848592526">                                    
        <input type="hidden" name="id" value="42024508430" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-barry-gold-bangle-for-her loading"><a class="add-in-wishlist-js btn" href="barry-gold-bangle-for-her"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-barry-gold-bangle-for-her loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="barry-gold-bangle-for-her"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-barry-gold-bangle-for-her loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/barry-gold-bangle-for-her" class="grid-link__title">Barry Gold Bangle for her</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>CaratLane</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$765.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10848592526"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
         
         </ul>
      <ul class="type3__items">                       
        
         
        
            
         
        










<li class="grid__item item-row   " id="product-10109323854"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/grooved-texture-gold-bangle-set" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/9_large.jpg?v=1496148941" class="featured-image" alt="Grooved Texture Gold Bangle Set">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109323854">                                    
        <input type="hidden" name="id" value="37510518926" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-grooved-texture-gold-bangle-set loading"><a class="add-in-wishlist-js btn" href="grooved-texture-gold-bangle-set"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-grooved-texture-gold-bangle-set loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="grooved-texture-gold-bangle-set"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-grooved-texture-gold-bangle-set loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/grooved-texture-gold-bangle-set" class="grid-link__title">Grooved Texture Gold Bangle Set</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>CaratLane</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$765.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109323854"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
         
         
        
            
         
        










<li class="grid__item item-row   " id="product-10109318286"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/hera-solitaire-ring" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/5_large.jpg?v=1496145595" class="featured-image" alt="Hera Solitaire Ring">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109318286">                                    
        <input type="hidden" name="id" value="37510488142" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-hera-solitaire-ring loading"><a class="add-in-wishlist-js btn" href="hera-solitaire-ring"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-hera-solitaire-ring loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="hera-solitaire-ring"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-hera-solitaire-ring loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/hera-solitaire-ring" class="grid-link__title">Hera Solitaire Ring</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>Glamr</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$299.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109318286"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
         
         </ul>
      <ul class="type3__items">                       
        
         
        
            
         
        










<li class="grid__item item-row   " id="product-10109326798"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name-78" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/13_large.jpg?v=1496149831" class="featured-image" alt="Zombi Lotus and Coin Gold Necklace">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109326798">                                    
        <input type="hidden" name="id" value="37510546510" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-78 loading"><a class="add-in-wishlist-js btn" href="product-name-78"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-78 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-78"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-78 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name-78" class="grid-link__title">Zombi Lotus and Coin Gold Necklace</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>Latika</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$90.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109326798"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
         
         
        
            
         
        










<li class="grid__item item-row   " id="product-10109336206"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name-87" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/14_large.jpg?v=1496150074" class="featured-image" alt="Xvino Nakshi Beads Necklace - Chokapora">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109336206">                                    
        <input type="hidden" name="id" value="37510593806" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-87 loading"><a class="add-in-wishlist-js btn" href="product-name-87"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-87 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-87"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-87 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name-87" class="grid-link__title">Xvino Nakshi Beads Necklace - Chokapora</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>Nakshi</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$765.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109336206"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
         
         </ul>
      <ul class="type3__items">                       
        
         
        
            
         
        










<li class="grid__item item-row   " id="product-10848622862"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/caren-swirl-stud-earrings" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/15_large.jpg?v=1496150696" class="featured-image" alt="Qaren Swirl Stud Earrings">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10848622862">                                    
        <input type="hidden" name="id" value="42025126606" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-caren-swirl-stud-earrings loading"><a class="add-in-wishlist-js btn" href="caren-swirl-stud-earrings"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-caren-swirl-stud-earrings loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="caren-swirl-stud-earrings"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-caren-swirl-stud-earrings loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/caren-swirl-stud-earrings" class="grid-link__title">Qaren Swirl Stud Earrings</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>Nakshi</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$765.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10848622862"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
         
         
        
            
         
        










<li class="grid__item item-row    on-sale" id="product-10109330318"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name-83" class="grid-link">    
        
        
        <div class="featured-tag">
        <span class="badge badge--sale">
          
          <span class="gift-tag badge__text">Sale</span>
        </span>
        </div>
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      <div class="reveal"> 
        <span class="product-additional">      
          <img src="//cdn.shopify.com/s/files/1/1811/9385/products/12_large.jpg?v=1496150442" class="featured-image" alt="Welaribe Star Moon Necklace">
        </span>
        <img  src="//cdn.shopify.com/s/files/1/1811/9385/products/14_large_ec6cc15a-2d75-4748-bc37-ba2dbe299601_large.jpg?v=1537862649" class="hidden-feature_img" alt="Welaribe Star Moon Necklace" />
      </div> 
      
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109330318">                                    
        <input type="hidden" name="id" value="37510561294" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-83 loading"><a class="add-in-wishlist-js btn" href="product-name-83"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-83 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-83"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-83 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name-83" class="grid-link__title">Welaribe Star Moon Necklace</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>Maribel</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$400.00</span>
          </div>
        
        
        <del class="grid-link__sale_price"><span class=money>$450.00</span></del>
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109330318"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
         
         </ul>
      <ul class="type3__items">                       
        
         
        
            
         
        










<li class="grid__item item-row    on-sale" id="product-10109319118"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name-68" class="grid-link">    
        
        
        <div class="featured-tag">
        <span class="badge badge--sale">
          
          <span class="gift-tag badge__text">Sale</span>
        </span>
        </div>
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/6_large.jpg?v=1496146889" class="featured-image" alt="Plaited Shank Ring Mount">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109319118">                                    
        <input type="hidden" name="id" value="37510498254" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-68 loading"><a class="add-in-wishlist-js btn" href="product-name-68"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-68 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-68"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-68 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name-68" class="grid-link__title">Plaited Shank Ring Mount</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>CaratLane</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$99.00</span>
          </div>
        
        
        <del class="grid-link__sale_price"><span class=money>$109.00</span></del>
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109319118"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
         
         
        
            
         
        










<li class="grid__item item-row   " id="product-10109320334"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name-69" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/7_large.jpg?v=1496148582" class="featured-image" alt="Light Stone mounted Ring">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109320334">                                    
        <input type="hidden" name="id" value="37510503694" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-69 loading"><a class="add-in-wishlist-js btn" href="product-name-69"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-69 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-69"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-69 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name-69" class="grid-link__title">Light Stone mounted Ring</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>Glamr</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$299.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109320334"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
         
         </ul>
      <ul class="type3__items">                       
        
         
        
            
         
        










<li class="grid__item item-row   " id="product-10109324238"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/11_large.jpg?v=1496149366" class="featured-image" alt="New Trendy copper bangle">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109324238">                                    
        <input type="hidden" name="id" value="37510519438" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name loading"><a class="add-in-wishlist-js btn" href="product-name"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name" class="grid-link__title">New Trendy copper bangle</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>Tradino</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$50.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109324238"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
         
         
        
            
         
        










<li class="grid__item item-row    on-sale" id="product-10109321806"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name-70" class="grid-link">    
        
        
        <div class="featured-tag">
        <span class="badge badge--sale">
          
          <span class="gift-tag badge__text">Sale</span>
        </span>
        </div>
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/8_large.jpg?v=1496148739" class="featured-image" alt="Crumpled hard Ring">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109321806">                                    
        <input type="hidden" name="id" value="37510511886" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-70 loading"><a class="add-in-wishlist-js btn" href="product-name-70"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-70 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-70"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-70 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name-70" class="grid-link__title">Crumpled hard Ring</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>Crocs</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$60.00</span>
          </div>
        
        
        <del class="grid-link__sale_price"><span class=money>$75.00</span></del>
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109321806"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
         
         </ul>
      <ul class="type3__items">                       
        
         
        
            
         
        










<li class="grid__item item-row   " id="product-10109315278"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/promise-solitaire-ring-mount" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/1_5c789c84-aba2-4dbb-8992-69166a320cac_large.jpg?v=1496144078" class="featured-image" alt="Let Promise Solitaire Ring Mount">
      
      
      
      </a>
     
        <div class="product-button">  
          
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109315278">                                    
        <input type="hidden" name="id" value="37510470862" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-promise-solitaire-ring-mount loading"><a class="add-in-wishlist-js btn" href="promise-solitaire-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-promise-solitaire-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="promise-solitaire-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-promise-solitaire-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/promise-solitaire-ring-mount" class="grid-link__title">Let Promise Solitaire Ring Mount</a>    
      
          <p class="product-vendor">
            <label>Brand:</label>
            <span>CaratLane</span>
          </p>
          
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$590.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109315278"></span>
      </div>
    
         
     
     
  </div>
  </div>
</li>
 
         
          
        
                 
        </ul>             
    </div>
      <div class="nav_type3__items">
            <a class="prev active btn"><i class="zmdi zmdi-arrow-left"></i></a>
            <a class="next btn"><i class="zmdi zmdi-arrow-right"></i></a>  
          </div>   
     </div>
    </div> 
    
    <style>
     .home-product-grid-type-3 .featured-content a.btn:hover { background:#d2aa5c !important;color:#fff !important; }
    </style>
  </div>
  </div>
  <div class="dt-sc-hr-invisible-large"></div> 
<script type="text/javascript">
  $(document).ready(function(){
   var type3_products_count = $('.type3__items li.item-row').length;
      if(type3_products_count > 10) { $('.nav_type3__items').css('display','block');}
      else {$('.nav_type3__items').css('display','none');}
       var type3_products = $(".type-3__items-wrapper");
       type3_products.owlCarousel({
        items: 3,
        itemsCustom: false,
        itemsDesktop: [1199, 2],
        itemsDesktopSmall: [980, 1],
        itemsTablet: [630, 1],
        itemsTabletSmall: false,
        itemsMobile: [479, 1],
        singleItem: false,
        itemsScaleUp: false,
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
        autoPlay: false,
        stopOnHover: false,
        navigation: false,
        pagination:false
      });
      // Custom Navigation Events
      $(".nav_type3__items .next").click(function(){
        type3_products.trigger('owl.next');
      })
      $(".nav_type3__items .prev").click(function(){
        type3_products.trigger('owl.prev');
      }) 
  });
      
</script>




<style>
  
  .home-product-grid-type-3 .full_width_tab li .products { background:#f4f4f4; }
  
</style>



</div><div id="shopify-section-1492061512174" class="shopify-section index-section"><div data-section-id="1492061512174" data-section-type="home-product-grid-type-8" class="home-product-grid-type-8" style="float:left;width:100%;background-color:#f9f9f9;"><div class="dt-sc-hr-invisible-large"></div>
  <div class="wrapper">
  <div class="grid-uniform collectionItems">  
    
    <div class="grid__item wide--one-quarter post-large--one-third large--one-half medium--one-half">
      <div class="ovrly10">
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/files/home-9_ee3fcc0c-3865-442c-a066-e9f5e318d521_500x.jpg?v=1496152320" alt="Make your day <br> Brighter" />
        <div style="background:rgba(0, 0, 0, 0.7);"  class="ovrly"></div>
                 
      
      <div class="featured-content">       
        
        <h4 style="color:#ffffff;">Accessories</h4>    
          
        
        <h2 style="color:#ffffff;">Make your day <br> Brighter</h2>
          
      </div> 
        <div class="collection_count">
         
        <span class="text_count" style="color:#d2aa5c;">16</span>    
          
          
        <span class="text_count text" style="color:#ffffff;">items</span>    
        
        </div>
        
    </div>
    </div>
    
    
    <div class="grid__item wide--three-quarters post-large--two-thirds large--one-half medium--one-half home-product-grid-type-8-right">
    <div class="type-8__items-wrapper">                            
         <ul class="type8__items wide-owl-1492061512174 owl-carousel owl-theme">                               
                     
          










<li class="grid__item item-row   " id="product-10109315918"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/lush-ring-mount" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/2_ac2c90d2-fae2-47b5-9c9c-7b73500c454a_large.jpg?v=1496144771" class="featured-image" alt="Lush Ring Mount">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="lush-ring-mount" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109315918">                                    
        <input type="hidden" name="id" value="37510472974" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/lush-ring-mount">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-lush-ring-mount loading"><a class="add-in-wishlist-js btn" href="lush-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-lush-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="lush-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-lush-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/lush-ring-mount" class="grid-link__title">Lush Ring Mount</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$620.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109315918"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row   " id="product-10109317582"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/classic-prong-set-ring-mount" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/4_large.jpg?v=1496145230" class="featured-image" alt="Classic Prong Set Ring Mount">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="classic-prong-set-ring-mount" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109317582">                                    
        <input type="hidden" name="id" value="37510483662" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/classic-prong-set-ring-mount">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-classic-prong-set-ring-mount loading"><a class="add-in-wishlist-js btn" href="classic-prong-set-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-classic-prong-set-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="classic-prong-set-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-classic-prong-set-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/classic-prong-set-ring-mount" class="grid-link__title">Classic Prong Set Ring Mount</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$199.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109317582"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row   " id="product-10109316878"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/amor-solitaire-ring-mount" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      <div class="reveal"> 
        <span class="product-additional">      
          <img src="//cdn.shopify.com/s/files/1/1811/9385/products/3_large.jpg?v=1540785990" class="featured-image" alt="Amor Solitaire Ring Mount">
        </span>
        <img  src="//cdn.shopify.com/s/files/1/1811/9385/products/4_884ddec6-3cdf-462a-b72a-c330074faca3_large.jpg?v=1540785990" class="hidden-feature_img" alt="Amor Solitaire Ring Mount" />
      </div> 
      
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="amor-solitaire-ring-mount" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109316878">                                    
        <input type="hidden" name="id" value="37510477518" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/amor-solitaire-ring-mount">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-amor-solitaire-ring-mount loading"><a class="add-in-wishlist-js btn" href="amor-solitaire-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-amor-solitaire-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="amor-solitaire-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-amor-solitaire-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/amor-solitaire-ring-mount" class="grid-link__title">Amor Solitaire Ring Mount</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$700.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109316878"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

  
    
    
    
    
      
        
      
      <li>
       <label style="background-color: silver; background-image: url(//cdn.shopify.com/s/files/1/1811/9385/files/silver.png?3493);"></label>
        
        <div class="hidden">
          <img src="//cdn.shopify.com/s/files/1/1811/9385/products/3_grande.jpg?v=1540785990" alt="silver"/>
        </div>
        
      </li>
        
        
      
    
      
      
    
  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row   " id="product-10848592526"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/barry-gold-bangle-for-her" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/10_large.jpg?v=1496149646" class="featured-image" alt="Barry Gold Bangle for her">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="barry-gold-bangle-for-her" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10848592526">                                    
        <input type="hidden" name="id" value="42024508430" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/barry-gold-bangle-for-her">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-barry-gold-bangle-for-her loading"><a class="add-in-wishlist-js btn" href="barry-gold-bangle-for-her"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-barry-gold-bangle-for-her loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="barry-gold-bangle-for-her"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-barry-gold-bangle-for-her loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/barry-gold-bangle-for-her" class="grid-link__title">Barry Gold Bangle for her</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$765.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10848592526"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row   " id="product-10109323854"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/grooved-texture-gold-bangle-set" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/9_large.jpg?v=1496148941" class="featured-image" alt="Grooved Texture Gold Bangle Set">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="grooved-texture-gold-bangle-set" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109323854">                                    
        <input type="hidden" name="id" value="37510518926" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/grooved-texture-gold-bangle-set">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-grooved-texture-gold-bangle-set loading"><a class="add-in-wishlist-js btn" href="grooved-texture-gold-bangle-set"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-grooved-texture-gold-bangle-set loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="grooved-texture-gold-bangle-set"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-grooved-texture-gold-bangle-set loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/grooved-texture-gold-bangle-set" class="grid-link__title">Grooved Texture Gold Bangle Set</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$765.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109323854"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row   " id="product-10109318286"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/hera-solitaire-ring" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/5_large.jpg?v=1496145595" class="featured-image" alt="Hera Solitaire Ring">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="hera-solitaire-ring" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109318286">                                    
        <input type="hidden" name="id" value="37510488142" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/hera-solitaire-ring">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-hera-solitaire-ring loading"><a class="add-in-wishlist-js btn" href="hera-solitaire-ring"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-hera-solitaire-ring loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="hera-solitaire-ring"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-hera-solitaire-ring loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/hera-solitaire-ring" class="grid-link__title">Hera Solitaire Ring</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$299.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109318286"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row   " id="product-10109326798"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name-78" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/13_large.jpg?v=1496149831" class="featured-image" alt="Zombi Lotus and Coin Gold Necklace">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name-78" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109326798">                                    
        <input type="hidden" name="id" value="37510546510" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name-78">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-78 loading"><a class="add-in-wishlist-js btn" href="product-name-78"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-78 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-78"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-78 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name-78" class="grid-link__title">Zombi Lotus and Coin Gold Necklace</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$90.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109326798"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row   " id="product-10109336206"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name-87" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/14_large.jpg?v=1496150074" class="featured-image" alt="Xvino Nakshi Beads Necklace - Chokapora">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name-87" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109336206">                                    
        <input type="hidden" name="id" value="37510593806" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name-87">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-87 loading"><a class="add-in-wishlist-js btn" href="product-name-87"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-87 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-87"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-87 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name-87" class="grid-link__title">Xvino Nakshi Beads Necklace - Chokapora</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$765.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109336206"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row   " id="product-10848622862"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/caren-swirl-stud-earrings" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/15_large.jpg?v=1496150696" class="featured-image" alt="Qaren Swirl Stud Earrings">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="caren-swirl-stud-earrings" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10848622862">                                    
        <input type="hidden" name="id" value="42025126606" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/caren-swirl-stud-earrings">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-caren-swirl-stud-earrings loading"><a class="add-in-wishlist-js btn" href="caren-swirl-stud-earrings"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-caren-swirl-stud-earrings loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="caren-swirl-stud-earrings"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-caren-swirl-stud-earrings loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/caren-swirl-stud-earrings" class="grid-link__title">Qaren Swirl Stud Earrings</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$765.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10848622862"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row    on-sale" id="product-10109330318"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name-83" class="grid-link">    
        
        
        <div class="featured-tag">
        <span class="badge badge--sale">
          
          <span class="gift-tag badge__text">Sale</span>
        </span>
        </div>
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      <div class="reveal"> 
        <span class="product-additional">      
          <img src="//cdn.shopify.com/s/files/1/1811/9385/products/12_large.jpg?v=1496150442" class="featured-image" alt="Welaribe Star Moon Necklace">
        </span>
        <img  src="//cdn.shopify.com/s/files/1/1811/9385/products/14_large_ec6cc15a-2d75-4748-bc37-ba2dbe299601_large.jpg?v=1537862649" class="hidden-feature_img" alt="Welaribe Star Moon Necklace" />
      </div> 
      
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name-83" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109330318">                                    
        <input type="hidden" name="id" value="37510561294" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name-83">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-83 loading"><a class="add-in-wishlist-js btn" href="product-name-83"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-83 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-83"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-83 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name-83" class="grid-link__title">Welaribe Star Moon Necklace</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$400.00</span>
          </div>
        
        
        <del class="grid-link__sale_price"><span class=money>$450.00</span></del>
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109330318"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  
    
    
    
    
      
        
      
      <li>
       <label style="background-color: gold; background-image: url(//cdn.shopify.com/s/files/1/1811/9385/files/gold.png?3493);"></label>
        
        <div class="hidden">
          <img src="//cdn.shopify.com/s/files/1/1811/9385/products/12_grande.jpg?v=1496150442" alt="gold"/>
        </div>
        
      </li>
        
        
      
    
      
        
      
      <li>
       <label style="background-color: green; background-image: url(//cdn.shopify.com/s/files/1/1811/9385/files/green.png?3493);"></label>
        
        <div class="hidden">
          <img src="//cdn.shopify.com/s/files/1/1811/9385/products/13_large_a68f8af4-e9a1-4056-aacf-423dea679c4e_grande.jpg?v=1537862648" alt="green"/>
        </div>
        
      </li>
        
        
      
    
      
        
      
      <li>
       <label style="border: 1px solid #cbcbcb; background-color: white; background-image: url(//cdn.shopify.com/s/files/1/1811/9385/files/white.png?3493);"></label>
        
        <div class="hidden">
          <img src="//cdn.shopify.com/s/files/1/1811/9385/products/14_large_ec6cc15a-2d75-4748-bc37-ba2dbe299601_grande.jpg?v=1537862649" alt="white"/>
        </div>
        
      </li>
        
        
      
    
  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row    on-sale" id="product-10109319118"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name-68" class="grid-link">    
        
        
        <div class="featured-tag">
        <span class="badge badge--sale">
          
          <span class="gift-tag badge__text">Sale</span>
        </span>
        </div>
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/6_large.jpg?v=1496146889" class="featured-image" alt="Plaited Shank Ring Mount">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name-68" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109319118">                                    
        <input type="hidden" name="id" value="37510498254" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name-68">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-68 loading"><a class="add-in-wishlist-js btn" href="product-name-68"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-68 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-68"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-68 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name-68" class="grid-link__title">Plaited Shank Ring Mount</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$99.00</span>
          </div>
        
        
        <del class="grid-link__sale_price"><span class=money>$109.00</span></del>
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109319118"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row   " id="product-10109320334"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name-69" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/7_large.jpg?v=1496148582" class="featured-image" alt="Light Stone mounted Ring">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name-69" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109320334">                                    
        <input type="hidden" name="id" value="37510503694" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name-69">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-69 loading"><a class="add-in-wishlist-js btn" href="product-name-69"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-69 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-69"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-69 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name-69" class="grid-link__title">Light Stone mounted Ring</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$299.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109320334"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row   " id="product-10109324238"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/11_large.jpg?v=1496149366" class="featured-image" alt="New Trendy copper bangle">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109324238">                                    
        <input type="hidden" name="id" value="37510519438" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name loading"><a class="add-in-wishlist-js btn" href="product-name"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name" class="grid-link__title">New Trendy copper bangle</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$50.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109324238"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row    on-sale" id="product-10109321806"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/product-name-70" class="grid-link">    
        
        
        <div class="featured-tag">
        <span class="badge badge--sale">
          
          <span class="gift-tag badge__text">Sale</span>
        </span>
        </div>
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/8_large.jpg?v=1496148739" class="featured-image" alt="Crumpled hard Ring">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="product-name-70" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109321806">                                    
        <input type="hidden" name="id" value="37510511886" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/product-name-70">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-product-name-70 loading"><a class="add-in-wishlist-js btn" href="product-name-70"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-product-name-70 loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="product-name-70"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-product-name-70 loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/product-name-70" class="grid-link__title">Crumpled hard Ring</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$60.00</span>
          </div>
        
        
        <del class="grid-link__sale_price"><span class=money>$75.00</span></del>
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109321806"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                      
          










<li class="grid__item item-row   " id="product-10109315278"  >
  <div class="products">
    <div class="product-container">  
      
      
      








      
      <a href="/collections/jewel/products/promise-solitaire-ring-mount" class="grid-link">    
        
        
        
           
        <div class="ImageOverlayCa"></div>
        
      
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/products/1_5c789c84-aba2-4dbb-8992-69166a320cac_large.jpg?v=1496144078" class="featured-image" alt="Let Promise Solitaire Ring Mount">
      
      
      
      </a>
      <div class="ImageWrapper">
        <div class="product-button">  
                 
          <a href="javascript:void(0)" id="promise-solitaire-ring-mount" class="quick-view-text">                      
            <i class="fa fa-expand" aria-hidden="true"></i>
          </a>       
          
           
          
      <form  action="/cart/add" method="post" class="variants clearfix" id="cart-form-10109315278">                                    
        <input type="hidden" name="id" value="37510470862" />  
         <a class="add-cart-btn">
       <i class="zmdi zmdi-shopping-cart"></i>
          </a>
      </form>  
      
       
                  
          <a href="/products/promise-solitaire-ring-mount">                      
            <i class="fa fa-external-link" aria-hidden="true"></i>
          </a>       
          
         
             
          <div class="add-to-wishlist">     
<div class="show">
  <div class="default-wishbutton-promise-solitaire-ring-mount loading"><a class="add-in-wishlist-js btn" href="promise-solitaire-ring-mount"><i class="fa fa-heart-o"></i><span class="tooltip-label">Add to wishlist</span></a></div>
 <div class="loadding-wishbutton-promise-solitaire-ring-mount loading btn" style="display: none; pointer-events: none"><a class="add_to_wishlist" href="promise-solitaire-ring-mount"><i class="fa fa-circle-o-notch fa-spin"></i></a></div>
  <div class="added-wishbutton-promise-solitaire-ring-mount loading" style="display: none;"><a class="added-wishlist btn add_to_wishlist" href="/pages/wishlist"><i class="fa fa-heart"></i><span class="tooltip-label">View Wishlist</span></a></div>
</div>
 </div>
         
           
        </div>
      </div>
    </div>
    <div class="product-detail">
      
      <a href="/collections/jewel/products/promise-solitaire-ring-mount" class="grid-link__title">Let Promise Solitaire Ring Mount</a>     
      <div class="grid-link__meta">
        <div class="product_price">
          
          
          <div class="grid-link__org_price">
          <span class=money>$590.00</span>
          </div>
        
        
          </div>      
            <span class="shopify-product-reviews-badge" data-id="10109315278"></span> 
      </div>
    
     
<ul class="item-swatch color_swatch_Value">  

  

  

</ul>

        
    
     
  </div>
  </div>
</li>
          
                   
          </ul>             
    </div>
      <div class="nav_type8__items wide-owl__nav-1492061512174 ">
            <a class="prev btn active"><i class="zmdi zmdi-arrow-left"></i></a>
            <a class="next btn"><i class="zmdi zmdi-arrow-right"></i></a>  
          </div> 
      
      <script type="text/javascript">       
  $(document).ready(function(){
   var type8_products_count = $('.type8__items li.item-row').length;
      if(type8_products_count > 5) { $('.wide-owl__nav-1492061512174').css('display','block');}
      else {$('.wide-owl__nav-1492061512174').css('display','none');}
       var type8_products = $(".wide-owl-1492061512174");
       type8_products.owlCarousel({
        items: 5,
        itemsCustom: false,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [980, 1],
        itemsTablet: [630, 1],
        itemsTabletSmall: false,
        itemsMobile: [479, 1],
        singleItem: false,
        itemsScaleUp: false,
        responsive: true,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: window,
        autoPlay: false,
        stopOnHover: false,
        navigation: false,
        pagination:false
      });
      // Custom Navigation Events
      $(".wide-owl__nav-1492061512174 .next").click(function(){
        type8_products.trigger('owl.next');
      })
      $(".wide-owl__nav-1492061512174 .prev").click(function(){
        type8_products.trigger('owl.prev');
      }) 
  });
      </script>
    </div>
    
    
    <div class="dt-sc-hr-invisible-large"></div>
  </div> 
  </div>
</div>
<div class="dt-sc-hr-invisible-large"></div>

</div><div id="shopify-section-1492061649731" class="shopify-section index-section"><div data-section-id="1492061649731" data-section-type="grid-banner-type-5" class="grid-banner-type-5">  
  <div class="wrapper">
  <div class="grid-uniform collectionItems">  
            
      
    
    <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item">
      <div class="img-hover-effect bg-effect-1492061649731-0">
      <div class="ovrly17">
      <a href="/collections">
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/files/home-10_519dcc00-b124-4191-8f73-72901fdeea5b_440x.jpg?v=1496151182" alt="White Pearl's" />
        <div class="ovrly"></div>
                 
        </a>
        
      
        </div>
      <div class="featured-content">              
                                 
        <h4><a href="/collections">White Pearl's</a></h4>
                        
      </div>
      </div>    
      </div>
    
    
    <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item">
      <div class="img-hover-effect bg-effect-1492061649731-1">
      <div class="ovrly17">
      <a href="/collections/necklaces">
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/files/home-11_28373b0c-d04d-49a5-85c7-b93782122438_440x.jpg?v=1496150864" alt="Necklaces" />
        <div class="ovrly"></div>
                 
        </a>
        
      
        </div>
      <div class="featured-content">              
                                 
        <h4><a href="/collections/necklaces">Necklaces</a></h4>
                        
      </div>
      </div>    
      </div>
    
    
    <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item">
      <div class="img-hover-effect bg-effect-1492061649731-2">
      <div class="ovrly17">
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/files/home-12_440x.jpg?v=1496151266" alt="Bangle Set" />
        <div class="ovrly"></div>
                 
      
        </div>
      <div class="featured-content">              
                                 
        <h4>Bangle Set</h4>
                        
      </div>
      </div>    
      </div>
    
    
    <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item">
      <div class="img-hover-effect bg-effect-1492061719122">
      <div class="ovrly17">
      
      
      <img src="//cdn.shopify.com/s/files/1/1811/9385/files/home-13_440x.jpg?v=1496151386" alt="Locket Designs" />
        <div class="ovrly"></div>
                 
      
        </div>
      <div class="featured-content">              
                                 
        <h4>Locket Designs</h4>
                        
      </div>
      </div>    
      </div>
    
    
  </div>
          
    
    <style>
      .grid-banner-type-5 .bg-effect-1492061649731-0 h4 a,.grid-banner-type-5 .bg-effect-1492061649731-0 h4  { color:#000; }
      .grid-banner-type-5 .bg-effect-1492061649731-0 h4 a:hover  { color:#000; }
       .grid-banner-type-5 .bg-effect-1492061649731-0 .ovrly { background:rgba(0, 0, 0, 0.7); }
      
  </style>
    
    <style>
      .grid-banner-type-5 .bg-effect-1492061649731-1 h4 a,.grid-banner-type-5 .bg-effect-1492061649731-1 h4  { color:#000; }
      .grid-banner-type-5 .bg-effect-1492061649731-1 h4 a:hover  { color:#000; }
       .grid-banner-type-5 .bg-effect-1492061649731-1 .ovrly { background:rgba(0, 0, 0, 0.7); }
      
  </style>
    
    <style>
      .grid-banner-type-5 .bg-effect-1492061649731-2 h4 a,.grid-banner-type-5 .bg-effect-1492061649731-2 h4  { color:#000; }
      .grid-banner-type-5 .bg-effect-1492061649731-2 h4 a:hover  { color:#000; }
       .grid-banner-type-5 .bg-effect-1492061649731-2 .ovrly { background:rgba(0, 0, 0, 0.7); }
      
  </style>
    
    <style>
      .grid-banner-type-5 .bg-effect-1492061719122 h4 a,.grid-banner-type-5 .bg-effect-1492061719122 h4  { color:#000; }
      .grid-banner-type-5 .bg-effect-1492061719122 h4 a:hover  { color:#000; }
       .grid-banner-type-5 .bg-effect-1492061719122 .ovrly { background:rgba(0, 0, 0, 0.7); }
      
  </style>
    
    
  </div>
  
</div>
<div class="dt-sc-hr-invisible-large"></div> 

</div><!-- END content_for_index -->
              
        </div>       
        
      
    </main>
  </div>

  <div id="shopify-section-footer-model-3" class="shopify-section index-section"><div data-section-id="footer-model-3"  data-section-type="Footer-model-3" class="footer-model-3">
  <div class="footer-logo" style="background:#f9f9f9;border-top:1px solid rgba(0,0,0,0);">
              
    <a href="/">
      <img class="normal-footer-logo" src="//cdn.shopify.com/s/files/1/1811/9385/files/logo_6c3cc8bc-dbd3-492e-b25b-e66807b1a581_200x.png?v=1496236059" alt="Huge Jewelry" />
    </a>
      
          
    <p style="color:#000">Pellentesque posuere orci lobortis scelerisque blandit. Donec id tellus lacinia an, tincidunt risus ac, consequat velit. 
<br>Quisquemos sodales suscipit tortor ditaemcos condimentum lacus.</p> 
    

    <div class="footer_social_icons">
            
      <ul class="inline-list social-icons social-links-type-3">
  
    <li>
      <a style="border:1px solid #000;color:#000" class="icon-fallback-text twitt hexagon" target="blank" href="https://twitter.com/shopify" title="Twitter">
        <i class="fa fa-twitter" aria-hidden="true"></i>
      </a>
    </li>
  
  
    <li>
      <a style="border:1px solid #000;color:#000" class="icon-fallback-text fb hexagon" target="blank" href="https://www.facebook.com/shopify" title="Facebook">
         <i class="fa fa-facebook" aria-hidden="true"></i>
      </a>
    </li>
  
  
  
    <li>
      <a style="border:1px solid #000;color:#000" class="icon-fallback-text google hexagon" target="blank" href="#" title="Google+" rel="publisher">
        <i class="fa fa-google-plus" aria-hidden="true"></i>
      </a>
    </li>
  
  
  
    <li>
      <a style="border:1px solid #000;color:#000" class="icon-fallback-text tumblr" target="blank" href="#" title="Tumblr">
        <i class="fa fa-tumblr" aria-hidden="true"></i>
      </a>
    </li>
  
  
  
  
  
</ul>

      
    </div>
  </div>

  <div style="background:#000;" class="grid__item copyright-section ">
    
    <p style="color:#ffffff;"  class="copyright">Copyright <a target="_blank" rel="nofollow" href="https://www.shopify.com?utm_campaign=poweredby&amp;utm_medium=shopify&amp;utm_source=onlinestore">Powered by Shopify</a></p>
       
    
    <div class="footer-icons">        
      <ul class="inline-list payment-icons">
      <li><a href="/cart"><img src="//cdn.shopify.com/s/files/1/1811/9385/files/payment_icon_6_small.png?v=1490168951" alt="payment_icon_1" /></a></li>
      <li><a href="/cart"><img src="//cdn.shopify.com/s/files/1/1811/9385/files/payment_icon_5_small.png?v=1490168944" alt="payment_icon_2" /></a></li>
      <li><a href="/cart"><img src="//cdn.shopify.com/s/files/1/1811/9385/files/payment_icon_3_small.png?v=1490168933" alt="payment_icon_3" /></a></li>
      <li><a href="/cart"><img src="//cdn.shopify.com/s/files/1/1811/9385/files/payment_icon_2_small.png?v=1490168929" alt="payment_icon_4" /></a></li>
      <li><a href="/cart"><img src="//cdn.shopify.com/s/files/1/1811/9385/files/payment_icon_4_small.png?v=1490168941" alt="payment_icon_5" /></a></li>
                  
                  
                  
                  
                  
      </ul>                 
    </div> 
    
  </div>

  <style>
    .footer-model-3 .footer_social_icons ul li a:hover { border-color:#d2aa5c !important;background:#d2aa5c !important;color:#ffffff !important; }
    .footer-model-3 .copyright-section p a { color:#ffffff; }
    .footer-model-3 .copyright-section p a:hover { color:#d2aa5c; }
    
  </style>
</div>


</div> 
  
  
  
  
  
  <script src="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/timber.js?3493" type="text/javascript"></script>  
  <script src="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/theme.js?3493" type="text/javascript"></script>
  <script src="//cdn.shopify.com/s/assets/themes_support/option_selection-fe6b72c2bbdd3369ac0bfefe8648e3c889efca213baefd4cfb0dd9363563831f.js" type="text/javascript"></script>
  <script src="//cdn.shopify.com/s/assets/themes_support/api.jquery-e94e010e92e659b566dbc436fdfe5242764380e00398907a14955ba301a4749f.js" type="text/javascript"></script>   
  <script src="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/footer.js?3493" type="text/javascript"></script>
  <script src="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/shop.js?3493" type="text/javascript"></script>    
   
  
  
  <script src="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/slider-init.js?3493" type="text/javascript"></script>
  
  <script src="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/countdown.js?3493" type="text/javascript"></script>
  
  <!-- Begin quick-view-template -->
<div class="clearfix" id="quickview-template" style="display:none">
  <div class="overlay"></div>
  <div class="content clearfix">
    <div class="product-img images">
      <div class="quickview-featured-image product-photo-container"></div>
      <div class="more-view-wrapper">
        <ul class="product-photo-thumbs quickview-more-views-owlslider owl-carousel owl-theme">
        </ul>
        <div class="quick-view-carousel"></div>
      </div>
    </div>
    <div class="product-shop summary">
      <div class="product-item product-detail-section">
        <h2 class="product-title"><a>&nbsp;</a></h2>
       
        <div class="prices product_price">
          <label>Effective Price:</label>
          <div class="price h6" id="QProductPrice"></div>
          <div class="compare-price" id="QComparePrice"></div>
        </div>
        
        <div class="product-infor">
          <p class="product-inventory"><label>Availability:</label><span></span></p>    
        </div>
        
        <div class="details clearfix">
          <form action="/cart/add" method="post" class="variants">
            <select name='id' style="display:none"></select>
            <div class="qty-section quantity-box">
              <label>Quantity:</label>
              <div class="dec button qtyminus">-</div>
              <input type="number" name="quantity" id="Qty" value="1" class="quantity">
              <div class="inc button qtyplus">+</div>
            </div>
            
            
            <div class="total-price">
              <label>Subtotal</label><span class="h5"></span>
            </div>
                        
            <div class="actions">
                <button type="button" class="add-to-cart-btn btn">
                  Add to Cart
                </button>
              </div>
          </form>
        </div>     
      </div>
    </div>  
 <a href="javascript:void(0)" class="close-window"></a> 
  </div>
 
</div>
<!-- End of quick-view-template -->
<script type="text/javascript">  
  Shopify.doNotTriggerClickOnThumb = false; 
                                       
  var selectCallbackQuickview = function(variant, selector) {
      var productItem = jQuery('.quick-view .product-item');
          addToCart = productItem.find('.add-to-cart-btn'),
          productPrice = productItem.find('.price'),
          comparePrice = productItem.find('.compare-price'),
          totalPrice = productItem.find('.total-price span'),
          inventory = productItem.find('.product-inventory');
      if (variant) {
        if (variant.available) {          
          // We have a valid product variant, so enable the submit button
          addToCart.removeClass('disabled').removeAttr('disabled').text('Add to Cart');         
    
        } else {
          // Variant is sold out, disable the submit button
          addToCart.val('Sold Out').addClass('disabled').attr('disabled', 'disabled');         
        }
    
        // Regardless of stock, update the product price
        productPrice.html(Shopify.formatMoney(variant.price, "<span class=money>${{amount}}</span>"));
    
        // Also update and show the product's compare price if necessary
        if ( variant.compare_at_price > variant.price ) {
          comparePrice
            .html(Shopify.formatMoney(variant.compare_at_price, "<span class=money>${{amount}}</span>"))
            .show();
          productPrice.addClass('on-sale');
        } else {
          comparePrice.hide();
          productPrice.removeClass('on-sale');
        }
    
       
    
     //update variant inventory
     
     var inventoryInfo = productItem.find('.product-inventory span');
          if (variant.available) {
            if (variant.inventory_management !=null ) {
              inventoryInfo.text(window.in_stock );
              inventoryInfo.addClass('items-count');
              inventoryInfo.removeClass('many-in-stock');
              inventoryInfo.removeClass('out-of-stock');
              inventoryInfo.removeClass('unavailable');
            } else {
              inventoryInfo.text(window.many_in_stock);
              inventoryInfo.addClass('many-in-stock')
              inventoryInfo.removeClass('items-count');
              inventoryInfo.removeClass('out-of-stock');
              inventoryInfo.removeClass('unavailable');
            }
          } else {
            inventoryInfo.addClass('out-of-stock')
            inventoryInfo.text(window.out_of_stock);
            inventoryInfo.removeClass('items-count');
            inventoryInfo.removeClass('many-in-stock');
            inventoryInfo.removeClass('unavailable');
          }
     
    
    
    /*recaculate total price*/
        //try pattern one before pattern 2
        var regex = /([0-9]+[.|,][0-9]+[.|,][0-9]+)/g;
        var unitPriceTextMatch = jQuery('.quick-view .price').text().match(regex);

        if (!unitPriceTextMatch) {
          regex = /([0-9]+[.|,][0-9]+)/g;
          unitPriceTextMatch = jQuery('.quick-view .price').text().match(regex);     
        }

        if (unitPriceTextMatch) {
          var unitPriceText = unitPriceTextMatch[0];     
          var unitPrice = unitPriceText.replace(/[.|,]/g,'');
          var quantity = parseInt(jQuery('.quick-view input[name=quantity]').val());
          var totalPrice = unitPrice * quantity;

          var totalPriceText = Shopify.formatMoney(totalPrice, window.money_format);
          regex = /([0-9]+[.|,][0-9]+[.|,][0-9]+)/g;     
          if (!totalPriceText.match(regex)) {
            regex = /([0-9]+[.|,][0-9]+)/g;
          } 
          totalPriceText = totalPriceText.match(regex)[0];

          var regInput = new RegExp(unitPriceText, "g"); 
          var totalPriceHtml = jQuery('.quick-view .price').html().replace(regInput ,totalPriceText);
          jQuery('.quick-view .total-price span').html(totalPriceHtml);             
        }
    /*end of price calculation*/
    
    
    
        Currency.convertAll('USD', jQuery('#currencies').val(), 'span.money', 'money_format');
        
                            
        /*begin variant image*/
     /*begin variant image*/
        if (variant && variant.featured_image) {
            var originalImage = jQuery(".quick-view .quickview-featured-image img");
            var newImage = variant.featured_image;
            var element = originalImage[0];
            Shopify.Image.switchImage(newImage, element, function (newImageSizedSrc, newImage, element) {
              newImageSizedSrc = newImageSizedSrc.replace(/\?(.*)/,"");
              jQuery('.quick-view .more-view-wrapper img').each(function() {
                var grandSize = jQuery(this).attr('src');
                grandSize = grandSize.replace('medium','grande');
               
                if (grandSize == newImageSizedSrc) {
                  jQuery(this).parent().trigger('click');              
                  return false;
                }
              });
            });        
        }
        /*end of variant image*/   
        /*end of variant image*/ 
      } else {
        // The variant doesn't exist. Just a safegaurd for errors, but disable the submit button anyway
        addToCart.text('Unavailable').addClass('disabled').attr('disabled', 'disabled');
       
            
          var inventoryInfo = productItem.find('.product-inventory span');
          inventoryInfo.addClass("unavailable");
          inventoryInfo.removeClass("many-in-stock");
          inventoryInfo.removeClass("items-count");
          inventoryInfo.removeClass("out-of-stock");
          inventoryInfo.text(window.unavailable);
          
      }   
  };  
  
  </script>

  <div class="loading-modal modal">Loading</div>
<div class="ajax-error-modal modal">
  <div class="modal-inner">
    <div class="ajax-error-title">Error</div>
    <div class="ajax-error-message"></div>
  </div>
</div>
<div class="ajax-success-modal modal">
    <div class="overlay"></div>
  <div class="content"> 
      
      <p class="added-to-cart info">Added to cart</p>
      <p class="added-to-wishlist info">Added to wishlist</p>
      <div class="ajax-left">        
      <img class="ajax-product-image" alt="modal window" src="/" />
      </div>
      <div class="ajax-right"> 
        <h3 class="ajax-product-title">Product name</h3>
        <span class="ajax_price"></span>
        <div class="success-message added-to-cart"><a href="/cart" class="btn"><i class="fa fa-shopping-cart"></i>View Cart</a> </div>  
        <div class="success-message added-to-wishlist"> <a href="/pages/wishlist" class="btn"><i class="fa fa-heart"></i>View Wishlist</a></div>                
      </div>
    <a href="javascript:void(0)" class="close-modal"><i class="fa fa-times-circle"></i></a>
  </div>    
</div>
  
<script src="//cdn.shopify.com/s/javascripts/currencies.js" type="text/javascript"></script>
<script src="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/jquery.currencies.min.js?3493" type="text/javascript"></script>

<script>      
// Pick your format here:  
// Can be 'money_format' or 'money_with_currency_format'
Currency.format = 'money_format';
var shopCurrency = 'USD';

/* Sometimes merchants change their shop currency, let's tell our JavaScript file */
Currency.moneyFormats[shopCurrency].money_with_currency_format = "${{amount}} USD";
Currency.moneyFormats[shopCurrency].money_format = "${{amount}}";

var cookieCurrency = Currency.cookie.read();

// Fix for customer account pages 
jQuery('span.money span.money').each(function() {
  jQuery(this).parent('span.money').removeClass('money');
});

// Add precalculated shop currency to data attribute 
jQuery('span.money').each(function() {
  jQuery(this).attr('data-currency-USD', jQuery(this).html());
});

// Select all your currencies buttons.
var currencySwitcher = jQuery('#currencies');

// When the page loads.
if (cookieCurrency == null || cookieCurrency == shopCurrency) {
  Currency.currentCurrency = shopCurrency;
}
else {
  Currency.currentCurrency = cookieCurrency;
    currencySwitcher.val(cookieCurrency);    
    Currency.convertAll(shopCurrency, cookieCurrency);  
}
//currencySwitcher.selectize();
jQuery('.selectize-input input').attr('disabled','disabled');

// When customer clicks on a currency switcher.
currencySwitcher.change(function() {
    var newCurrency =  jQuery(this).val();
    Currency.cookie.write(newCurrency);
    Currency.convertAll(Currency.currentCurrency, newCurrency); 
    //show modal
    jQuery("#currencies-modal span").text(newCurrency);
    if (jQuery("#cart-currency").length >0) {
      jQuery("#cart-currency").text(newCurrency);
    }
    jQuery("#currencies-modal").fadeIn(500).delay(3000).fadeOut(500);    
});

// For product options.
var original_selectCallback = window.selectCallback;
var selectCallback = function(variant, selector) {
  original_selectCallback(variant, selector);
  Currency.convertAll(shopCurrency, jQuery('#currencies').val());
};
</script>

  
<script type="text/javascript">// <![CDATA[
jQuery(document).ready(function() {    //
    var $modalParent        = jQuery('div.newsletterwrapper'),
        modalWindow         = jQuery('#email-modal'),
        emailModal          = jQuery('#email-modal'),
        modalPageURL        = window.location.pathname; 
          
    modalWindow = modalWindow.html();
    modalWindow = '<div id="email-modal">' + modalWindow + '</div>';
    $modalParent.css({'position':'relative'});
    jQuery('.wrapper #email-modal').remove();
    $modalParent.append(modalWindow);
  
    if (jQuery.cookie('emailSubcribeModal') != 'closed') {
        openEmailModalWindow();
    };
      
    jQuery('#email-modal .btn.close').click(function(e) {
        e.preventDefault();
        closeEmailModalWindow();
    });
    jQuery('body').keydown(function(e) {
        if( e.which == 27) {
            closeEmailModalWindow();
            jQuery('body').unbind('keydown');
        }
    });
    jQuery('#mc_embed_signup form').submit(function() {
        if (jQuery('#mc_embed_signup .email').val() != '') {
            closeEmailModalWindow();
        }
    });
      
    function closeEmailModalWindow () {
        jQuery('#email-modal .modal-window').fadeOut(600, function() {
            jQuery('#email-modal .modal-overlay').fadeOut(600, function() {
                jQuery('#email-modal').hide();
                jQuery.cookie('emailSubcribeModal', 'closed', {expires:1, path:'/'});
            });
        })
    }
    function openEmailModalWindow () {
        jQuery('#email-modal').fadeIn(600, function() {
           jQuery('#email-modal .modal-window').fadeIn(600);
        });
    }
      
});
// ]]
// ]]></script>
<div class="newsletterwrapper">
  <div id="email-modal" style="display: none;">
    <div class="modal-overlay"></div>
    <div class="modal-window" style="display: none;">
      <div class="window-window">
          <a class="btn close" title="Close Window"></a>
          
           
         
          
            <div class="window-content">
          <div class="newsletter-title">
         <h2 class="title">Newsletter Signup</h2>
          </div>
           <p class="message">Subscribe and get notified at first on the latest update and offers!</p>
          <div id="mailchimp-email-subscibe">
            <div id="mc_embed_signup">
              

<form method="post" action="/contact#contact_form" id="contact_form" accept-charset="UTF-8" class="contact-form"><input type="hidden" name="form_type" value="customer" /><input type="hidden" name="utf8" value="✓" />


  <input type="email" value="" placeholder="Email address" name="contact[email]" class="mail" aria-label="Email address" >
  <input type="hidden" name="contact[tags]" value="newsletter">
  <button type="submit" class="btn" name="subscribe" value=""  id="subscribe" >Send</button>          



</form>  

            </div>   
            <span>Note:we do not spam</span>
          </div>   
        </div>
        
      </div>
    </div>
  </div>
</div>
  
  
  <script src="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/wow.js?3493" type="text/javascript"></script>  
  
  <script src="//cdn.shopify.com/s/files/1/1811/9385/t/7/assets/classie.js?3493"></script>
</body>
</html>


