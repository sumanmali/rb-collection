@extends('layouts.frontend.app')

@section('content')
<main class="main-content"> 




	<nav class="breadcrumb" aria-label="breadcrumbs">



		<h1>{{$blogs->title}}</h1>
		<a href="/" title="Back to the frontpage">Home</a>

		<span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
		<a href="/blogs" title="">News</a>
		<span aria-hidden="true" class="breadcrumb__sep">&#47;</span>
		<span>{{$blogs->title}}</span>


	</nav>




	<div class="dt-sc-hr-invisible-large"></div>
	<div class="container-bg">

		<div class="grid__item">         



			<div class="grid blog-design-4 blog-detail-section">
				<div class="container">
					<div class="blog-section">
						<article class="grid__item" itemscope itemtype="http://schema.org/Article">


							<img src="/uploads/blogs/{{$blogs->f_image}}" alt="{{$blogs->title}}" class="article__image" />

							<div class="blog-description">
								<div class="blogs-sub-title">
									<p class="author">

										<i class="zmdi zmdi-account"></i>
										<span> Admin</span>

									</p> 

									<p class="blog-date">
										<span data-datetime="2017-05-30"><span class="date">{{ $blogs->created_at->format('M d , Y') }}</span></span>             
									</p>

								</div>

<div class="blog_section_detail">    
<h4>{{$blogs->title}}</h4>
<p class="desc"><?php echo ($blogs->description )?></p>




<!-- <p class="comments-count">0 Comments</p>
<div class="social-sharing normal" data-permalink="https://huge-jewelry.myshopify.com/blogs/news/neque-porro-quisquam-est-qui-dolorem-ipsum-quia">
<label>Share this on:  </label>      


<a target="_blank" href="//www.facebook.com/sharer.php?u=https://huge-jewelry.myshopify.com/blogs/news/neque-porro-quisquam-est-qui-dolorem-ipsum-quia" class="share-facebook">
											<span class="icon-social-facebook"></span> -->
    <!--  <span class="share-title">Share</span>
      
        <span class="share-count">0</span>
    -->
<!-- </a>



<a target="_blank" href="//twitter.com/share?text=Neque%20porro%20quisquam%20est,%20qui%20dolorem%20ipsum%20quia&amp;url=https://huge-jewelry.myshopify.com/blogs/news/neque-porro-quisquam-est-qui-dolorem-ipsum-quia" class="share-twitter">
	<span class="icon-social-twitter"></span> -->
     <!-- <span class="share-title">Tweet</span>
      
        <span class="share-count">0</span>
    -->
<!-- </a>

<a target="_blank" href="//pinterest.com/pin/create/button/?url=https://huge-jewelry.myshopify.com/blogs/news/neque-porro-quisquam-est-qui-dolorem-ipsum-quia&amp;media=http://cdn.shopify.com/s/files/1/1811/9385/articles/shutterstock_519693688_1024x1024.jpg?v=1496218114&amp;description=Neque%20porro%20quisquam%20est,%20qui%20dolorem%20ipsum%20quia" class="share-pinterest">
	<span class="icon-social-pinterest"></span> -->
       <!-- <span class="share-title">Pin it</span>
        
          <span class="share-count">0</span>
      -->
  <!-- </a> -->

 
  <!-- <a target="_blank" href="//plus.google.com/share?url=https://huge-jewelry.myshopify.com/blogs/news/neque-porro-quisquam-est-qui-dolorem-ipsum-quia" class="share-google"> -->
  	<!-- Cannot get Google+ share count with JS yet -->
  	<!-- <span class="icon-social-google"></span> -->
     <!-- 
        <span class="share-count">+1</span>
    -->
<!-- </a> -->


</div>



<!-- <hr class="hr--clear hr--small">
<div id="comments">




	<form method="post" action="/blogs/news/neque-porro-quisquam-est-qui-dolorem-ipsum-quia/comments#comment_form" id="comment_form" accept-charset="UTF-8" class="comment-form"><input type="hidden" name="form_type" value="new_comment" /><input type="hidden" name="utf8" value="✓" />

		<h3>Leave a comment</h3>



		<div class="grid">
			<p class="grid__item wide--one-half post-large--one-half large--one-half">
				<label for="CommentAuthor" class="label--hidden">Name</label>
				<input  type="text" name="comment[author]" placeholder="Name" id="CommentAuthor" value="" autocapitalize="words">
			</p>
			<p class="grid__item wide--one-half post-large--one-half large--one-half">
				<label for="CommentEmail" class="label--hidden">Email</label>
				<input  type="email" name="comment[email]" placeholder="Email" id="CommentEmail" value="" autocorrect="off" autocapitalize="off">
			</p>
			<label for="CommentBody" class="label--hidden">Message</label>
			<textarea  name="comment[body]" id="CommentBody" placeholder="Message"></textarea>
		</div>

		<button type="submit" class="btn"><span>Post comment</span></button>
	</form>
</div> -->

</div>

</div>
</article>
</div>
</div>
</div>     
</div>       

</div>


<div class="dt-sc-hr-invisible-large"></div>

</main>
@endsection